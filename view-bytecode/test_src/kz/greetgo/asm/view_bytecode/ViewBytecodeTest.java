package kz.greetgo.asm.view_bytecode;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

public class ViewBytecodeTest {

  @SneakyThrows
  @Test
  public void readFormattedBytes() {
    final byte[] bytes;
    final String text;
    try (InputStream resourceAsStream = getClass().getResourceAsStream("some-bytecode.txt")) {
      requireNonNull(resourceAsStream);

      text  = new String(resourceAsStream.readAllBytes(), UTF_8);
      bytes = ViewBytecode.readFormattedBytes(text);

    }

    StringBuilder sb = new StringBuilder();

    ViewBytecode.printBytes(bytes, sb);

    assertThat(sb.toString()).isEqualTo(text);
  }

  @DataProvider
  Object[][] extractByte_DataProvider() {
    return new Object[][]{
      {"32", (byte) 0x32},
      {"ff", (byte) 0xFF},
      {"00", (byte) 0x00},
      {"03", (byte) 0x03},
      {"13", (byte) 0x13},
      {"f3", (byte) 0xF3},
      {"f7", (byte) 0xF7},
      {"b9", (byte) 0xB9},
    };
  }

  @Test(dataProvider = "extractByte_DataProvider")
  public void extractByte_001(String str, byte expectedValue) {

    //
    //
    final byte actual = ViewBytecode.extractByte(str, 0);
    //
    //

    System.out.println("2SOhGMAOmv :: actual = " + actual + ", expected = " + expectedValue);

    assertThat(actual).isEqualTo(expectedValue);

  }

  @Test(dataProvider = "extractByte_DataProvider")
  public void byteToHex(String expectedHex, byte source) {

    //
    //
    final String actual = ViewBytecode.byteToHex(source);
    //
    //

    assertThat(actual).isEqualTo(expectedHex);
  }

  @SneakyThrows
  @Test
  public void convertToAsmFromResource() {
    final byte[] bytes;
    try (InputStream resourceAsStream = getClass().getResourceAsStream("some-bytecode.txt")) {
      requireNonNull(resourceAsStream);
      bytes = ViewBytecode.readFormattedBytes(new String(resourceAsStream.readAllBytes(), UTF_8));
    }

    StringBuilder sb = new StringBuilder();

    try {
      ViewBytecode.convertToAsm(bytes, sb);
    } catch (RuntimeException e) {
      System.out.println(sb);
      throw e;
    }

    System.out.println(sb);

  }

  @DataProvider
  Object[][] convertToAsm_DataProvider() {
    return new Object[][]{
      {"32", "0000000: 32  AALoad"},
      {"53", "0000000: 53  AAStore"},
      {"01", "0000000: 01  AConst_NULL"},

      {"19 10", "0000000: 19 10 ALoad 16"},
      {"2a   ", "0000000: 2a    ALoad_0"},
      {"2b   ", "0000000: 2b    ALoad_1"},
      {"2c   ", "0000000: 2c    ALoad_2"},
      {"2d   ", "0000000: 2d    ALoad_3"},

      {"bd 00 11", "0000000: bd 00 11     ANewArray 17"},
      {"bd ff 11", "0000000: bd ff 11     ANewArray 65297"},

      {"b0", "0000000: b0 AReturn"},

      {"be", "0000000: be ArrayLength"},

      {"3a 10", "0000000: 3a 10 AStore 16"},
      {"4b   ", "0000000: 4b    AStore_0"},
      {"4c   ", "0000000: 4c    AStore_1"},
      {"4d   ", "0000000: 4d    AStore_2"},
      {"4e   ", "0000000: 4e    AStore_3"},

      {"bf", "0000000: bf    AThrow"},

      {"33", "0000000: 33    BALoad"},
      {"54", "0000000: 54    BAStore"},

      {"10 11", "0000000: 10 11  BIPush 17"},

      {"ca", "0000000: ca  Breakpoint"},

      {"34", "0000000: 34  CALoad"},
      {"55", "0000000: 55  CAStore"},

      {"c0 00 11", "0000000: c0 00 11 CheckCast #17"},

      {"90", "0000000: 90  D2F"},
      {"8e", "0000000: 8e  D2I"},
      {"8f", "0000000: 8f  D2L"},
      {"63", "0000000: 63  DAdd"},
      {"31", "0000000: 31  DALoad"},
      {"52", "0000000: 52  DAStore"},
      {"98", "0000000: 98  DCmpG"},
      {"97", "0000000: 97  DCmpL"},
      {"0e", "0000000: 0e  DConst_0"},
      {"0f", "0000000: 0f  DConst_1"},
      {"6f", "0000000: 6f  DDiv"},

      {"18 11", "0000000: 18 11  DLoad 17"},
      {"26   ", "0000000: 26     DLoad_0"},
      {"27   ", "0000000: 27     DLoad_1"},
      {"28   ", "0000000: 28     DLoad_2"},
      {"29   ", "0000000: 29     DLoad_3"},

      {"6b", "0000000: 6b  DMul"},
      {"77", "0000000: 77  DNeg"},
      {"73", "0000000: 73  DRem"},
      {"af", "0000000: af  DReturn"},

      {"39 11", "0000000: 39 11  DStore 17"},
      {"47   ", "0000000: 47     DStore_0"},
      {"48   ", "0000000: 48     DStore_1"},
      {"49   ", "0000000: 49     DStore_2"},
      {"4a   ", "0000000: 4a     DStore_3"},

      {"67", "0000000: 67  DSub"},
      {"59", "0000000: 59  DUP"},
      {"5a", "0000000: 5a  DUP_X1"},
      {"5b", "0000000: 5b  DUP_X2"},
      {"5c", "0000000: 5c  DUP2"},
      {"5d", "0000000: 5d  DUP2_X1"},
      {"5e", "0000000: 5e  DUP2_X2"},
      {"8d", "0000000: 8d  F2D"},
      {"8b", "0000000: 8b  F2I"},
      {"8c", "0000000: 8c  F2L"},
      {"62", "0000000: 62  FAdd"},
      {"30", "0000000: 30  FALoad"},
      {"51", "0000000: 51  FAStore"},
      {"96", "0000000: 96  FCmpG"},
      {"95", "0000000: 95  FCmpL"},
      {"0b", "0000000: 0b  FConst_0"},
      {"0c", "0000000: 0c  FConst_1"},
      {"0d", "0000000: 0d  FConst_2"},
      {"6e", "0000000: 6e  FDiv"},

      {"17 11", "0000000: 17 11  FLoad 17"},
      {"22   ", "0000000: 22     FLoad_0"},
      {"23   ", "0000000: 23     FLoad_1"},
      {"24   ", "0000000: 24     FLoad_2"},
      {"25   ", "0000000: 25     FLoad_3"},

      {"6a", "0000000: 6a  FMul"},
      {"76", "0000000: 76  FNeg"},
      {"72", "0000000: 72  FRem"},
      {"ae", "0000000: ae  FReturn"},

      {"38 10", "0000000: 38 10 FStore 16"},
      {"43   ", "0000000: 43    FStore_0"},
      {"44   ", "0000000: 44    FStore_1"},
      {"45   ", "0000000: 45    FStore_2"},
      {"46   ", "0000000: 46    FStore_3"},

      {"66", "0000000: 66  FSub"},

      {"b4 00 11", "0000000: b4 00 11  GetField #17"},
      {"b4 aa 11", "0000000: b4 aa 11  GetField #43537"},

      {"b2 00 11", "0000000: b2 00 11  GetStatic #17"},
      {"b2 aa 11", "0000000: b2 aa 11  GetStatic #43537"},

      {"a7 00 11", "0000000: a7 00 11  Goto +17 (0 -> 17)"},
      {"66 66 66 66 a7 00 11", """
                               0000000: 66 FSub
                               0000001: 66 FSub
                               0000002: 66 FSub
                               0000003: 66 FSub
                               0000004: a7 00 11 Goto +17 (4 -> 21)
                               """},
      {"66 66 66 66 a7 ff ff", """
                               0000000: 66 FSub
                               0000001: 66 FSub
                               0000002: 66 FSub
                               0000003: 66 FSub
                               0000004: a7 ff ff Goto -1 (4 -> 3)
                               """},
      {"66 66 66 66 a7 ff fc", """
                               0000000: 66 FSub
                               0000001: 66 FSub
                               0000002: 66 FSub
                               0000003: 66 FSub
                               0000004: a7 ff fc Goto -4 (4 -> 0)
                               """},

      {"c8 00 00 00 11", "0000000: c8 00 00 00 11  Goto_W +17 (0 -> 17)"},
      {"66 66 66 66 c8 ff ff ff fc", """
                                     0000000: 66 FSub
                                     0000001: 66 FSub
                                     0000002: 66 FSub
                                     0000003: 66 FSub
                                     0000004: c8 ff ff ff fc Goto_W -4 (4 -> 0)
                                     """},

      {"91", "0000000: 91    I2B"},
      {"92", "0000000: 92    I2C"},
      {"87", "0000000: 87    I2D"},
      {"86", "0000000: 86    I2F"},
      {"85", "0000000: 85    I2L"},
      {"93", "0000000: 93    I2S"},
      {"60", "0000000: 60    IAdd"},
      {"2e", "0000000: 2e    IALoad"},
      {"7e", "0000000: 7e    IAnd"},
      {"4f", "0000000: 4f    IAStore"},
      {"02", "0000000: 02    IConst_m1"},
      {"03", "0000000: 03    IConst_0"},
      {"04", "0000000: 04    IConst_1"},
      {"05", "0000000: 05    IConst_2"},
      {"06", "0000000: 06    IConst_3"},
      {"07", "0000000: 07    IConst_4"},
      {"08", "0000000: 08    IConst_5"},
      {"6c", "0000000: 6c    IDiv"},

      {"a5 00 11", "0000000: a5 00 11   if_ACmpEQ    +17 (0 -> 17)"},
      {"a6 00 11", "0000000: a6 00 11   if_ACmpNE    +17 (0 -> 17)"},
      {"9f 00 11", "0000000: 9f 00 11   if_ICmpEQ    +17 (0 -> 17)"},
      {"a2 00 11", "0000000: a2 00 11   if_ICmpGE    +17 (0 -> 17)"},
      {"a3 00 11", "0000000: a3 00 11   if_ICmpGT    +17 (0 -> 17)"},
      {"a4 00 11", "0000000: a4 00 11   if_ICmpLE    +17 (0 -> 17)"},
      {"a1 00 11", "0000000: a1 00 11   if_ICmpLT    +17 (0 -> 17)"},
      {"a0 00 11", "0000000: a0 00 11   if_ICmpNE    +17 (0 -> 17)"},
      {"99 00 11", "0000000: 99 00 11   ifEQ         +17 (0 -> 17)"},
      {"9c 00 11", "0000000: 9c 00 11   ifGE         +17 (0 -> 17)"},
      {"9d 00 11", "0000000: 9d 00 11   ifGT         +17 (0 -> 17)"},
      {"9e 00 11", "0000000: 9e 00 11   ifLE         +17 (0 -> 17)"},
      {"9b 00 11", "0000000: 9b 00 11   ifLT         +17 (0 -> 17)"},
      {"9a 00 11", "0000000: 9a 00 11   ifNE         +17 (0 -> 17)"},
      {"c7 00 11", "0000000: c7 00 11   ifNonNull    +17 (0 -> 17)"},
      {"c6 00 11", "0000000: c6 00 11   ifNull       +17 (0 -> 17)"},

      {"84 01 02", "0000000: 84 01 02   IInc 1, 2"},
      {"84 07 fd", "0000000: 84 07 fd   IInc 7, -3"},

      {"15 0a", "0000000: 15 0a   ILoad 10"},
      {"1a   ", "0000000: 1a      ILoad_0"},
      {"1b   ", "0000000: 1b      ILoad_1"},
      {"1c   ", "0000000: 1c      ILoad_2"},
      {"1d   ", "0000000: 1d      ILoad_3"},

      {"fe", "0000000: fe   ImpDep1"},
      {"ff", "0000000: ff   ImpDep2"},


      {"68", "0000000: 68   IMul"},
      {"74", "0000000: 74   INeg"},

      {"c1 00 11", "0000000: c1 00 11   InstanceOf  #17"},

      {"ba 00 11 00 00", "0000000: ba 00 11 00 00   InvokeDynamic   #17, 0, 0"},
      {"b9 00 10 04 00", "0000000: b9 00 10 04 00   InvokeInterface #16, args 4, 0"},
      {"b7 00 11      ", "0000000: b7 00 11         InvokeSpecial   #17"},
      {"b8 00 11      ", "0000000: b8 00 11         InvokeStatic    #17"},
      {"b6 00 11      ", "0000000: b6 00 11         InvokeVirtual   #17"},

      {"80", "0000000: 80   IOr"},
      {"70", "0000000: 70   IRem"},
      {"ac", "0000000: ac   IReturn"},
      {"78", "0000000: 78   IShl"},
      {"7a", "0000000: 7a   IShr"},

      {"36 11", "0000000: 36 11  IStore 17"},
      {"3b   ", "0000000: 3b     IStore_0"},
      {"3c   ", "0000000: 3c     IStore_1"},
      {"3d   ", "0000000: 3d     IStore_2"},
      {"3e   ", "0000000: 3e     IStore_3"},

      {"a8 00 11      ", "0000000: a8 00 11        JSR   +17 (0 -> 17)"},
      {"c9 00 00 00 11", "0000000: c9 00 00 00 11  JSR_W +17 (0 -> 17)"},

      {"8a", "0000000: 8a   L2D      "},
      {"89", "0000000: 89   L2F      "},
      {"88", "0000000: 88   L2I      "},
      {"61", "0000000: 61   LAdd     "},
      {"2f", "0000000: 2f   LALoad   "},
      {"7f", "0000000: 7f   LAnd     "},
      {"50", "0000000: 50   LAStore  "},
      {"94", "0000000: 94   LCmp     "},
      {"09", "0000000: 09   LConst_0 "},
      {"0a", "0000000: 0a   LConst_1 "},

      {"12 11   ", "0000000: 12 11     LDC    #17"},
      {"13 00 11", "0000000: 13 00 11  LDC_W  #17"},
      {"14 00 11", "0000000: 14 00 11  LDC2_W #17"},

      {"6d   ", "0000000: 6d      LDiv     "},
      {"16 11", "0000000: 16 11   LLoad 17 "},
      {"1e   ", "0000000: 1e      LLoad_0  "},
      {"1f   ", "0000000: 1f      LLoad_1  "},
      {"20   ", "0000000: 20      LLoad_2  "},
      {"21   ", "0000000: 21      LLoad_3  "},
      {"69   ", "0000000: 69      LMul     "},
      {"75   ", "0000000: 75      LNeg     "},

      {"66 66 66 66    66 ab 00 00 "
        + " 00 00 00 11    00 00 00 03"
        + " 00 00 23 ab    ff ff ff fc"
        + " 00 00 23 17    00 52 cd 03"
        + " ad 00 ff 12    00 00 00 27"
        + " 89 88",

        "0000000: 66  FSub\n0000001: 66  FSub\n0000002: 66  FSub\n0000003: 66  FSub\n0000004: 66  FSub\n"
          + " 0000005: ab 00 00"
          + "          00 00 00 11    00 00 00 03"
          + "          00 00 23 ab    ff ff ff fc"
          + "          00 00 23 17    00 52 cd 03"
          + "          ad 00 ff 12    00 00 00 27"
          + " LookupSwitch default:+17 (5 -> 22), 9131:-4 (5 -> 1), 8983:+5426435 (5 -> 5426440), -1392443630:+39 (5 -> 44)\n"

          + "0000040: 89 L2F\n0000041: 88 L2I"},

      {"81", "0000000: 81   LOr     "},
      {"71", "0000000: 71   LRem    "},
      {"ad", "0000000: ad   LReturn "},
      {"79", "0000000: 79   LShl    "},
      {"7b", "0000000: 7b   LShr    "},

      {"37 11", "0000000: 37 11  LStore 17"},
      {"3f   ", "0000000: 3f     LStore_0"},
      {"40   ", "0000000: 40     LStore_1"},
      {"41   ", "0000000: 41     LStore_2"},
      {"42   ", "0000000: 42     LStore_3"},

      {"65", "0000000: 65   LSub          "},
      {"7d", "0000000: 7d   LUShr         "},
      {"83", "0000000: 83   LXor          "},
      {"c2", "0000000: c2   MonitorEnter  "},
      {"c3", "0000000: c3   MonitorExit   "},

      {"c5 00 20 07", "0000000: c5 00 20 07   MultiANewArray  #32, dimensions 7"},

      {"bb 00 22", "0000000: bb 00 22   New  #34"},

      {"bc 04", "0000000: bc 04   NewArray  boolean "},
      {"bc 05", "0000000: bc 05   NewArray  char    "},
      {"bc 06", "0000000: bc 06   NewArray  float   "},
      {"bc 07", "0000000: bc 07   NewArray  double  "},
      {"bc 08", "0000000: bc 08   NewArray  byte    "},
      {"bc 09", "0000000: bc 09   NewArray  short   "},
      {"bc 0a", "0000000: bc 0a   NewArray  int     "},
      {"bc 0b", "0000000: bc 0b   NewArray  long    "},
      {"bc aa", "0000000: bc aa   NewArray  (Unknown Primitive Type Code)"},

      {"00", "0000000: 00   NOP"},
      {"57", "0000000: 57   POP"},
      {"58", "0000000: 58   POP2"},

      {"b5 01 aa", "0000000: b5 01 aa   PutField  #426"},
      {"b3 01 aa", "0000000: b3 01 aa   PutStatic #426"},

      {"a9 11", "0000000: a9 11  ret 17"},

      {"b1", "0000000: b1  Return"},
      {"35", "0000000: 35  SALoad"},
      {"56", "0000000: 56  SAStore"},

      {"11 ae 0c", "0000000: 11 ae 0c  SIPush -20980"},
      {"11 ff ff", "0000000: 11 ff ff  SIPush -1"},

      {"5f", "0000000: 5f   Swap"},
      {"c4", "0000000: c4   Wide"},


    };
  }

  @SneakyThrows
  @Test(dataProvider = "convertToAsm_DataProvider")
  public void convertToAsm(String strBytes, String expected) {

    byte[] bytes = ViewBytecode.parseBytes(strBytes);

    StringBuilder sb = new StringBuilder();

    try {
      ViewBytecode.convertToAsm(bytes, sb);
    } catch (RuntimeException e) {
      System.out.println(sb);
      throw e;
    }

    assertThat(toLines(sb)).isEqualTo(toLines(expected));
  }

  private @NonNull List<String> toLines(@NonNull CharSequence sb) {
    List<String> ret = new ArrayList<>();

    final String[] split = sb.toString().trim().split("\n");

    for (final String line : split) {
      ret.add(String.join(" ", line.trim().split("\\s+")));
    }

    return ret;
  }


}