package kz.greetgo.asm.view_bytecode;

import lombok.NonNull;
import lombok.SneakyThrows;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ViewBytecode {

  private static final Pattern LINE = Pattern.compile("[0-9abcdef]+:((\\s+[0-9abcdef]+)+)");

  @SneakyThrows
  public static byte[] readFormattedBytes(@NonNull String text) {

    final String[] lines = text.split("\n");

    byte[] bytes = new byte[lines.length * 16];
    int    index = 0;

    for (final String line : lines) {

      final Matcher matcher = LINE.matcher(line);
      if (matcher.matches()) {

        final String digitStr = matcher.group(1).replaceAll("\\s+", "");

        for (int i = 0; i < 16; i++) {
          if (i + 1 >= digitStr.length()) {
            break;
          }
          bytes[index++] = extractByte(digitStr, i * 2);
        }

      }
    }

    if (index < bytes.length) {
      byte[] ret = new byte[index];
      System.arraycopy(bytes, 0, ret, 0, index);
      return ret;
    }

    return bytes;
  }

  public static byte extractByte(@NonNull String digitStr, int offset) {
    final char c1 = digitStr.charAt(offset + 0);
    final char c2 = digitStr.charAt(offset + 1);

    int i1 = charToInt(c1);
    int i2 = charToInt(c2);

    return (byte) (i1 * 16 + i2);
  }

  private static int charToInt(char c) {
    if ('0' <= c && c <= '9') {
      return c - '0';
    }
    if ('a' <= c && c <= 'f') {
      return c - 'a' + 10;
    }
    if ('A' <= c && c <= 'F') {
      return c - 'A' + 10;
    }
    throw new RuntimeException("ZxYBWw1w5G :: Illegal argument exception");
  }

  @SuppressWarnings("SameParameterValue")
  private static @NonNull String toLenHex(int i, int len) {
    String s = Integer.toHexString(i);
    return s.length() < len ? "0".repeat(len - s.length()) + s : s;
  }

  @SuppressWarnings("SameParameterValue")
  private static @NonNull String toLen(int i, int len) {
    String s = "" + i;
    return s.length() < len ? "0".repeat(len - s.length()) + s : s;
  }

  public static @NonNull String byteToHex(byte b) {
    int i = b;
    if (i < 0) {
      i += 256;
    }
    final String s = Integer.toHexString(i);
    return s.length() == 1 ? "0" + s : s;
  }

  public static void printBytes(byte[] bytes, StringBuilder sb) {
    if (bytes == null) {
      return;
    }

    int i = 0;

    while (i < bytes.length) {

      int i2 = i + 16;
      if (i2 > bytes.length) {
        i2 = bytes.length;
      }

      sb.append(toLenHex(i, 7)).append(":");

      boolean printSpace = true;
      for (int j = i; j < i2; j++) {
        if (printSpace) {
          sb.append(' ');
        }
        printSpace = !printSpace;
        sb.append(byteToHex(bytes[j]));
      }

      i = i2;
      sb.append('\n');
    }

  }

  private interface Instruction {
    int printAsm(StringBuilder sb, byte[] bytes, int index);
  }

  private static final Map<Integer, Instruction> ALU = new HashMap<>();

  private static int wordToInt(byte[] bytes, int index) {
    int b2 = byteToInt(bytes, index + 0);
    int b1 = byteToInt(bytes, index + 1);
    return b1 + b2 * 256;
  }

  private static int wordToSignedInt(byte[] bytes, int index) {
    int b2  = byteToInt(bytes, index + 0);
    int b1  = byteToInt(bytes, index + 1);
    int ret = b1 + b2 * 256;
    return ret >= 256 * 128 ? ret - 256 * 256 : ret;
  }

  private static long dwordToSignedLong(byte[] bytes, int index) {
    int  b4  = byteToInt(bytes, index + 0);
    int  b3  = byteToInt(bytes, index + 1);
    int  b2  = byteToInt(bytes, index + 2);
    int  b1  = byteToInt(bytes, index + 3);
    long ret = b1 + b2 * 256L + b3 * 256L * 256L + b4 * 256L * 256L * 256L;
    return ret >= 256L * 256L * 256L * 128L ? ret - 256L * 256L * 256L * 256L : ret;
  }

  private static int byteToInt(byte[] bytes, int index) {
    int b = bytes[index];
    return b < 0 ? b + 256 : b;
  }

  public static void convertToAsm(byte[] bytes, StringBuilder sb) {
    int       index       = 0;
    final int bytesLength = bytes.length;
    while (index < bytesLength) {
      int b = bytes[index];
      if (b < 0) {
        b += 256;
      }

      final Instruction instruction = ALU.get(b);

      if (instruction == null) {
        throw new RuntimeException("KdCdfMN6KN :: Unknown opcode: hex=" + byteToHex((byte) b) + ", deg=" + b);
      }

      index += instruction.printAsm(sb, bytes, index);
    }
  }

  private static void printAsmPrefix(@NonNull StringBuilder sb, byte[] bytes, int pos, int count) {

    StringBuilder s = new StringBuilder();
    s.append(" ".repeat(4)).append(toLen(pos, 7)).append(':');

    for (int i = 0; i < count; i++) {
      s.append(' ').append(byteToHex(bytes[pos + i]));
    }

    final int LEN = 34;

    sb.append(s);

    if (s.length() < LEN) {
      sb.append(" ".repeat(LEN - s.length()));
    }


  }

  public static byte[] parseBytes(String str) {

    final String s = str.replaceAll("\\s+", "");
    if (s.length() % 2 != 0) {
      throw new RuntimeException("eG0D6D3hVU :: odd chars");
    }

    final int length = s.length() / 2;

    byte[] ret = new byte[length];

    for (int i = 0; i < length; i++) {
      ret[i] = extractByte(s, 2 * i);
    }

    return ret;
  }

  private static void alu(@NonNull String mnemonic) {
    final String[] split  = mnemonic.trim().split("\\s+");
    final int      opcode = Integer.parseInt(split[0], 16);
    ALU.put(opcode, (sb, bytes, index) -> {

      StringBuilder args = new StringBuilder();

      int argSize = 0;

      for (int ii = 2; ii < split.length; ii++) {
        args.append(ii == 2 ? "  " : ", ");
        int argIndex = index + 1 + argSize;
        switch (split[ii]) {

          case "var_byte_to_int" -> {
            args.append(byteToInt(bytes, argIndex));
            argSize++;
          }

          case "byte_to_int" -> {
            args.append(byteToInt(bytes, argIndex));
            argSize++;
          }

          case "dimensions_byte_to_int" -> {
            args.append("dimensions " + byteToInt(bytes, argIndex));
            argSize++;
          }

          case "arg_byte_to_int" -> {
            args.append("args " + byteToInt(bytes, argIndex));
            argSize++;
          }

          case "zero_byte" -> {
            args.append("0");
            argSize++;
          }

          case "ref_word_to_int" -> {
            args.append("#" + wordToInt(bytes, argIndex));
            argSize += 2;
          }

          case "ref_byte_to_int" -> {
            args.append("#" + byteToInt(bytes, argIndex));
            argSize += 1;
          }

          case "word_to_int" -> {
            args.append(wordToInt(bytes, argIndex));
            argSize += 2;
          }

          case "shift_word" -> {
            final int shift = wordToSignedInt(bytes, argIndex);
            args.append(shift < 0 ? "" + shift : "+" + shift);
            args.append(" (" + index + " -> " + (index + shift) + ")");
            argSize += 2;
          }

          case "word_to_signed_int" -> {
            final int value = wordToSignedInt(bytes, argIndex);
            args.append(value);
            argSize += 2;
          }

          case "shift_dword" -> {
            final long shift = dwordToSignedLong(bytes, argIndex);
            args.append(shift < 0 ? "" + shift : "+" + shift);
            args.append(" (" + index + " -> " + (index + shift) + ")");
            argSize += 4;
          }

          case "byte_to_signed_int" -> {
            final int value = byteToSignedInt(bytes, argIndex);
            args.append(value);
            argSize += 1;
          }

          case "primitive_type_byte_to_string" -> {
            final int value = byteToSignedInt(bytes, argIndex);
            args.append(switch (value) {
              case 4 -> "boolean";
              case 5 -> "char";
              case 6 -> "float";
              case 7 -> "double";
              case 8 -> "byte";
              case 9 -> "short";
              case 10 -> "int";
              case 11 -> "long";
              default -> "(Unknown Primitive Type Code)";
            });
            argSize += 1;
          }

          case "lookup_switch" -> {
            int ai = argIndex;
            while (ai % 4 != 0) {
              ai++;
              argSize++;
            }
            final long shiftDefault = dwordToSignedLong(bytes, ai);
            argSize += 4;
            ai += 4;
            args.append("default:");
            args.append(shiftDefault < 0 ? "" + shiftDefault : "+" + shiftDefault);
            args.append(" (" + index + " -> " + (index + shiftDefault) + ")");
            final int count = (int) dwordToSignedLong(bytes, ai);
            ai += 4;
            argSize += 4 * (2 * count + 1);
            for (int i = 0; i < count; i++) {
              final int match = (int) dwordToSignedLong(bytes, ai + 8 * i + 0);
              final int shift = (int) dwordToSignedLong(bytes, ai + 8 * i + 4);
              args.append(", ").append(match).append(':');
              args.append(shift < 0 ? "" + shift : "+" + shift);
              args.append(" (" + index + " -> " + (index + shift) + ")");
            }
          }

          default -> throw new RuntimeException("gqKTfaO9E6 :: Unknown arg=`" + split[ii] + "`");
        }
      }

      int size = 1 + argSize;

      printAsmPrefix(sb, bytes, index, size);

      if (sb.length() > 0 && sb.charAt(sb.length() - 1) != ' ') {
        sb.append(' ');
      }

      sb.append(split[1]).append(args).append('\n');

      return size;
    });
  }

  private static int byteToSignedInt(byte[] bytes, int argIndex) {
    return bytes[argIndex];
  }

  static {

    alu("32 AALoad");
    alu("53 AAStore");
    alu("01 AConst_NULL");

    alu("19 ALoad  var_byte_to_int");
    alu("2a ALoad_0");
    alu("2b ALoad_1");
    alu("2c ALoad_2");
    alu("2d ALoad_3");

    alu("bd ANewArray word_to_int");

    alu("b0 AReturn");

    alu("be ArrayLength");

    alu("3a AStore var_byte_to_int");
    alu("4b AStore_0");
    alu("4c AStore_1");
    alu("4d AStore_2");
    alu("4e AStore_3");

    alu("bf AThrow");

    alu("33 BALoad");
    alu("54 BAStore");

    alu("10 BIPush byte_to_int");

    alu("ca Breakpoint");

    alu("34 CALoad");
    alu("55 CAStore");
    alu("c0 CheckCast ref_word_to_int");

    alu("90 D2F");
    alu("8e D2I");
    alu("8f D2L");
    alu("63 DAdd");
    alu("31 DALoad");
    alu("52 DAStore");
    alu("98 DCmpG");
    alu("97 DCmpL");
    alu("0e DConst_0");
    alu("0f DConst_1");
    alu("6f DDiv");

    alu("18 DLoad var_byte_to_int");
    alu("26 DLoad_0");
    alu("27 DLoad_1");
    alu("28 DLoad_2");
    alu("29 DLoad_3");

    alu("6b DMul");
    alu("77 DNeg");
    alu("73 DRem");
    alu("af DReturn");

    alu("39 DStore var_byte_to_int");
    alu("47 DStore_0");
    alu("48 DStore_1");
    alu("49 DStore_2");
    alu("4a DStore_3");

    alu("67 DSub");
    alu("59 DUP");
    alu("5a DUP_X1");
    alu("5b DUP_X2");
    alu("5c DUP2");
    alu("5d DUP2_X1");
    alu("5e DUP2_X2");
    alu("8d F2D");
    alu("8b F2I");
    alu("8c F2L");
    alu("62 FAdd");
    alu("30 FALoad");
    alu("51 FAStore");
    alu("96 FCmpG");
    alu("95 FCmpL");
    alu("0b FConst_0");
    alu("0c FConst_1");
    alu("0d FConst_2");
    alu("6e FDiv");

    alu("17 FLoad var_byte_to_int");
    alu("22 FLoad_0");
    alu("23 FLoad_1");
    alu("24 FLoad_2");
    alu("25 FLoad_3");

    alu("6a FMul");
    alu("76 FNeg");
    alu("72 FRem");
    alu("ae FReturn");

    alu("38 FStore var_byte_to_int");
    alu("43 FStore_0");
    alu("44 FStore_1");
    alu("45 FStore_2");
    alu("46 FStore_3");

    alu("66 FSub");

    alu("b4 GetField ref_word_to_int");
    alu("b2 GetStatic ref_word_to_int");
    alu("a7 Goto shift_word");
    alu("c8 Goto_W shift_dword");

    alu("91   I2B");
    alu("92   I2C");
    alu("87   I2D");
    alu("86   I2F");
    alu("85   I2L");
    alu("93   I2S");
    alu("60   IAdd");
    alu("2e   IALoad");
    alu("7e   IAnd");
    alu("4f   IAStore");
    alu("02   IConst_m1");
    alu("03   IConst_0");
    alu("04   IConst_1");
    alu("05   IConst_2");
    alu("06   IConst_3");
    alu("07   IConst_4");
    alu("08   IConst_5");
    alu("6c   IDiv");

    alu("a5   if_ACmpEQ   shift_word");
    alu("a6   if_ACmpNE   shift_word");
    alu("9f   if_ICmpEQ   shift_word");
    alu("a2   if_ICmpGE   shift_word");
    alu("a3   if_ICmpGT   shift_word");
    alu("a4   if_ICmpLE   shift_word");
    alu("a1   if_ICmpLT   shift_word");
    alu("a0   if_ICmpNE   shift_word");
    alu("99   ifEQ        shift_word");
    alu("9c   ifGE        shift_word");
    alu("9d   ifGT        shift_word");
    alu("9e   ifLE        shift_word");
    alu("9b   ifLT        shift_word");
    alu("9a   ifNE        shift_word");
    alu("c7   ifNonNull   shift_word");
    alu("c6   ifNull      shift_word");

    alu("84   IInc  var_byte_to_int   byte_to_signed_int");

    alu("15 ILoad var_byte_to_int");
    alu("1a ILoad_0");
    alu("1b ILoad_1");
    alu("1c ILoad_2");
    alu("1d ILoad_3");

    alu("fe ImpDep1");
    alu("ff ImpDep2");

    alu("68 IMul");
    alu("74 INeg");

    alu("c1 InstanceOf ref_word_to_int");

    alu("ba InvokeDynamic     ref_word_to_int   zero_byte         zero_byte");
    alu("b9 InvokeInterface   ref_word_to_int   arg_byte_to_int   zero_byte");
    alu("b7 InvokeSpecial     ref_word_to_int");
    alu("b8 InvokeStatic      ref_word_to_int");
    alu("b6 InvokeVirtual     ref_word_to_int");

    alu("80 IOr");
    alu("70 IRem");
    alu("ac IReturn");
    alu("78 IShl");
    alu("7a IShr");

    alu("36 IStore var_byte_to_int");
    alu("3b IStore_0");
    alu("3c IStore_1");
    alu("3d IStore_2");
    alu("3e IStore_3");

    alu("a8 JSR shift_word");
    alu("c9 JSR_W shift_dword");

    alu("8a L2D      ");
    alu("89 L2F      ");
    alu("88 L2I      ");
    alu("61 LAdd     ");
    alu("2f LALoad   ");
    alu("7f LAnd     ");
    alu("50 LAStore  ");
    alu("94 LCmp     ");
    alu("09 LConst_0 ");
    alu("0a LConst_1 ");

    alu("12 LDC    ref_byte_to_int");
    alu("13 LDC_W  ref_word_to_int");
    alu("14 LDC2_W ref_word_to_int");

    alu("6d LDiv    ");
    alu("16 LLoad   var_byte_to_int");
    alu("1e LLoad_0 ");
    alu("1f LLoad_1 ");
    alu("20 LLoad_2 ");
    alu("21 LLoad_3 ");
    alu("69 LMul    ");
    alu("75 LNeg    ");

    alu("ab LookupSwitch lookup_switch");

    alu("81 LOr     ");
    alu("71 LRem    ");
    alu("ad LReturn ");
    alu("79 LShl    ");
    alu("7b LShr    ");

    alu("37 LStore var_byte_to_int");
    alu("3f LStore_0");
    alu("40 LStore_1");
    alu("41 LStore_2");
    alu("42 LStore_3");

    alu("65 LSub         ");
    alu("7d LUShr        ");
    alu("83 LXor         ");
    alu("c2 MonitorEnter ");
    alu("c3 MonitorExit  ");

    alu("c5 MultiANewArray  ref_word_to_int  dimensions_byte_to_int");

    alu("bb New  ref_word_to_int");

    alu("bc NewArray  primitive_type_byte_to_string");

    alu("00 NOP");
    alu("57 POP");
    alu("58 POP2");

    alu("b5 PutField  ref_word_to_int");
    alu("b3 PutStatic ref_word_to_int");

    alu("a9 ret var_byte_to_int");

    alu("b1 Return  ");
    alu("35 SALoad  ");
    alu("56 SAStore ");

    alu("11 SIPush word_to_signed_int");

    alu("5f Swap ");
    alu("c4 Wide ");

  }

}
