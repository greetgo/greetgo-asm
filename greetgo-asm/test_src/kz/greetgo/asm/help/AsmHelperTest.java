package kz.greetgo.asm.help;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmHelperTest {

  @Test
  public void tab_1() {
    StringBuilder sb = new StringBuilder();
    sb.append("test");

    //
    //
    AsmHelper.tab(sb, 20, true);
    //
    //

    assertThat(sb.length()).isEqualTo(20);

  }

  @Test
  public void tab_2() {
    final String testText = "Hello World";

    StringBuilder sb = new StringBuilder();
    sb.append(testText);

    //
    //
    AsmHelper.tab(sb, 3, false);
    //
    //

    assertThat(sb.length()).isEqualTo(testText.length());

  }

  @Test
  public void tab_3() {
    StringBuilder sb = new StringBuilder();
    sb.append("test");

    //
    //
    AsmHelper.tab(sb, 3, true);
    //
    //

    assertThat(sb.toString()).endsWith(" ");

  }
}