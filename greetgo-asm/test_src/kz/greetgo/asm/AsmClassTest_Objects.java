package kz.greetgo.asm;

import kz.greetgo.util.RND;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.assertj.core.data.Offset;
import org.objectweb.asm.Type;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static kz.greetgo.asm.help.AsmHelper.returnDesc;
import static kz.greetgo.asm.help.AsmHelper.voidDesc;
import static kz.greetgo.asm.AccessType.PUBLIC;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_Objects extends AsmClassTest_Parent {

  public static class TestError extends RuntimeException {}

  public static class Test1 {}

  @RequiredArgsConstructor
  public static class Test2 {
    public final long   trace_part_count;
    public final double distance_to_star_km;
    public final String star_name;

    public int seconds_before;
  }

  public static class Test2_Source {
    public long   trace_part_count;
    public double distance_to_star_km;
    public String star_name;
    public int    seconds_before;
  }

  public interface ParentInterface {

    void call_check_cast();

    void throw_error();

  }

  public interface TestInterface {
    @SuppressWarnings("unused")
    void testMethod();
  }

  public static class Test3 {
    public String message;

    public static String static_message;
    public static int    int_static_value;

    @SuppressWarnings("unused")
    public void setMessage(String message) {
      this.message = message;
    }
  }

  public static abstract class ParentClass implements ParentInterface {

    public static String static_field_str;

    public Test2 test2;

    public TestInterface testInterface;

    public double trace_part_length_km;

    public String status;

    @SuppressWarnings("unused")
    public void setStatus(String status) {
      this.status = status;
    }

    public Test3 test3;
  }

  Class<?>    generatedClass;
  ParentClass instance;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) throws Exception {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .impl(ParentInterface.class)
                               .ext(ParentClass.class)
                               .decimalByteCodeNumbers(true)
                               .build()) {

      cl.generateField("field_object", Object.class, PUBLIC, false);

      cl.generateDefaultConstructor();

      create__call_check_cast(cl);
      create__get_static_field(cl);
      create__set_static_field(cl);
      create__new_Test2(cl);

      create__throw_error(cl);
      create__invoke_interface(cl);
      create__invoke_virtual(cl);
      create__setMessage(cl);
      create__put_static_to_test3_str(cl);
      create__put_static_to_test3_int(cl);
      create__get_static_from_test3_int(cl);
      create__get_static_from_test3_str(cl);

      cl.finishClass();

      generatedClass = cl.load();

    }

    instance = (ParentClass) generatedClass.getConstructor().newInstance();
  }


  private void create__get_static_from_test3_str(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(String.class);
    final AsmMethod asm = cl.method("get_static_from_test3_str", methodDesc).access(PUBLIC).static_(true).build();
    asm.startMethodCode();
    {
      asm.GETSTATIC(Test3.class, "static_message", String.class);
      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__get_static_from_test3_int(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(int.class);
    final AsmMethod asm = cl.method("get_static_from_test3_int", methodDesc).access(PUBLIC).static_(true).build();
    asm.startMethodCode();
    {
      asm.GETSTATIC(Test3.class, "int_static_value", "I")
         .IRETURN();
    }
    asm.finishMethodCode();
  }

  private void create__put_static_to_test3_int(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class);
    final AsmMethod asm = cl.method("put_static_to_test3_int", methodDesc).access(PUBLIC).static_(true).build();
    asm.startMethodCode();
    {
      asm.ILOAD(0)
         .PUTSTATIC(Test3.class, "int_static_value", "I")
         .space(1)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__put_static_to_test3_str(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(String.class);
    final AsmMethod asm = cl.method("put_static_to_test3_str", methodDesc).access(PUBLIC).static_(true).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .PUTSTATIC(Test3.class, "static_message", String.class)
         .space(1)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__setMessage(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(String.class);
    final AsmMethod asm = cl.method("setMessage", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "test3", Test3.class);
      asm.ALOAD(1);
      asm.INVOKEVIRTUAL(Test3.class, "setMessage", voidDesc(String.class), false);

      asm.space(1);
      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__invoke_virtual(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(String.class);
    final AsmMethod asm = cl.method("invoke_virtual", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);
      asm.ALOAD(1);
      asm.INVOKEVIRTUAL(cl.classPath, "setStatus", voidDesc(String.class), false);

      asm.space(1);
      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__invoke_interface(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("invoke_interface", "()V").access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(cl.classPath, "testInterface", TestInterface.class)
         .ASTORE(1);

      asm.space(1)
         .ALOAD(1)
         .INVOKEINTERFACE(TestInterface.class, "testMethod", "()V", true)
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__throw_error(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("throw_error", "()V").access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.NEW(TestError.class)
         .DUP()
         .INVOKESPECIAL(TestError.class, "<init>", "()V", false)
         .ATHROW()
         .space(1);
    }
    asm.finishMethodCode();
  }

  private void create__new_Test2(@NonNull AsmClass cl) {
    /*
                                this                   =     0
     argument     Test2_Source  arg0                   =     1
     local        long          trace_part_count       =     2
     local        double        distance_to_star_km    =     4
     local        String        star_name              =     6
     local        int           seconds_before         =     7
     local        double        trace_part_length_km   =     8
     local        Test2         result                 =    10
     */
    @NonNull String methodDesc = voidDesc(Test2_Source.class);
    final AsmMethod asm = cl.method("new_Test2", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1); // Test2_Source arg0
      asm.GETFIELD(Test2_Source.class, "trace_part_count", long.class);
      asm.LSTORE(2); // long trace_part_count

      asm.space(1);
      asm.ALOAD(1); // Test2_Source arg0
      asm.GETFIELD(Test2_Source.class, "distance_to_star_km", Type.getDescriptor(double.class));
      asm.DSTORE(4); // double distance_to_star_km

      asm.space(1);
      asm.ALOAD(1); // Test2_Source arg0
      asm.GETFIELD(Test2_Source.class, "star_name", String.class);
      asm.ASTORE(6); // String star_name

      asm.space(1);
      asm.ALOAD(1); // Test2_Source arg0
      asm.GETFIELD(Test2_Source.class, "seconds_before", Type.getDescriptor(int.class));
      asm.ISTORE(7); // int seconds_before

      asm.space(1); // Test2_Source arg0
      asm.DLOAD(4); // double distance_to_star_km
      asm.LLOAD(2); // long   trace_part_count
      asm.L2D();
      asm.DDIV();
      asm.DSTORE(8); // double trace_part_length_km

      asm.space(1);
      asm.NEW(Test2.class);
      asm.DUP();
      asm.LLOAD(2); // long   trace_part_count
      asm.DLOAD(4); // double distance_to_star_km
      asm.ALOAD(6); // String star_name
      asm.INVOKESPECIAL(Test2.class, "<init>", voidDesc(long.class, double.class, String.class), false);
      asm.ASTORE(10); // Test2 result

      asm.space(1);
      asm.ALOAD(10); // Test2 result
      asm.ILOAD(7);  // int   seconds_before
      asm.PUTFIELD(Test2.class, "seconds_before", int.class);

      asm.space(1);
      asm.ALOAD(0);  //       this    (GeneratedClass extends ParentClass)
      asm.ALOAD(10); // Test2 result
      asm.PUTFIELD(cl.classPath, "test2", Test2.class);

      asm.space(1);
      asm.ALOAD(0); //        this                  (GeneratedClass extends ParentClass)
      asm.DLOAD(8); // double trace_part_length_km
      asm.PUTFIELD(cl.classPath, "trace_part_length_km", double.class);

      asm.space(1);
      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__set_static_field(@NonNull AsmClass cl) {
    final String    desc = "(" + Type.getDescriptor(String.class) + ")V";
    final AsmMethod asm  = cl.method("set_static_field", desc).access(PUBLIC).static_(true).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .PUTSTATIC(cl.classPath, "static_field_str", String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__get_static_field(@NonNull AsmClass cl) {
    final String    desc = "()" + Type.getDescriptor(String.class);
    final AsmMethod asm  = cl.method("get_static_field", desc).access(PUBLIC).static_(true).build();
    asm.startMethodCode();
    {
      asm.GETSTATIC(cl.classPath, "static_field_str", String.class)
         .ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__call_check_cast(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("call_check_cast", "()V").access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(cl.classPath, "field_object", Object.class)
         .CHECKCAST(Test1.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_check_cast__OK() throws Exception {
    generatedClass.getField("field_object").set(instance, new Test1());
    instance.call_check_cast();
  }

  @Test(expectedExceptions = ClassCastException.class)
  public void call_check_cast__ERR() throws Exception {
    generatedClass.getField("field_object").set(instance, "Left");
    instance.call_check_cast();
  }

  @Test
  public void call_check_cast__NULL() throws Exception {
    generatedClass.getField("field_object").set(instance, null);
    instance.call_check_cast();
  }

  @Test
  public void get_static_field() throws Exception {

    String tst = "Some test str 5hj4gv325gv345";

    ParentClass.static_field_str = tst;

    final Object fieldValue = generatedClass.getMethod("get_static_field").invoke(null);

    assertThat(fieldValue).isEqualTo(tst);
  }

  @Test
  public void set_static_field() throws Exception {

    String tst = "Some test str kh5v42gh5vhj";

    generatedClass.getMethod("set_static_field", String.class).invoke(null, tst);

    assertThat(ParentClass.static_field_str).isEqualTo(tst);

  }

  @Test
  public void newTest2() throws Exception {

    Test2_Source src = new Test2_Source();

    src.trace_part_count    = RND.plusLong(54_325_478_623_465_296L);
    src.distance_to_star_km = 4.43215670045E+34;
    src.star_name           = "Стожары";
    src.seconds_before      = RND.plusInt(1_000_000_000);

    generatedClass.getMethod("new_Test2", Test2_Source.class).invoke(instance, src);

    assertThat(instance.test2).isNotNull();
    assertThat(instance.test2.trace_part_count).isEqualTo(src.trace_part_count);
    assertThat(instance.test2.distance_to_star_km).isEqualTo(src.distance_to_star_km);
    assertThat(instance.test2.star_name).isEqualTo(src.star_name);
    assertThat(instance.test2.seconds_before).isEqualTo(src.seconds_before);

    assertThat(instance.trace_part_length_km).isEqualTo(src.distance_to_star_km / src.trace_part_count, Offset.offset(1e-10));

  }

  @Test(expectedExceptions = TestError.class)
  public void throw_error() {
    instance.throw_error();
  }

  @Test
  public void invoke_interface() throws Exception {

    String[] str = new String[]{"Left Value"};

    instance.testInterface = () -> str[0] = "Right Value";

    generatedClass.getMethod("invoke_interface").invoke(instance);

    assertThat(str[0]).isEqualTo("Right Value");

  }

  @Test
  public void invoke_virtual() throws Exception {

    instance.status = "Left";

    generatedClass.getMethod("invoke_virtual", String.class).invoke(instance, "Super Status");

    assertThat(instance.status).isEqualTo("Super Status");

  }

  @Test
  public void setMessage() throws Exception {

    instance.test3         = new Test3();
    instance.test3.message = "Left Message";

    generatedClass.getMethod("setMessage", String.class).invoke(instance, "OK");

    assertThat(instance.test3.message).isEqualTo("OK");

  }

  @Test
  public void put_static_to_test3_str() throws Exception {

    Test3.static_message = "Left message";

    generatedClass.getMethod("put_static_to_test3_str", String.class).invoke(instance, "OK");

    assertThat(Test3.static_message).isEqualTo("OK");

  }

  @Test
  public void put_static_to_test3_int() throws Exception {

    Test3.int_static_value = 11;

    generatedClass.getMethod("put_static_to_test3_int", int.class).invoke(instance, 234);

    assertThat(Test3.int_static_value).isEqualTo(234);

  }

  @Test
  public void get_static_from_test3_str() throws Exception {

    Test3.static_message = "Reading Message";

    String value = (String) generatedClass.getMethod("get_static_from_test3_str").invoke(instance);

    assertThat(value).isEqualTo("Reading Message");

  }

  @Test
  public void get_static_from_test3_int() throws Exception {

    Test3.int_static_value = 56426;

    final int value = (Integer) generatedClass.getMethod("get_static_from_test3_int").invoke(instance);

    assertThat(value).isEqualTo(56426);

  }

}
