package kz.greetgo.asm;

import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_LOAD_STORE extends AsmClassTest_Parent {

  @Test
  public void moveAllTypes() throws Exception {
    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("moveAllTypes"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("src_ref", Object.class, AccessType.PUBLIC, false);
      cl.generateField("src_int", int.class, AccessType.PUBLIC, false);
      cl.generateField("src_long", long.class, AccessType.PUBLIC, false);
      cl.generateField("src_float", float.class, AccessType.PUBLIC, false);
      cl.generateField("src_double", double.class, AccessType.PUBLIC, false);

      cl.generateField("dest_ref", Object.class, AccessType.PUBLIC, false);
      cl.generateField("dest_int", int.class, AccessType.PUBLIC, false);
      cl.generateField("dest_long", long.class, AccessType.PUBLIC, false);
      cl.generateField("dest_float", float.class, AccessType.PUBLIC, false);
      cl.generateField("dest_double", double.class, AccessType.PUBLIC, false);

      {
        final AsmMethod asm = cl.method("move", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .GETFIELD(cl.classPath, "src_ref", Object.class)
             .STORE(1, Object.class);

          asm.ALOAD(0)
             .GETFIELD(cl.classPath, "src_long", long.class)
             .STORE(2, long.class)
             .space(1);

          asm.ALOAD(0)
             .GETFIELD(cl.classPath, "src_int", int.class)
             .STORE(4, int.class);

          asm.ALOAD(0)
             .GETFIELD(cl.classPath, "src_double", double.class)
             .STORE(5, double.class);

          asm.ALOAD(0)
             .GETFIELD(cl.classPath, "src_float", float.class)
             .STORE(7, float.class)
             .space(2);

          asm.ALOAD(0)
             .LOAD(1, Object.class)
             .PUTFIELD(cl.classPath, "dest_ref", Object.class);

          asm.ALOAD(0)
             .LOAD(2, long.class)
             .PUTFIELD(cl.classPath, "dest_long", long.class);

          asm.ALOAD(0)
             .LOAD(4, int.class)
             .PUTFIELD(cl.classPath, "dest_int", int.class);

          asm.ALOAD(0)
             .LOAD(5, double.class)
             .PUTFIELD(cl.classPath, "dest_double", double.class);

          asm.ALOAD(0)
             .LOAD(7, float.class)
             .PUTFIELD(cl.classPath, "dest_float", float.class);

          asm.space(1)
             .RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    Object src_ref    = "Hello World";
    int    src_int    = 34215;
    long   src_long   = 437862585435L;
    float  src_float  = 4325.432f;
    double src_double = 54325435.654d;

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("src_ref").set(instance, src_ref);
    generatedClass.getField("src_int").set(instance, src_int);
    generatedClass.getField("src_long").set(instance, src_long);
    generatedClass.getField("src_float").set(instance, src_float);
    generatedClass.getField("src_double").set(instance, src_double);

    generatedClass.getMethod("move").invoke(instance);

    Object dest_ref    = generatedClass.getField("dest_ref").get(instance);
    int    dest_int    = (Integer) generatedClass.getField("dest_int").get(instance);
    long   dest_long   = (Long) generatedClass.getField("dest_long").get(instance);
    float  dest_float  = (Float) generatedClass.getField("dest_float").get(instance);
    double dest_double = (Double) generatedClass.getField("dest_double").get(instance);

    assertThat(dest_ref).isEqualTo(src_ref);
    assertThat(dest_int).isEqualTo(src_int);
    assertThat(dest_long).isEqualTo(src_long);
    assertThat(dest_float).isEqualTo(src_float);
    assertThat(dest_double).isEqualTo(src_double);
  }

  @Test
  public void moveSeveralInt() throws Exception {
    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("moveSeveralInt"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("src_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("src_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("src_3", int.class, AccessType.PUBLIC, false);
      cl.generateField("src_4", int.class, AccessType.PUBLIC, false);
      cl.generateField("src_5", int.class, AccessType.PUBLIC, false);
      cl.generateField("src_6", int.class, AccessType.PUBLIC, false);

      cl.generateField("dest_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("dest_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("dest_3", int.class, AccessType.PUBLIC, false);
      cl.generateField("dest_4", int.class, AccessType.PUBLIC, false);
      cl.generateField("dest_5", int.class, AccessType.PUBLIC, false);
      cl.generateField("dest_6", int.class, AccessType.PUBLIC, false);

      {
        //noinspection SpellCheckingInspection
        final AsmMethod asm = cl.method("move", "(IIIIIIL" + cl.classPath + ";)V").static_(true).build();
        asm.startMethodCode();
        {
          asm.ALOAD(6)
             .GETFIELD(cl.classPath, "src_1", int.class)
             .ISTORE(0)
             .space(1);

          asm.ALOAD(6)
             .GETFIELD(cl.classPath, "src_2", int.class)
             .ISTORE(1)
             .space(1);

          asm.ALOAD(6)
             .GETFIELD(cl.classPath, "src_3", int.class)
             .ISTORE(2)
             .space(1);

          asm.ALOAD(6)
             .GETFIELD(cl.classPath, "src_4", int.class)
             .ISTORE(3)
             .space(1);

          asm.ALOAD(6)
             .GETFIELD(cl.classPath, "src_5", int.class)
             .ISTORE(4)
             .space(1);

          asm.ALOAD(6);
          asm.GETFIELD(cl.classPath, "src_6", int.class);
          asm.ISTORE(5)
             .space(1);

          asm.space(2);

          asm.ALOAD(6);
          asm.ILOAD(0);
          asm.PUTFIELD(cl.classPath, "dest_1", int.class);

          asm.ALOAD(6);
          asm.ILOAD(1);
          asm.PUTFIELD(cl.classPath, "dest_2", int.class);

          asm.ALOAD(6);
          asm.ILOAD(2);
          asm.PUTFIELD(cl.classPath, "dest_3", int.class);

          asm.ALOAD(6);
          asm.ILOAD(3);
          asm.PUTFIELD(cl.classPath, "dest_4", int.class);

          asm.ALOAD(6);
          asm.ILOAD(4);
          asm.PUTFIELD(cl.classPath, "dest_5", int.class);

          asm.ALOAD(6);
          asm.ILOAD(5);
          asm.PUTFIELD(cl.classPath, "dest_6", int.class);

          asm.space(1);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("src_1").set(instance, 345);
    generatedClass.getField("src_2").set(instance, 675);
    generatedClass.getField("src_3").set(instance, 384);
    generatedClass.getField("src_4").set(instance, 195);
    generatedClass.getField("src_5").set(instance, 327);
    generatedClass.getField("src_6").set(instance, 756);

    final Method move = generatedClass.getMethod("move", int.class, int.class, int.class, int.class, int.class, int.class, generatedClass);

    move.invoke(0, 0, 0, 0, 0, 0, 0, instance);

    int dest_1 = (Integer) generatedClass.getField("dest_1").get(instance);
    int dest_2 = (Integer) generatedClass.getField("dest_2").get(instance);
    int dest_3 = (Integer) generatedClass.getField("dest_3").get(instance);
    int dest_4 = (Integer) generatedClass.getField("dest_4").get(instance);
    int dest_5 = (Integer) generatedClass.getField("dest_5").get(instance);
    int dest_6 = (Integer) generatedClass.getField("dest_6").get(instance);

    assertThat(dest_1).isEqualTo(345);
    assertThat(dest_2).isEqualTo(675);
    assertThat(dest_3).isEqualTo(384);
    assertThat(dest_4).isEqualTo(195);
    assertThat(dest_5).isEqualTo(327);
    assertThat(dest_6).isEqualTo(756);
  }

}
