package kz.greetgo.asm;

import lombok.NonNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_TypeConversions extends AsmClassTest_Parent {
  Class<?> generatedClass;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) {
    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .build()) {

      cl.generateField("field_int", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_long", long.class, AccessType.PUBLIC, false);
      cl.generateField("field_float", float.class, AccessType.PUBLIC, false);
      cl.generateField("field_double", double.class, AccessType.PUBLIC, false);
      cl.generateField("field_byte", byte.class, AccessType.PUBLIC, false);
      cl.generateField("field_char", char.class, AccessType.PUBLIC, false);
      cl.generateField("field_short", short.class, AccessType.PUBLIC, false);

      cl.generateDefaultConstructor();

      {
        final AsmMethod asm = cl.method("call__I2L", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int", int.class);
          asm.space(2)
             .I2L()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_long", long.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__I2F", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int", int.class);
          asm.space(2)
             .I2F()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_float", float.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__I2D", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int", int.class);
          asm.space(2)
             .I2D()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_double", double.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__I2B", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int", int.class);
          asm.space(2);
          asm.I2B()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_byte", byte.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__I2C", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int", int.class);
          asm.space(2)
             .I2C()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_char", char.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__I2S", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int", int.class);
          asm.space(2)
             .I2S()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_short", short.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__L2I", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long", long.class);
          asm.space(2)
             .L2I()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_int", int.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__L2F", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long", long.class);
          asm.space(2)
             .L2F()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_float", float.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__L2D", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long", long.class);
          asm.space(2)
             .L2D()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_double", double.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__F2I", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float", float.class);
          asm.space(2)
             .F2I()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_int", int.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__F2L", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float", float.class);
          asm.space(2)
             .F2L()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_long", long.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__F2D", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float", float.class);
          asm.space(2);
          asm.F2D()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_double", double.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__D2I", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double", double.class);
          asm.space(2);
          asm.D2I()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_int", int.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__D2L", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double", double.class);
          asm.space(2);
          asm.D2L()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_long", long.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call__D2F", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double", double.class);
          asm.space(2);
          asm.D2F()
             .space(2);
          asm.PUTFIELD(cl.classPath, "field_float", float.class);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }


      cl.finishClass();

      generatedClass = cl.load();
    }
  }

  @Test
  public void conversion_I2L() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_int").set(instance, 13);
    generatedClass.getMethod("call__I2L").invoke(instance);
    long result = (Long) generatedClass.getField("field_long").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_I2F() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_int").set(instance, 13);
    generatedClass.getMethod("call__I2F").invoke(instance);
    float result = (Float) generatedClass.getField("field_float").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_I2D() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_int").set(instance, 13);
    generatedClass.getMethod("call__I2D").invoke(instance);
    double result = (Double) generatedClass.getField("field_double").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_I2B() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_int").set(instance, 13);
    generatedClass.getMethod("call__I2B").invoke(instance);
    byte result = (Byte) generatedClass.getField("field_byte").get(instance);
    assertThat(result).isEqualTo((byte) 13);
  }

  @Test
  public void conversion_I2C() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_int").set(instance, 13);
    generatedClass.getMethod("call__I2C").invoke(instance);
    char result = (Character) generatedClass.getField("field_char").get(instance);
    assertThat(result).isEqualTo((char) 13);
  }

  @Test
  public void conversion_I2S() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_int").set(instance, 13);
    generatedClass.getMethod("call__I2S").invoke(instance);
    short result = (Short) generatedClass.getField("field_short").get(instance);
    assertThat(result).isEqualTo((short) 13);
  }

  @Test
  public void conversion_L2I() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_long").set(instance, 13);
    generatedClass.getMethod("call__L2I").invoke(instance);
    int result = (Integer) generatedClass.getField("field_int").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_L2F() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_long").set(instance, 13);
    generatedClass.getMethod("call__L2F").invoke(instance);
    float result = (Float) generatedClass.getField("field_float").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_L2D() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_long").set(instance, 13);
    generatedClass.getMethod("call__L2D").invoke(instance);
    double result = (Double) generatedClass.getField("field_double").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_F2I() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_float").set(instance, 13);
    generatedClass.getMethod("call__F2I").invoke(instance);
    int result = (Integer) generatedClass.getField("field_int").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_F2L() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_float").set(instance, 13);
    generatedClass.getMethod("call__F2L").invoke(instance);
    long result = (Long) generatedClass.getField("field_long").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_F2D() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_float").set(instance, 13);
    generatedClass.getMethod("call__F2D").invoke(instance);
    double result = (Double) generatedClass.getField("field_double").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_D2I() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_double").set(instance, 13);
    generatedClass.getMethod("call__D2I").invoke(instance);
    int result = (Integer) generatedClass.getField("field_int").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_D2L() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_double").set(instance, 13);
    generatedClass.getMethod("call__D2L").invoke(instance);
    long result = (Long) generatedClass.getField("field_long").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void conversion_D2F() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_double").set(instance, 13);
    generatedClass.getMethod("call__D2F").invoke(instance);
    float result = (Float) generatedClass.getField("field_float").get(instance);
    assertThat(result).isEqualTo(13);
  }


}
