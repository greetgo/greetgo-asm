package kz.greetgo.asm;

import lombok.NonNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_Arithmetic extends AsmClassTest_Parent {

  Class<?> generatedClass;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) {
    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("field_int_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_int_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("result_int", int.class, AccessType.PUBLIC, false);

      cl.generateField("field_long_1", long.class, AccessType.PUBLIC, false);
      cl.generateField("field_long_2", long.class, AccessType.PUBLIC, false);
      cl.generateField("result_long", long.class, AccessType.PUBLIC, false);

      cl.generateField("field_float_1", float.class, AccessType.PUBLIC, false);
      cl.generateField("field_float_2", float.class, AccessType.PUBLIC, false);
      cl.generateField("result_float", float.class, AccessType.PUBLIC, false);

      cl.generateField("field_double_1", double.class, AccessType.PUBLIC, false);
      cl.generateField("field_double_2", double.class, AccessType.PUBLIC, false);
      cl.generateField("result_double", double.class, AccessType.PUBLIC, false);

      {
        final AsmMethod asm = cl.method("call_IADD", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class)
             .IADD()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LADD", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LADD()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_FADD", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_float_1", float.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float_2", float.class);
          asm.FADD()
             .PUTFIELD(cl.classPath, "result_float", float.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_DADD", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_double_1", double.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double_2", double.class);
          asm.DADD()
             .PUTFIELD(cl.classPath, "result_double", double.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_ISUB", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.ISUB()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LSUB", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LSUB()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_FSUB", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_float_1", float.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float_2", float.class);
          asm.FSUB()
             .PUTFIELD(cl.classPath, "result_float", float.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_DSUB", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_double_1", double.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double_2", double.class);
          asm.DSUB()
             .PUTFIELD(cl.classPath, "result_double", double.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_IMUL", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.IMUL()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LMUL", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LMUL()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_FMUL", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_float_1", float.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float_2", float.class);
          asm.FMUL()
             .PUTFIELD(cl.classPath, "result_float", float.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_DMUL", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_double_1", double.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double_2", double.class);
          asm.DMUL()
             .PUTFIELD(cl.classPath, "result_double", double.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_IDIV", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.IDIV()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LDIV", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LDIV()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_FDIV", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_float_1", float.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float_2", float.class);
          asm.FDIV()
             .PUTFIELD(cl.classPath, "result_float", float.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_DDIV", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_double_1", double.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double_2", double.class);
          asm.DDIV()
             .PUTFIELD(cl.classPath, "result_double", double.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_IREM", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.IREM()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LREM", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LREM()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_FREM", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_float_1", float.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float_2", float.class);
          asm.FREM()
             .PUTFIELD(cl.classPath, "result_float", float.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_DREM", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_double_1", double.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double_2", double.class);
          asm.DREM()
             .PUTFIELD(cl.classPath, "result_double", double.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_IAND", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.IAND()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LAND", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LAND()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_IOR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.IOR()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LOR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LOR()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_IXOR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.IXOR()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_LXOR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long_2", long.class);
          asm.LXOR()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }


      {
        final AsmMethod asm = cl.method("call_ISHL", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.ISHL()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LSHL", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.LSHL()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ISHR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.ISHR()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LSHR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.LSHR()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_IUSHR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.IUSHR()
             .PUTFIELD(cl.classPath, "result_int", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LUSHR", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "field_long_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.LUSHR()
             .PUTFIELD(cl.classPath, "result_long", long.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_IINC", "()V").build();
        asm.startMethodCode();
        {
          asm.NOP();

          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_1", int.class);
          asm.ISTORE(1);

          asm.NOP();

          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int_2", int.class);
          asm.ISTORE(2);

          asm.NOP();

          asm.IINC(1, +13)
             .IINC(2, -17);

          asm.NOP();

          asm.ALOAD(0);
          asm.ILOAD(1)
             .PUTFIELD(cl.classPath, "field_int_1", int.class);

          asm.NOP();

          asm.ALOAD(0);
          asm.ILOAD(2)
             .PUTFIELD(cl.classPath, "field_int_2", int.class);

          asm.NOP()
             .RETURN();
        }
        asm.finishMethodCode();
      }


      cl.finishClass();

      generatedClass = cl.load();

    }
  }

  @Test
  public void arithmetic_IADD() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 17);
    generatedClass.getField("field_int_2").set(instance, 5000);
    generatedClass.getMethod("call_IADD").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(5017);
  }

  @Test
  public void arithmetic_LADD() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 17);
    generatedClass.getField("field_long_2").set(instance, 5000);
    generatedClass.getMethod("call_LADD").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(5017);
  }

  @Test
  public void arithmetic_FADD() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_float_1").set(instance, 17);
    generatedClass.getField("field_float_2").set(instance, 5000);
    generatedClass.getMethod("call_FADD").invoke(instance);
    final float result = (Float) generatedClass.getField("result_float").get(instance);
    assertThat(result).isEqualTo(5017);
  }

  @Test
  public void arithmetic_DADD() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_double_1").set(instance, 17);
    generatedClass.getField("field_double_2").set(instance, 5000);
    generatedClass.getMethod("call_DADD").invoke(instance);
    final double result = (Double) generatedClass.getField("result_double").get(instance);
    assertThat(result).isEqualTo(5017);
  }

  @Test
  public void arithmetic_ISUB() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 17);
    generatedClass.getField("field_int_2").set(instance, 5000);
    generatedClass.getMethod("call_ISUB").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(-4983);
  }

  @Test
  public void arithmetic_LSUB() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 17);
    generatedClass.getField("field_long_2").set(instance, 5000);
    generatedClass.getMethod("call_LSUB").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(-4983L);
  }

  @Test
  public void arithmetic_FSUB() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_float_1").set(instance, 17);
    generatedClass.getField("field_float_2").set(instance, 5000);
    generatedClass.getMethod("call_FSUB").invoke(instance);
    final float result = (Float) generatedClass.getField("result_float").get(instance);
    assertThat(result).isEqualTo(-4983.0f);
  }

  @Test
  public void arithmetic_DSUB() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_double_1").set(instance, 17);
    generatedClass.getField("field_double_2").set(instance, 5000);
    generatedClass.getMethod("call_DSUB").invoke(instance);
    final double result = (Double) generatedClass.getField("result_double").get(instance);
    assertThat(result).isEqualTo(-4983.0);
  }

  @Test
  public void arithmetic_IMUL() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 17);
    generatedClass.getField("field_int_2").set(instance, 5000);
    generatedClass.getMethod("call_IMUL").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(85000);
  }

  @Test
  public void arithmetic_LMUL() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 17);
    generatedClass.getField("field_long_2").set(instance, 5000);
    generatedClass.getMethod("call_LMUL").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(85000);
  }

  @Test
  public void arithmetic_FMUL() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_float_1").set(instance, 17);
    generatedClass.getField("field_float_2").set(instance, 5000);
    generatedClass.getMethod("call_FMUL").invoke(instance);
    final float result = (Float) generatedClass.getField("result_float").get(instance);
    assertThat(result).isEqualTo(85000.0f);
  }

  @Test
  public void arithmetic_DMUL() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_double_1").set(instance, 17);
    generatedClass.getField("field_double_2").set(instance, 5000);
    generatedClass.getMethod("call_DMUL").invoke(instance);
    final double result = (Double) generatedClass.getField("result_double").get(instance);
    assertThat(result).isEqualTo(85000.0);
  }

  @Test
  public void arithmetic_IDIV() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 5000);
    generatedClass.getField("field_int_2").set(instance, 17);
    generatedClass.getMethod("call_IDIV").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(294);
  }

  @Test
  public void arithmetic_LDIV() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 5984236472565433543L);
    generatedClass.getField("field_long_2").set(instance, 515436546L);
    generatedClass.getMethod("call_LDIV").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(11610035258L);
  }

  @Test
  public void arithmetic_FDIV() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_float_1").set(instance, 17);
    generatedClass.getField("field_float_2").set(instance, 5000);
    generatedClass.getMethod("call_FDIV").invoke(instance);
    final float result = (Float) generatedClass.getField("result_float").get(instance);
    assertThat(result).isEqualTo(0.0034f);
  }

  @Test
  public void arithmetic_DDIV() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_double_1").set(instance, 17);
    generatedClass.getField("field_double_2").set(instance, 5000);
    generatedClass.getMethod("call_DDIV").invoke(instance);
    final double result = (Double) generatedClass.getField("result_double").get(instance);
    assertThat(result).isEqualTo(0.0034);
  }

  @Test
  public void arithmetic_IREM() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 5011);
    generatedClass.getField("field_int_2").set(instance, 119);
    generatedClass.getMethod("call_IREM").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(13);
  }

  @Test
  public void arithmetic_LREM() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 5000);
    generatedClass.getField("field_long_2").set(instance, 117);
    generatedClass.getMethod("call_LREM").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(86L);
  }

  @Test
  public void arithmetic_FREM() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_float_1").set(instance, 17);
    generatedClass.getField("field_float_2").set(instance, 5000);
    generatedClass.getMethod("call_FREM").invoke(instance);
    final float result = (Float) generatedClass.getField("result_float").get(instance);
    assertThat(result).isEqualTo(17.0f);
  }

  @Test
  public void arithmetic_DREM() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_double_1").set(instance, 17);
    generatedClass.getField("field_double_2").set(instance, 5000);
    generatedClass.getMethod("call_DREM").invoke(instance);
    final double result = (Double) generatedClass.getField("result_double").get(instance);
    assertThat(result).isEqualTo(17.0);
  }

  @Test
  public void arithmetic_IAND() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 43256);
    generatedClass.getField("field_int_2").set(instance, 54365);
    generatedClass.getMethod("call_IAND").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(32856);
  }

  @Test
  public void arithmetic_LAND() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 4356724723564L);
    generatedClass.getField("field_long_2").set(instance, 1014654631454L);
    generatedClass.getMethod("call_LAND").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(979789450764L);
  }

  @Test
  public void arithmetic_IOR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 17);
    generatedClass.getField("field_int_2").set(instance, 5000);
    generatedClass.getMethod("call_IOR").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(5017);
  }

  @Test
  public void arithmetic_LOR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 17);
    generatedClass.getField("field_long_2").set(instance, 5000);
    generatedClass.getMethod("call_LOR").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(5017);
  }

  @Test
  public void arithmetic_IXOR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 17);
    generatedClass.getField("field_int_2").set(instance, 5000);
    generatedClass.getMethod("call_IXOR").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(5017);
  }

  @Test
  public void arithmetic_LXOR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 17);
    generatedClass.getField("field_long_2").set(instance, 5000);
    generatedClass.getMethod("call_LXOR").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(5017);
  }


  @Test
  public void arithmetic_ISHL() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 111111);
    generatedClass.getField("field_int_2").set(instance, 12);
    generatedClass.getMethod("call_ISHL").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(455110656);
  }

  @Test
  public void arithmetic_LSHL() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 111111);
    generatedClass.getField("field_int_2").set(instance, 14);
    generatedClass.getMethod("call_LSHL").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(1820442624L);
  }

  @Test
  public void arithmetic_ISHR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 432145423);
    generatedClass.getField("field_int_2").set(instance, 13);
    generatedClass.getMethod("call_ISHR").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(52752);
  }

  @Test
  public void arithmetic_LSHR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 543245325L);
    generatedClass.getField("field_int_2").set(instance, 17);
    generatedClass.getMethod("call_LSHR").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(4144L);
  }

  @Test
  public void arithmetic_IUSHR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 543254325);
    generatedClass.getField("field_int_2").set(instance, 7);
    generatedClass.getMethod("call_IUSHR").invoke(instance);
    final int result = (Integer) generatedClass.getField("result_int").get(instance);
    assertThat(result).isEqualTo(4244174);
  }

  @Test
  public void arithmetic_LUSHR() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long_1").set(instance, 542656346456L);
    generatedClass.getField("field_int_2").set(instance, 23);
    generatedClass.getMethod("call_LUSHR").invoke(instance);
    final long result = (Long) generatedClass.getField("result_long").get(instance);
    assertThat(result).isEqualTo(64689L);
  }

  @Test
  public void arithmetic_IINC() throws Exception {

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int_1").set(instance, 12_000);
    generatedClass.getField("field_int_2").set(instance, 40_000);
    generatedClass.getMethod("call_IINC").invoke(instance);
    final int int1 = (Integer) generatedClass.getField("field_int_1").get(instance);
    final int int2 = (Integer) generatedClass.getField("field_int_2").get(instance);
    assertThat(int1).isEqualTo(12_013);
    assertThat(int2).isEqualTo(39_983);
  }

}
