package kz.greetgo.asm;

import kz.greetgo.asm.help.AsmByteHelper;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmByteHelperTest {

  @Test
  public void intToHex() {

    //
    //
    String result = AsmByteHelper.intToHex(11, 4);
    //
    //

    assertThat(result).isEqualTo("000b");

  }

  @Test
  public void bytesToHex() {

    //
    //
    String result = AsmByteHelper.bytesToHex(new byte[]{1, 0, -1, 3, 11, 127, -128});
    //
    //

    assertThat(result).isEqualTo("01 00 ff 03 0b 7f 80");

  }
}