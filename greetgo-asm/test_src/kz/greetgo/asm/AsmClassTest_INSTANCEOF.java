package kz.greetgo.asm;

import lombok.NonNull;
import org.objectweb.asm.Type;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_INSTANCEOF extends AsmClassTest_Parent {

  public static class Test1 {}

  public static class Test2 {}

  public static class Test3 {}

  public static class Test4 {}

  Class<?> generatedClass;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .build()) {

      cl.generateField("field_object", Object.class, AccessType.PUBLIC, false);

      cl.generateDefaultConstructor();

      {
        String          methodDesc = "()" + Type.getDescriptor(int.class);
        final AsmMethod asm = cl.method("call_instanceof", methodDesc).build();
        asm.startMethodCode();
        {
          asm.label("Check_Null");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_object", Object.class);
          asm.IFNONNULL("Test_1")
             .space(1);

          asm.ICONST_0();
          asm.IRETURN();

          asm.label("Test_1");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_object", Object.class);
          asm.INSTANCEOF(Test1.class);
          asm.IFEQ("Test_2")
             .space(1);

          asm.ICONST_1();
          asm.IRETURN();

          asm.label("Test_2")
             .ALOAD(0)
             .GETFIELD(cl.classPath, "field_object", Object.class)
             .INSTANCEOF(Test2.class)
             .IFEQ("Test_3")
             .space(1);

          asm.ICONST_2();
          asm.IRETURN();

          asm.label("Test_3");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_object", Object.class);
          asm.INSTANCEOF(Test3.class);
          asm.IFEQ("Test_4")
             .space(1);

          asm.ICONST_3();
          asm.IRETURN();

          asm.label("Test_4");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_object", Object.class);
          asm.INSTANCEOF(Test4.class);
          asm.IFEQ("UNKNOWN")
             .space(1);

          asm.ICONST_4();
          asm.IRETURN();

          asm.label("UNKNOWN");
          asm.LDC(999_999);
          asm.IRETURN();
        }
        asm.finishMethodCode();
      }


      cl.finishClass();

      generatedClass = cl.load();

    }
  }

  @Test
  public void test_null() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_object").set(instance, null);
    final int result = (Integer) generatedClass.getMethod("call_instanceof").invoke(instance);
    assertThat(result).isEqualTo(0);
  }

  @Test
  public void test1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_object").set(instance, new Test1());
    final int result = (Integer) generatedClass.getMethod("call_instanceof").invoke(instance);
    assertThat(result).isEqualTo(1);
  }

  @Test
  public void test2() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_object").set(instance, new Test2());
    final int result = (Integer) generatedClass.getMethod("call_instanceof").invoke(instance);
    assertThat(result).isEqualTo(2);
  }

  @Test
  public void test3() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_object").set(instance, new Test3());
    final int result = (Integer) generatedClass.getMethod("call_instanceof").invoke(instance);
    assertThat(result).isEqualTo(3);
  }

  @Test
  public void test4() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_object").set(instance, new Test4());
    final int result = (Integer) generatedClass.getMethod("call_instanceof").invoke(instance);
    assertThat(result).isEqualTo(4);
  }


  @Test
  public void test_UNKNOWN() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    generatedClass.getField("field_object").set(instance, "Left");
    final int result = (Integer) generatedClass.getMethod("call_instanceof").invoke(instance);
    assertThat(result).isEqualTo(999_999);
  }

}
