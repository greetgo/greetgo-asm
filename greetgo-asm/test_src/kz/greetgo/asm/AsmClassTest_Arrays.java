package kz.greetgo.asm;

import lombok.NonNull;
import org.objectweb.asm.Type;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static kz.greetgo.asm.help.AsmHelper.returnDesc;
import static kz.greetgo.asm.help.AsmHelper.voidDesc;
import static kz.greetgo.asm.AccessType.PUBLIC;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_Arrays extends AsmClassTest_Parent {

  public static abstract class ParentClass {
    public int[]    array_int;
    public long[]   array_long;
    public float[]  array_float;
    public double[] array_double;
    public String[] array_String;
    public byte[]   array_byte;
    public char[]   array_char;
    public short[]  array_short;

    public double[][][][] array_4D_double;

    public abstract String read_array_int();

    public abstract int read_array_int_length();

    // READING

    public abstract int read_array_int_at(int index);

    public abstract long read_array_long_at(int index);

    public abstract float read_array_float_at(int index);

    public abstract double read_array_double_at(int index);

    public abstract String read_array_String_at(int index);

    public abstract byte read_array_byte_at(int index);

    public abstract char read_array_char_at(int index);

    public abstract short read_array_short_at(int index);

    // WRITING

    public abstract void write_array_int_at(int index, int value);

    public abstract void write_array_long_at(int index, long value);

    public abstract void write_array_float_at(int index, float value);

    public abstract void write_array_double_at(int index, double value);

    public abstract void write_array_String_at(int index, String value);

    public abstract void write_array_byte_at(int index, byte value);

    public abstract void write_array_char_at(int index, char value);

    public abstract void write_array_short_at(int index, short value);

    // ANOTHER
    public abstract void new_array_4D_double(int len1, int len2, int len3, int len4, double value1111);

    public abstract void new_array_int(int len, int value1, int value2);

    public abstract void new_array_long(int len, long value1, long value2);

    public abstract void new_array_String(int len, String value1, String value2);

  }


  Class<?>    generatedClass;
  ParentClass instance;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) throws Exception {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(ParentClass.class)
                               .decimalByteCodeNumbers(true)
                               .build()) {

      cl.generateDefaultConstructor();

      create_read_array_int(cl);
      create_read_array_int_length(cl);

      // READING
      create_read_array_int_at(cl);
      create_read_array_long_at(cl);
      create_read_array_float_at(cl);
      create_read_array_double_at(cl);
      create_read_array_String_at(cl);
      create_read_array_byte_at(cl);
      create_read_array_char_at(cl);
      create_read_array_short_at(cl);

      // WRITING
      create_write_array_int_at(cl);
      create_write_array_long_at(cl);
      create_write_array_float_at(cl);
      create_write_array_double_at(cl);
      create_write_array_String_at(cl);
      create_write_array_byte_at(cl);
      create_write_array_char_at(cl);
      create_write_array_short_at(cl);

      // CREATES
      create_new_array_4D_double(cl);
      create_new_array_int(cl);
      create_new_array_long(cl);
      create_new_array_String(cl);

      cl.finishClass();

      generatedClass = cl.load();

    }

    instance = (ParentClass) generatedClass.getConstructor().newInstance();
  }

  private void create_read_array_int(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(String.class);
    final AsmMethod asm = cl.method("read_array_int", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      // 0 - this
      // 1 - StringBuilder  sb
      // 2 - []int          array
      // 3 - int            i
      // 4 - int            arrayLength

      final int THIS         = 0;
      final int SB           = 1;
      final int ARRAY        = 2;
      final int I            = 3;
      final int ARRAY_LENGTH = 4;

      asm.space(1);
      asm.NEW(StringBuilder.class);
      asm.DUP();
      asm.INVOKESPECIAL(StringBuilder.class, "<init>", "()V")
         .ASTORE(SB);

      asm.space(1);
      asm.ALOAD(THIS);
      asm.GETFIELD(cl.classPath, "array_int", "[I");
      asm.ASTORE(ARRAY);

      asm.space(1);
      asm.ICONST_0();
      asm.ISTORE(I);

      asm.space(1)
         .ALOAD(ARRAY)
         .ARRAYLENGTH()
         .ISTORE(ARRAY_LENGTH);

      final String circle     = "CIRCLE";
      final String circleExit = "CIRCLE_EXIT";

      asm.space(1);
      asm.label(circle);
      asm.ILOAD(I);
      asm.ILOAD(ARRAY_LENGTH);
      asm.IF_ICMPGE(circleExit);

      asm.space(1)
         .ALOAD(SB)
         .ALOAD(ARRAY)
         .ILOAD(I)
         .IALOAD()
         .INVOKEVIRTUAL(StringBuilder.class, "append", returnDesc(StringBuilder.class, int.class), false)
         .POP();

      asm.space(1);
      asm.ALOAD(SB);
      asm.LDC(" ");
      asm.INVOKEVIRTUAL(StringBuilder.class, "append", returnDesc(StringBuilder.class, String.class), false);
      asm.POP();

      asm.space(1);
      asm.IINC(I, 1);
      asm.GOTO(circle);

      asm.space(1);
      asm.label(circleExit);
      asm.ALOAD(SB);
      asm.INVOKEVIRTUAL(StringBuilder.class, "toString", returnDesc(String.class), false);
      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_int() {

    instance.array_int = new int[]{345, 56, -34, 17};

    final String str = instance.read_array_int();

    System.out.println("IVQ6UU2wL4 :: str = " + str);

    assertThat(str).isEqualTo("345 56 -34 17 ");

  }

  private void create_read_array_int_length(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(int.class);
    final AsmMethod asm = cl.method("read_array_int_length", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_int", "[I");
      asm.ARRAYLENGTH();
      asm.IRETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_int_length() {
    instance.array_int = new int[]{345, 56, -34, 17};

    assertThat(instance.read_array_int_length()).isEqualTo(4);
  }

  private void create_read_array_int_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(int.class, int.class);
    final AsmMethod asm = cl.method("read_array_int_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_int", "[I");
      asm.ILOAD(1);
      asm.IALOAD();
      asm.IRETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_int_at() {
    instance.array_int = new int[]{345, 56, -34, 17};

    assertThat(instance.read_array_int_at(0)).isEqualTo(345);
    assertThat(instance.read_array_int_at(1)).isEqualTo(56);
    assertThat(instance.read_array_int_at(2)).isEqualTo(-34);
    assertThat(instance.read_array_int_at(3)).isEqualTo(17);
  }

  private void create_read_array_long_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(long.class, int.class);
    final AsmMethod asm = cl.method("read_array_long_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_long", "[J")
         .ILOAD(1)
         .LALOAD()
         .LRETURN()
         .space(1);
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_long_at() {
    instance.array_long = new long[]{4325423464536453645L, -4367821584672354L, 34, 0, 5};
    assertThat(instance.read_array_long_at(0)).isEqualTo(4325423464536453645L);
    assertThat(instance.read_array_long_at(1)).isEqualTo(-4367821584672354L);
    assertThat(instance.read_array_long_at(2)).isEqualTo(34);
    assertThat(instance.read_array_long_at(3)).isEqualTo(0);
    assertThat(instance.read_array_long_at(4)).isEqualTo(5);
  }

  private void create_read_array_float_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(float.class, int.class);
    final AsmMethod asm = cl.method("read_array_float_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_float", "[F");
      asm.ILOAD(1);
      asm.FALOAD()
         .FRETURN()
         .space(1);
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_float_at() {
    instance.array_float = new float[]{3.4f, -1.3e-12f, -700f, 0f, 13f};
    assertThat(instance.read_array_float_at(0)).isEqualTo(3.4f);
    assertThat(instance.read_array_float_at(1)).isEqualTo(-1.3e-12f);
    assertThat(instance.read_array_float_at(2)).isEqualTo(-700f);
    assertThat(instance.read_array_float_at(3)).isEqualTo(0f);
    assertThat(instance.read_array_float_at(4)).isEqualTo(13f);
  }

  private void create_read_array_double_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(double.class, int.class);
    final AsmMethod asm = cl.method("read_array_double_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_double", "[D")
         .ILOAD(1)
         .DALOAD()
         .DRETURN()
         .space(1);
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_double_at() {
    instance.array_double = new double[]{3.4d, -1.3e-12d, -700d, 0d, 13d};
    assertThat(instance.read_array_double_at(0)).isEqualTo(3.4d);
    assertThat(instance.read_array_double_at(1)).isEqualTo(-1.3e-12d);
    assertThat(instance.read_array_double_at(2)).isEqualTo(-700d);
    assertThat(instance.read_array_double_at(3)).isEqualTo(0d);
    assertThat(instance.read_array_double_at(4)).isEqualTo(13d);
  }

  private void create_read_array_String_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(String.class, int.class);
    final AsmMethod asm = cl.method("read_array_String_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_String", "[" + Type.getDescriptor(String.class))
         .ILOAD(1)
         .AALOAD()
         .ARETURN()
         .space(1);
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_String_at() {
    instance.array_String = new String[]{"tst5435", "test4324"};
    assertThat(instance.read_array_String_at(0)).isEqualTo("tst5435");
    assertThat(instance.read_array_String_at(1)).isEqualTo("test4324");
  }

  private void create_read_array_byte_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(byte.class, int.class);
    final AsmMethod asm = cl.method("read_array_byte_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_byte", "[B");
      asm.ILOAD(1);
      asm.BALOAD()
         .IRETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_byte_at() {
    instance.array_byte = new byte[]{1, 56, -100};
    assertThat(instance.read_array_byte_at(0)).isEqualTo((byte) 1);
    assertThat(instance.read_array_byte_at(1)).isEqualTo((byte) 56);
    assertThat(instance.read_array_byte_at(2)).isEqualTo((byte) -100);
  }

  private void create_read_array_char_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(char.class, int.class);
    final AsmMethod asm = cl.method("read_array_char_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_char", "[C")
         .ILOAD(1)
         .СALOAD()
         .IRETURN()
         .space(1);
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_char_at() {
    instance.array_char = new char[]{'Ъ', 'Й', 'Щ', 'Ё'};
    assertThat(instance.read_array_char_at(0)).isEqualTo('Ъ');
    assertThat(instance.read_array_char_at(1)).isEqualTo('Й');
    assertThat(instance.read_array_char_at(2)).isEqualTo('Щ');
    assertThat(instance.read_array_char_at(3)).isEqualTo('Ё');
  }

  private void create_read_array_short_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = returnDesc(short.class, int.class);
    final AsmMethod asm = cl.method("read_array_short_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_short", "[S")
         .ILOAD(1)
         .SALOAD()
         .IRETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void read_array_short_at() {
    instance.array_short = new short[]{234, 456, 678, 1};
    assertThat(instance.read_array_short_at(0)).isEqualTo((short) 234);
    assertThat(instance.read_array_short_at(1)).isEqualTo((short) 456);
    assertThat(instance.read_array_short_at(2)).isEqualTo((short) 678);
    assertThat(instance.read_array_short_at(3)).isEqualTo((short) 1);
  }

  private void create_write_array_short_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, short.class);
    final AsmMethod asm = cl.method("write_array_short_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_short", "[S")
         .ILOAD(1)
         .ILOAD(2)
         .SASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_short_at() {
    instance.array_short = new short[]{0, 0, 0, 0};

    instance.write_array_short_at(0, (short) 234);
    instance.write_array_short_at(1, (short) 17);
    instance.write_array_short_at(2, (short) -11);
    instance.write_array_short_at(3, (short) 900);

    assertThat(instance.array_short[0]).isEqualTo((short) 234);
    assertThat(instance.array_short[1]).isEqualTo((short) 17);
    assertThat(instance.array_short[2]).isEqualTo((short) -11);
    assertThat(instance.array_short[3]).isEqualTo((short) 900);
  }

  private void create_write_array_char_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, char.class);
    final AsmMethod asm = cl.method("write_array_char_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_char", "[C")
         .ILOAD(1)
         .ILOAD(2)
         .CASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_char_at() {
    instance.array_char = new char[]{0, 0, 0, 0};

    instance.write_array_char_at(0, 'D');
    instance.write_array_char_at(1, 'F');
    instance.write_array_char_at(2, 'G');
    instance.write_array_char_at(3, 'J');

    assertThat(instance.array_char[0]).isEqualTo('D');
    assertThat(instance.array_char[1]).isEqualTo('F');
    assertThat(instance.array_char[2]).isEqualTo('G');
    assertThat(instance.array_char[3]).isEqualTo('J');
  }

  private void create_write_array_byte_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, byte.class);
    final AsmMethod asm = cl.method("write_array_byte_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_byte", "[B")
         .ILOAD(1)
         .ILOAD(2)
         .BASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_byte_at() {
    instance.array_byte = new byte[]{0, 0, 0, 0};

    instance.write_array_byte_at(0, (byte) 12);
    instance.write_array_byte_at(1, (byte) 17);
    instance.write_array_byte_at(2, (byte) -11);
    instance.write_array_byte_at(3, (byte) 90);

    assertThat(instance.array_byte[0]).isEqualTo((byte) 12);
    assertThat(instance.array_byte[1]).isEqualTo((byte) 17);
    assertThat(instance.array_byte[2]).isEqualTo((byte) -11);
    assertThat(instance.array_byte[3]).isEqualTo((byte) 90);
  }

  private void create_write_array_String_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, String.class);
    final AsmMethod asm = cl.method("write_array_String_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_String", "[" + Type.getDescriptor(String.class))
         .ILOAD(1)
         .ALOAD(2)
         .AASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_String_at() {
    instance.array_String = new String[]{null, null, null, null};

    instance.write_array_String_at(0, "Oi1jbDPcDM");
    instance.write_array_String_at(1, "2KmHGf1elR");
    instance.write_array_String_at(2, "Zt3YYPqg11");
    instance.write_array_String_at(3, "Jjo7zevMoG");

    assertThat(instance.array_String[0]).isEqualTo("Oi1jbDPcDM");
    assertThat(instance.array_String[1]).isEqualTo("2KmHGf1elR");
    assertThat(instance.array_String[2]).isEqualTo("Zt3YYPqg11");
    assertThat(instance.array_String[3]).isEqualTo("Jjo7zevMoG");
  }

  private void create_write_array_double_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, double.class);
    final AsmMethod asm = cl.method("write_array_double_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "array_double", "[D")
         .ILOAD(1)
         .DLOAD(2)
         .DASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_double_at() {
    instance.array_double = new double[]{0, 0, 0, 0};

    instance.write_array_double_at(0, 234);
    instance.write_array_double_at(1, 17);
    instance.write_array_double_at(2, -11);
    instance.write_array_double_at(3, 900);

    assertThat(instance.array_double[0]).isEqualTo(234);
    assertThat(instance.array_double[1]).isEqualTo(17);
    assertThat(instance.array_double[2]).isEqualTo(-11);
    assertThat(instance.array_double[3]).isEqualTo(900);
  }

  private void create_write_array_float_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, float.class);
    final AsmMethod asm = cl.method("write_array_float_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_float", "[F");
      asm.ILOAD(1);
      asm.FLOAD(2);
      asm.FASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_float_at() {
    instance.array_float = new float[]{0, 0, 0, 0};

    instance.write_array_float_at(0, 234);
    instance.write_array_float_at(1, 17);
    instance.write_array_float_at(2, -11);
    instance.write_array_float_at(3, 900);

    assertThat(instance.array_float[0]).isEqualTo(234f);
    assertThat(instance.array_float[1]).isEqualTo(17f);
    assertThat(instance.array_float[2]).isEqualTo(-11f);
    assertThat(instance.array_float[3]).isEqualTo(900f);
  }

  private void create_write_array_long_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, long.class);
    final AsmMethod asm = cl.method("write_array_long_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_long", "[J");
      asm.ILOAD(1);
      asm.LLOAD(2);
      asm.LASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_long_at() {
    instance.array_long = new long[]{0, 0, 0, 0};

    instance.write_array_long_at(0, 234);
    instance.write_array_long_at(1, 17);
    instance.write_array_long_at(2, -11);
    instance.write_array_long_at(3, 900);

    assertThat(instance.array_long[0]).isEqualTo(234L);
    assertThat(instance.array_long[1]).isEqualTo(17L);
    assertThat(instance.array_long[2]).isEqualTo(-11L);
    assertThat(instance.array_long[3]).isEqualTo(900L);
  }

  private void create_write_array_int_at(@NonNull AsmClass cl) {
    @NonNull String methodDesc = voidDesc(int.class, int.class);
    final AsmMethod asm = cl.method("write_array_int_at", methodDesc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      asm.space(1);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "array_int", "[I");
      asm.ILOAD(1);
      asm.ILOAD(2);
      asm.IASTORE()
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void write_array_int_at() {
    instance.array_int = new int[]{0, 0, 0, 0};

    instance.write_array_int_at(0, 234);
    instance.write_array_int_at(1, 17);
    instance.write_array_int_at(2, -11);
    instance.write_array_int_at(3, 900);

    assertThat(instance.array_int[0]).isEqualTo(234);
    assertThat(instance.array_int[1]).isEqualTo(17);
    assertThat(instance.array_int[2]).isEqualTo(-11);
    assertThat(instance.array_int[3]).isEqualTo(900);
  }


  private void create_new_array_4D_double(@NonNull AsmClass cl) {
    // public abstract void new_array_4D_double(int len1, int len2, int len3, int len4, double value1111);
    final String    desc = voidDesc(int.class, int.class, int.class, int.class, double.class);
    final AsmMethod asm  = cl.method("new_array_4D_double", desc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      // 0 -        this
      // 1 - int    len1
      // 2 - int    len2
      // 3 - int    len3
      // 4 - int    len4
      // 5 - double value1111
      // 7 - double[][][][] array

      asm.space(1);
      asm.ILOAD(1);
      asm.ILOAD(2);
      asm.ILOAD(3);
      asm.ILOAD(4);
      asm.MULTIANEWARRAY(4, double[][][][].class)
         .ASTORE(7)
         .space(1);

      asm.ALOAD(7);
      asm.ICONST_1();
      asm.AALOAD();
      asm.ICONST_1();
      asm.AALOAD();
      asm.ICONST_1();
      asm.AALOAD();
      asm.ICONST_1();
      asm.DLOAD(5);
      asm.DASTORE()
         .space(1);

      asm.ALOAD(0);
      asm.ALOAD(7);
      asm.PUTFIELD(cl.classPath, "array_4D_double", double[][][][].class);

      asm.space(1);
      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void new_array_4D_double() {
    instance.array_4D_double = null;

    instance.new_array_4D_double(2, 3, 5, 7, -4.5432600756e+230d);

    assertThat(instance.array_4D_double).isNotNull();
    assertThat(instance.array_4D_double.length).isEqualTo(2);
    assertThat(instance.array_4D_double[0].length).isEqualTo(3);
    assertThat(instance.array_4D_double[0][0].length).isEqualTo(5);
    assertThat(instance.array_4D_double[0][0][0].length).isEqualTo(7);

    assertThat(instance.array_4D_double[1][1][1][1]).isEqualTo(-4.5432600756e+230d);
  }

  private void create_new_array_int(@NonNull AsmClass cl) {
    // public abstract void new_array_int(int len, int value1, int value2);
    final String    desc = voidDesc(int.class, int.class, int.class);
    final AsmMethod asm  = cl.method("new_array_int", desc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      // 0 -        this
      // 1 - int    len
      // 2 - int    value1
      // 3 - int    value2
      // 4 - int[]  array

      asm.space(1)
         .ILOAD(1)
         .NEWARRAY(VarType.INT)
         .ASTORE(4)
         .space(1);


      asm.ALOAD(4)
         .ICONST_1()
         .ILOAD(2)
         .IASTORE()
         .space(1);

      asm.ALOAD(4)
         .ICONST_2()
         .ILOAD(3)
         .IASTORE()
         .space(1);

      asm.ALOAD(0)
         .ALOAD(4)
         .PUTFIELD(cl.classPath, "array_int", int[].class)
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void new_array_int() {
    instance.array_int = null;

    instance.new_array_int(10, 345, 654);

    assertThat(instance.array_int).isNotNull();
    assertThat(instance.array_int[1]).isEqualTo(345);
    assertThat(instance.array_int[2]).isEqualTo(654);
  }

  private void create_new_array_long(@NonNull AsmClass cl) {
    // public abstract void new_array_long(int len, long value1, long value2);
    final String    desc = voidDesc(int.class, long.class, long.class);
    final AsmMethod asm  = cl.method("new_array_long", desc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      // 0 -         this
      // 1 - int     len
      // 2 - long    value1
      // 4 - long    value2
      // 6 - long[]  array

      asm.ILOAD(1);
      asm.NEWARRAY(VarType.LONG);
      asm.ASTORE(6)
         .space(1);

      asm.ALOAD(6);
      asm.ICONST_1();
      asm.LLOAD(2);
      asm.LASTORE()
         .space(1);

      asm.ALOAD(6);
      asm.ICONST_2();
      asm.LLOAD(4);
      asm.LASTORE()
         .space(1);

      asm.ALOAD(0);
      asm.ALOAD(6);
      asm.PUTFIELD(cl.classPath, "array_long", long[].class)
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void new_array_long() {
    instance.array_long = null;

    instance.new_array_long(10, 345, 654);

    assertThat(instance.array_long).isNotNull();
    assertThat(instance.array_long[1]).isEqualTo(345);
    assertThat(instance.array_long[2]).isEqualTo(654);
  }


  private void create_new_array_String(@NonNull AsmClass cl) {
    // public abstract void new_array_String(int len, String value1, String value2);
    final String    desc = voidDesc(int.class, String.class, String.class);
    final AsmMethod asm  = cl.method("new_array_String", desc).access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      // 0 -           this
      // 1 - int       len
      // 2 - String    value1
      // 3 - String    value2
      // 4 - String[]  array

      asm.space(1);
      asm.ILOAD(1);
      asm.ANEWARRAY(String.class)
         .ASTORE(4)
         .space(1);

      asm.space(1)
         .ALOAD(4)
         .ICONST_1()
         .ALOAD(2)
         .AASTORE()
         .space(1);

      asm.space(1)
         .ALOAD(4)
         .ICONST_2()
         .ALOAD(3)
         .AASTORE()
         .space(1);

      asm.ALOAD(0)
         .ALOAD(4)
         .PUTFIELD(cl.classPath, "array_String", String[].class)
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void new_array_String() {
    instance.array_String = null;

    instance.new_array_String(10, "345", "654");

    assertThat(instance.array_String).isNotNull();
    assertThat(instance.array_String[1]).isEqualTo("345");
    assertThat(instance.array_String[2]).isEqualTo("654");
  }

}
