package kz.greetgo.asm;

import lombok.NonNull;
import org.objectweb.asm.Type;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_CONST extends AsmClassTest_Parent {
  Class<?> generatedClass;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .build()) {

      cl.generateField("field_int", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_long", long.class, AccessType.PUBLIC, false);
      cl.generateField("field_float", float.class, AccessType.PUBLIC, false);
      cl.generateField("field_double", double.class, AccessType.PUBLIC, false);
      cl.generateField("field_object", Object.class, AccessType.PUBLIC, false);
      cl.generateField("field_byte", byte.class, AccessType.PUBLIC, false);
      cl.generateField("field_short", short.class, AccessType.PUBLIC, false);
      cl.generateField("field_str", String.class, AccessType.PUBLIC, false);
      cl.generateField("field_class", Class.class, AccessType.PUBLIC, false);

      cl.generateDefaultConstructor();

      {
        final AsmMethod asm = cl.method("call_ICONST_0", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ICONST_0();
          asm.PUTFIELD(cl.classPath, "field_int", "I");
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ICONST_1", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ICONST_1();
          asm.PUTFIELD(cl.classPath, "field_int", "I");
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ICONST_2", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ICONST_2();
          asm.PUTFIELD(cl.classPath, "field_int", "I");
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ICONST_3", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .ICONST_3()
             .PUTFIELD(cl.classPath, "field_int", "I")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ICONST_4", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .ICONST_4()
             .PUTFIELD(cl.classPath, "field_int", "I")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ICONST_5", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .ICONST_5()
             .PUTFIELD(cl.classPath, "field_int", "I")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ICONST_M1", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .ICONST_M1()
             .PUTFIELD(cl.classPath, "field_int", "I")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LCONST_0", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .LCONST_0()
             .PUTFIELD(cl.classPath, "field_long", long.class)
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LCONST_1", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .LCONST_1()
             .PUTFIELD(cl.classPath, "field_long", long.class)
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_FCONST_0", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .FCONST_0()
             .PUTFIELD(cl.classPath, "field_float", "F")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_FCONST_1", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .FCONST_1()
             .PUTFIELD(cl.classPath, "field_float", "F")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_FCONST_2", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .FCONST_2()
             .PUTFIELD(cl.classPath, "field_float", "F")
             .RETURN();
        }
        asm.finishMethodCode();
      }

      {
        final AsmMethod asm = cl.method("call_DCONST_0", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .DCONST_0()
             .PUTFIELD(cl.classPath, "field_double", "D")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_DCONST_1", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .DCONST_1()
             .PUTFIELD(cl.classPath, "field_double", "D")
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_ACONST_NULL", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .ACONST_NULL()
             .PUTFIELD(cl.classPath, "field_object", Type.getDescriptor(Object.class))
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_BIPUSH_78", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .BIPUSH(78)
             .PUTFIELD(cl.classPath, "field_byte", Type.getDescriptor(byte.class))
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_SIPUSH_844", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .SIPUSH(844)
             .PUTFIELD(cl.classPath, "field_short", Type.getDescriptor(short.class))
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LDC_int", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .LDC_BI_CI_PUSH(34913)
             .PUTFIELD(cl.classPath, "field_int", Type.getDescriptor(int.class))
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LDC_float", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .LDC(1724.56f)
             .PUTFIELD(cl.classPath, "field_float", Type.getDescriptor(float.class))
             .RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LDC_double", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.LDC(65437.345d)
             .PUTFIELD(cl.classPath, "field_double", Type.getDescriptor(double.class));
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LDC_long", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.LDC(463728146L)
             .PUTFIELD(cl.classPath, "field_long", Type.getDescriptor(long.class));
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LDC_str", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.LDC("Hello World")
             .PUTFIELD(cl.classPath, "field_str", Type.getDescriptor(String.class));
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("call_LDC_class", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.LDC(AsmClassTest_CONST.class)
             .PUTFIELD(cl.classPath, "field_class", Type.getDescriptor(Class.class));
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }
  }

  @Test
  public void call_ICONST_0() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("field_int").set(instance, 123);
      generatedClass.getMethod("call_ICONST_0").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(0);
    }
  }

  @Test
  public void call_ICONST_1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_int").set(instance, 123);
      generatedClass.getMethod("call_ICONST_1").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(1);
    }
  }

  @Test
  public void call_ICONST_2() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_int").set(instance, 123);
      generatedClass.getMethod("call_ICONST_2").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(2);
    }
  }

  @Test
  public void call_ICONST_3() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_int").set(instance, 123);
      generatedClass.getMethod("call_ICONST_3").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(3);
    }
  }

  @Test
  public void call_ICONST_4() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_int").set(instance, 123);
      generatedClass.getMethod("call_ICONST_4").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(4);
    }
  }

  @Test
  public void call_ICONST_5() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_int").set(instance, 123);
      generatedClass.getMethod("call_ICONST_5").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(5);
    }
  }

  @Test
  public void call_ICONST_M1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_int").set(instance, 123);
      generatedClass.getMethod("call_ICONST_M1").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(-1);
    }
  }

  @Test
  public void call_LCONST_0() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_long").set(instance, 123);
      generatedClass.getMethod("call_LCONST_0").invoke(instance);
      long value = (Long) generatedClass.getField("field_long").get(instance);
      assertThat(value).isEqualTo(0);
    }
  }

  @Test
  public void call_LCONST_1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_long").set(instance, 123);
      generatedClass.getMethod("call_LCONST_1").invoke(instance);
      long value = (Long) generatedClass.getField("field_long").get(instance);
      assertThat(value).isEqualTo(1);
    }
  }

  @Test
  public void call_FCONST_0() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_float").set(instance, 123f);
      generatedClass.getMethod("call_FCONST_0").invoke(instance);
      float value = (Float) generatedClass.getField("field_float").get(instance);
      assertThat(value).isEqualTo(0);
    }
  }

  @Test
  public void call_FCONST_1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_float").set(instance, 123f);
      generatedClass.getMethod("call_FCONST_1").invoke(instance);
      float value = (Float) generatedClass.getField("field_float").get(instance);
      assertThat(value).isEqualTo(1f);
    }
  }

  @Test
  public void call_FCONST_2() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_float").set(instance, 123f);
      generatedClass.getMethod("call_FCONST_2").invoke(instance);
      float value = (Float) generatedClass.getField("field_float").get(instance);
      assertThat(value).isEqualTo(2f);
    }
  }

  @Test
  public void call_DCONST_0() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_double").set(instance, 123d);
      generatedClass.getMethod("call_DCONST_0").invoke(instance);
      double value = (Double) generatedClass.getField("field_double").get(instance);
      assertThat(value).isEqualTo(0d);
    }
  }

  @Test
  public void call_DCONST_1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_double").set(instance, 123d);
      generatedClass.getMethod("call_DCONST_1").invoke(instance);
      double value = (Double) generatedClass.getField("field_double").get(instance);
      assertThat(value).isEqualTo(1d);
    }
  }

  @Test
  public void call_ACONST_NULL() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_object").set(instance, new Object());
      generatedClass.getMethod("call_ACONST_NULL").invoke(instance);
      Object value = generatedClass.getField("field_object").get(instance);
      assertThat(value).isNull();
    }
  }

  @Test
  public void call_BIPUSH_78() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_byte").set(instance, (byte) 1);
      generatedClass.getMethod("call_BIPUSH_78").invoke(instance);
      byte value = (Byte) generatedClass.getField("field_byte").get(instance);
      assertThat(value).isEqualTo((byte) 78);
    }
  }

  @Test
  public void call_SIPUSH_844() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_short").set(instance, (short) 1);
      generatedClass.getMethod("call_SIPUSH_844").invoke(instance);
      short value = (Short) generatedClass.getField("field_short").get(instance);
      assertThat(value).isEqualTo((short) 844);
    }
  }

  @Test
  public void call_LDC_int() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_int").set(instance, 11);
      generatedClass.getMethod("call_LDC_int").invoke(instance);
      int value = (Integer) generatedClass.getField("field_int").get(instance);
      assertThat(value).isEqualTo(34913);
    }
  }

  @Test
  public void call_LDC_float() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_float").set(instance, 11);
      generatedClass.getMethod("call_LDC_float").invoke(instance);
      float value = (Float) generatedClass.getField("field_float").get(instance);
      assertThat(value).isEqualTo(1724.56f);
    }
  }

  @Test
  public void call_LDC_double() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_double").set(instance, 11);
      generatedClass.getMethod("call_LDC_double").invoke(instance);
      double value = (Double) generatedClass.getField("field_double").get(instance);
      assertThat(value).isEqualTo(65437.345d);
    }
  }

  @Test
  public void call_LDC_long() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_long").set(instance, 11);
      generatedClass.getMethod("call_LDC_long").invoke(instance);
      long value = (Long) generatedClass.getField("field_long").get(instance);
      assertThat(value).isEqualTo(463728146L);
    }
  }

  @Test
  public void call_LDC_str() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_str").set(instance, "left string");
      generatedClass.getMethod("call_LDC_str").invoke(instance);
      String value = (String) generatedClass.getField("field_str").get(instance);
      assertThat(value).isEqualTo("Hello World");
    }
  }

  @Test
  public void call_LDC_class() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("field_class").set(instance, String.class);
      generatedClass.getMethod("call_LDC_class").invoke(instance);
      Class<?> value = (Class<?>) generatedClass.getField("field_class").get(instance);
      assertThat(value).isEqualTo(AsmClassTest_CONST.class);
    }

  }
}
