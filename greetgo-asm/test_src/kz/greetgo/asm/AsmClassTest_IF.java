package kz.greetgo.asm;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_IF extends AsmClassTest_Parent {

  @Test
  public void conditionsAndLabels() throws Exception {

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("conditionsAndLabels"))
                               .build()) {

      cl.generateField("field_IFEQ", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFEQ", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFEQ", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFNE", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFNE", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFNE", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFLT", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFLT", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFLT", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFGE", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFGE", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFGE", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFGT", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFGT", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFGT", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFLE", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFLE", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFLE", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFNULL", Object.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFNULL", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFNULL", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFNONNULL", Object.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFNONNULL", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFNONNULL", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFICMPEQ_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFICMPEQ_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFICMPEQ", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFICMPEQ", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFICMPNE_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFICMPNE_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFICMPNE", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFICMPNE", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFICMPLT_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFICMPLT_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFICMPLT", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFICMPLT", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFICMPGE_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFICMPGE_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFICMPGE", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFICMPGE", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFICMPGT_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFICMPGT_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFICMPGT", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFICMPGT", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFICMPLE_1", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFICMPLE_2", int.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFICMPLE", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFICMPLE", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFACMPEQ_1", Object.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFACMPEQ_2", Object.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFACMPEQ", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFACMPEQ", String.class, AccessType.PUBLIC, false);

      cl.generateField("field_IFACMPNE_1", Object.class, AccessType.PUBLIC, false);
      cl.generateField("field_IFACMPNE_2", Object.class, AccessType.PUBLIC, false);
      cl.generateField("actual_IFACMPNE", String.class, AccessType.PUBLIC, false);
      cl.generateField("expected_IFACMPNE", String.class, AccessType.PUBLIC, false);

      cl.generateDefaultConstructor();

      {
        final AsmMethod asm = cl.method("doComparisons", "()V").build();
        asm.startMethodCode();
        {
          asm.label("GO_1");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFEQ", int.class);
          asm.IFEQ("IF_1")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not EQ");
          asm.PUTFIELD(cl.classPath, "actual_IFEQ", String.class);
          asm.GOTO("GO_2");

          asm.label("IF_1");
          asm.ALOAD(0);
          asm.LDC("EQ");
          asm.PUTFIELD(cl.classPath, "actual_IFEQ", String.class);
          asm.GOTO("GO_2");
        }
        {
          asm.label("GO_2");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFNE", int.class);
          asm.IFNE("IF_2")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not NE");
          asm.PUTFIELD(cl.classPath, "actual_IFNE", String.class);
          asm.GOTO("GO_3");

          asm.label("IF_2");
          asm.ALOAD(0);
          asm.LDC("NE");
          asm.PUTFIELD(cl.classPath, "actual_IFNE", String.class);
          asm.GOTO("GO_3");
        }

        {
          asm.label("GO_3");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFLT", int.class);
          asm.IFLT("IF_3")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not LT");
          asm.PUTFIELD(cl.classPath, "actual_IFLT", String.class);
          asm.GOTO("GO_4");

          asm.label("IF_3");
          asm.ALOAD(0);
          asm.LDC("LT");
          asm.PUTFIELD(cl.classPath, "actual_IFLT", String.class);
          asm.GOTO("GO_4");
        }
        {
          asm.label("GO_4");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFGE", int.class);
          asm.IFGE("IF_4")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not GE");
          asm.PUTFIELD(cl.classPath, "actual_IFGE", String.class);
          asm.GOTO("GO_5");

          asm.label("IF_4");
          asm.ALOAD(0);
          asm.LDC("GE");
          asm.PUTFIELD(cl.classPath, "actual_IFGE", String.class);
          asm.GOTO("GO_5");
        }
        {
          asm.label("GO_5");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFGT", int.class);
          asm.IFGT("IF_5")
             .space(1);

          asm.ALOAD(0)
             .LDC("not GT")
             .PUTFIELD(cl.classPath, "actual_IFGT", String.class)
             .GOTO("GO_6")
             .space(1);

          asm.label("IF_5");
          asm.ALOAD(0);
          asm.LDC("GT");
          asm.PUTFIELD(cl.classPath, "actual_IFGT", String.class);
          asm.GOTO("GO_6");
        }
        {
          asm.label("GO_6");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFLE", int.class);
          asm.IFLE("IF_6")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not LE");
          asm.PUTFIELD(cl.classPath, "actual_IFLE", String.class);
          asm.GOTO("GO_7");

          asm.label("IF_6");
          asm.ALOAD(0);
          asm.LDC("LE");
          asm.PUTFIELD(cl.classPath, "actual_IFLE", String.class);
          asm.GOTO("GO_7");
        }
        {
          asm.label("GO_7");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFNULL", Object.class);
          asm.IFNULL("IF_7")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not NULL");
          asm.PUTFIELD(cl.classPath, "actual_IFNULL", String.class);
          asm.GOTO("GO_8");

          asm.label("IF_7");
          asm.ALOAD(0);
          asm.LDC("NULL");
          asm.PUTFIELD(cl.classPath, "actual_IFNULL", String.class);
          asm.GOTO("GO_8");
        }
        {
          asm.label("GO_8");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFNONNULL", Object.class);
          asm.IFNONNULL("IF_8")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("NULL");
          asm.PUTFIELD(cl.classPath, "actual_IFNONNULL", String.class);
          asm.GOTO("GO_9");

          asm.label("IF_8");
          asm.ALOAD(0);
          asm.LDC("not NULL");
          asm.PUTFIELD(cl.classPath, "actual_IFNONNULL", String.class);
          asm.GOTO("GO_9");
        }
        {
          asm.label("GO_9");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPEQ_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPEQ_2", int.class);
          asm.IF_ICMPEQ("IF_9")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not EQ");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPEQ", String.class);
          asm.GOTO("GO_10");

          asm.label("IF_9");
          asm.ALOAD(0);
          asm.LDC("EQ");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPEQ", String.class);
          asm.GOTO("GO_10");
        }
        {
          asm.label("GO_10");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPNE_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPNE_2", int.class);
          asm.IF_ICMPNE("IF_10")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not NE");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPNE", String.class);
          asm.GOTO("GO_11");

          asm.label("IF_10");
          asm.ALOAD(0);
          asm.LDC("NE");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPNE", String.class);
          asm.GOTO("GO_11");
        }
        {
          asm.label("GO_11");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPLT_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPLT_2", int.class);
          asm.IF_ICMPLT("IF_11")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not LT");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPLT", String.class);
          asm.GOTO("GO_12");

          asm.label("IF_11");
          asm.ALOAD(0);
          asm.LDC("LT");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPLT", String.class);
          asm.GOTO("GO_12");
        }
        {
          asm.label("GO_12");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPGE_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPGE_2", int.class);
          asm.IF_ICMPGE("IF_12")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not GE");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPGE", String.class);
          asm.GOTO("GO_13");

          asm.label("IF_12");
          asm.ALOAD(0);
          asm.LDC("GE");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPGE", String.class);
          asm.GOTO("GO_13");
        }
        {
          asm.label("GO_13");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPGT_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPGT_2", int.class);
          asm.IF_ICMPGT("IF_13")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not GT");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPGT", String.class);
          asm.GOTO("GO_14");

          asm.label("IF_13");
          asm.ALOAD(0);
          asm.LDC("GT");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPGT", String.class);
          asm.GOTO("GO_14");
        }
        {
          asm.label("GO_14");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPLE_1", int.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFICMPLE_2", int.class);
          asm.IF_ICMPLE("IF_14")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not LE");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPLE", String.class);
          asm.GOTO("GO_15");

          asm.label("IF_14");
          asm.ALOAD(0);
          asm.LDC("LE");
          asm.PUTFIELD(cl.classPath, "actual_IFICMPLE", String.class);
          asm.GOTO("GO_15");
        }
        {
          asm.label("GO_15");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFACMPEQ_1", Object.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFACMPEQ_2", Object.class);
          asm.IF_ACMPEQ("IF_15")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not EQ");
          asm.PUTFIELD(cl.classPath, "actual_IFACMPEQ", String.class);
          asm.GOTO("GO_16");

          asm.label("IF_15");
          asm.ALOAD(0);
          asm.LDC("EQ");
          asm.PUTFIELD(cl.classPath, "actual_IFACMPEQ", String.class);
          asm.GOTO("GO_16");
        }
        {
          asm.label("GO_16");
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFACMPNE_1", Object.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_IFACMPNE_2", Object.class);
          asm.IF_ACMPNE("IF_16")
             .space(1);

          asm.ALOAD(0);
          asm.LDC("not NE");
          asm.PUTFIELD(cl.classPath, "actual_IFACMPNE", String.class);
          asm.GOTO("GO_17");

          asm.label("IF_16");
          asm.ALOAD(0);
          asm.LDC("NE");
          asm.PUTFIELD(cl.classPath, "actual_IFACMPNE", String.class);
          asm.GOTO("GO_17");
        }

        asm.label("GO_17");
        asm.RETURN();

        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();
    }

    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("field_IFEQ").set(instance, 0);
      generatedClass.getField("expected_IFEQ").set(instance, "EQ");

      generatedClass.getField("field_IFNE").set(instance, 0);
      generatedClass.getField("expected_IFNE").set(instance, "not NE");

      generatedClass.getField("field_IFLT").set(instance, -34);
      generatedClass.getField("expected_IFLT").set(instance, "LT");

      generatedClass.getField("field_IFGE").set(instance, -34);
      generatedClass.getField("expected_IFGE").set(instance, "not GE");

      generatedClass.getField("field_IFGT").set(instance, 17);
      generatedClass.getField("expected_IFGT").set(instance, "GT");

      generatedClass.getField("field_IFLE").set(instance, 17);
      generatedClass.getField("expected_IFLE").set(instance, "not LE");

      generatedClass.getField("field_IFNULL").set(instance, null);
      generatedClass.getField("expected_IFNULL").set(instance, "NULL");

      generatedClass.getField("field_IFNONNULL").set(instance, "asd");
      generatedClass.getField("expected_IFNONNULL").set(instance, "not NULL");

      generatedClass.getField("field_IFICMPEQ_1").set(instance, 8);
      generatedClass.getField("field_IFICMPEQ_2").set(instance, 10);
      generatedClass.getField("expected_IFICMPEQ").set(instance, "not EQ");

      generatedClass.getField("field_IFICMPNE_1").set(instance, -8);
      generatedClass.getField("field_IFICMPNE_2").set(instance, 11);
      generatedClass.getField("expected_IFICMPNE").set(instance, "NE");

      generatedClass.getField("field_IFICMPLT_1").set(instance, 8);
      generatedClass.getField("field_IFICMPLT_2").set(instance, -11);
      generatedClass.getField("expected_IFICMPLT").set(instance, "not LT");

      generatedClass.getField("field_IFICMPGE_1").set(instance, 9);
      generatedClass.getField("field_IFICMPGE_2").set(instance, 1);
      generatedClass.getField("expected_IFICMPGE").set(instance, "GE");

      generatedClass.getField("field_IFICMPGT_1").set(instance, 1);
      generatedClass.getField("field_IFICMPGT_2").set(instance, 9);
      generatedClass.getField("expected_IFICMPGT").set(instance, "not GT");

      generatedClass.getField("field_IFICMPLE_1").set(instance, 1);
      generatedClass.getField("field_IFICMPLE_2").set(instance, 9);
      generatedClass.getField("expected_IFICMPLE").set(instance, "LE");

      generatedClass.getField("field_IFACMPEQ_1").set(instance, "tst1");
      generatedClass.getField("field_IFACMPEQ_2").set(instance, "tst2");
      generatedClass.getField("expected_IFACMPEQ").set(instance, "not EQ");

      generatedClass.getField("field_IFACMPNE_1").set(instance, "tst1");
      generatedClass.getField("field_IFACMPNE_2").set(instance, "tst2");
      generatedClass.getField("expected_IFACMPNE").set(instance, "NE");

      generatedClass.getMethod("doComparisons").invoke(instance);

      List<String> errors = new ArrayList<>();

      collectErrors(errors, instance);

      for (final String error : errors) {
        System.out.println("rhWqG9uP3U :: " + error);
      }

      assertThat(errors).isEmpty();
    }


    {
      generatedClass.getField("field_IFEQ").set(instance, 1);
      generatedClass.getField("expected_IFEQ").set(instance, "not EQ");

      generatedClass.getField("field_IFNE").set(instance, 1);
      generatedClass.getField("expected_IFNE").set(instance, "NE");

      generatedClass.getField("field_IFLT").set(instance, 34);
      generatedClass.getField("expected_IFLT").set(instance, "not LT");

      generatedClass.getField("field_IFGE").set(instance, 34);
      generatedClass.getField("expected_IFGE").set(instance, "GE");

      generatedClass.getField("field_IFGT").set(instance, -17);
      generatedClass.getField("expected_IFGT").set(instance, "not GT");

      generatedClass.getField("field_IFLE").set(instance, -17);
      generatedClass.getField("expected_IFLE").set(instance, "LE");

      generatedClass.getField("field_IFNULL").set(instance, "123");
      generatedClass.getField("expected_IFNULL").set(instance, "not NULL");

      generatedClass.getField("field_IFNONNULL").set(instance, null);
      generatedClass.getField("expected_IFNONNULL").set(instance, "NULL");

      generatedClass.getField("field_IFICMPEQ_1").set(instance, 10);
      generatedClass.getField("field_IFICMPEQ_2").set(instance, 10);
      generatedClass.getField("expected_IFICMPEQ").set(instance, "EQ");

      generatedClass.getField("field_IFICMPNE_1").set(instance, 11);
      generatedClass.getField("field_IFICMPNE_2").set(instance, 11);
      generatedClass.getField("expected_IFICMPNE").set(instance, "not NE");

      generatedClass.getField("field_IFICMPLT_1").set(instance, 8);
      generatedClass.getField("field_IFICMPLT_2").set(instance, 11);
      generatedClass.getField("expected_IFICMPLT").set(instance, "LT");

      generatedClass.getField("field_IFICMPGE_1").set(instance, -9);
      generatedClass.getField("field_IFICMPGE_2").set(instance, 1);
      generatedClass.getField("expected_IFICMPGE").set(instance, "not GE");

      generatedClass.getField("field_IFICMPGT_1").set(instance, 11);
      generatedClass.getField("field_IFICMPGT_2").set(instance, 9);
      generatedClass.getField("expected_IFICMPGT").set(instance, "GT");

      generatedClass.getField("field_IFICMPLE_1").set(instance, 11);
      generatedClass.getField("field_IFICMPLE_2").set(instance, 9);
      generatedClass.getField("expected_IFICMPLE").set(instance, "not LE");

      String tst = "tst";

      generatedClass.getField("field_IFACMPEQ_1").set(instance, tst);
      generatedClass.getField("field_IFACMPEQ_2").set(instance, tst);
      generatedClass.getField("expected_IFACMPEQ").set(instance, "EQ");

      generatedClass.getField("field_IFACMPNE_1").set(instance, tst);
      generatedClass.getField("field_IFACMPNE_2").set(instance, tst);
      generatedClass.getField("expected_IFACMPNE").set(instance, "not NE");

      generatedClass.getMethod("doComparisons").invoke(instance);

      List<String> errors = new ArrayList<>();

      collectErrors(errors, instance);

      for (final String error : errors) {
        System.out.println("Dxp1jrAzeT :: " + error);
      }

      assertThat(errors).isEmpty();
    }

    {
      generatedClass.getField("field_IFLT").set(instance, 0);
      generatedClass.getField("expected_IFLT").set(instance, "not LT");

      generatedClass.getField("field_IFGE").set(instance, 0);
      generatedClass.getField("expected_IFGE").set(instance, "GE");

      generatedClass.getField("field_IFGT").set(instance, 0);
      generatedClass.getField("expected_IFGT").set(instance, "not GT");

      generatedClass.getField("field_IFLE").set(instance, 0);
      generatedClass.getField("expected_IFLE").set(instance, "LE");

      generatedClass.getField("field_IFICMPLT_1").set(instance, 11);
      generatedClass.getField("field_IFICMPLT_2").set(instance, 11);
      generatedClass.getField("expected_IFICMPLT").set(instance, "not LT");

      generatedClass.getField("field_IFICMPGE_1").set(instance, -9);
      generatedClass.getField("field_IFICMPGE_2").set(instance, -9);
      generatedClass.getField("expected_IFICMPGE").set(instance, "GE");

      generatedClass.getField("field_IFICMPGT_1").set(instance, 11);
      generatedClass.getField("field_IFICMPGT_2").set(instance, 11);
      generatedClass.getField("expected_IFICMPGT").set(instance, "not GT");

      generatedClass.getField("field_IFICMPLE_1").set(instance, 11);
      generatedClass.getField("field_IFICMPLE_2").set(instance, 11);
      generatedClass.getField("expected_IFICMPLE").set(instance, "LE");

      generatedClass.getMethod("doComparisons").invoke(instance);

      List<String> errors = new ArrayList<>();

      collectErrors(errors, instance);

      for (final String error : errors) {
        System.out.println("xWYGKrkQK6 :: " + error);
      }

      assertThat(errors).isEmpty();
    }


  }

  @SneakyThrows
  private void collectErrors(@NonNull List<String> errors, @NonNull Object instance) {

    final String actual   = "actual_";
    final String expected = "expected_";

    Map<String, Field[]> fieldPairs = new HashMap<>();

    for (final Field field : instance.getClass().getFields()) {
      final String name = field.getName();
      if (name.startsWith(actual)) {
        String key = name.substring(actual.length());
        fieldPairs.computeIfAbsent(key, k -> new Field[2])[0] = field;
        continue;
      }
      if (name.startsWith(expected)) {
        String key = name.substring(expected.length());
        fieldPairs.computeIfAbsent(key, k -> new Field[2])[1] = field;
        continue;
      }
    }

    for (final Field[] fields : fieldPairs.values()) {
      if (fields[0] == null && fields[1] == null) {
        throw new RuntimeException("5P82JfGbgD :: Такого не бывает");
      }
      if (fields[0] == null) {
        throw new RuntimeException("3Jfm561UeT :: No pair for field " + fields[1].getName());
      }
      if (fields[1] == null) {
        throw new RuntimeException("lzYE5PP18I :: No pair for field " + fields[0].getName());
      }

      final Field field1 = fields[0];
      final Field field2 = fields[1];

      final Object value1 = field1.get(instance);
      final Object value2 = field2.get(instance);

      if (Objects.equals(value1, value2)) {
        continue;
      }

      errors.add(
        field1.getName() + " = `" + value1 + "` BUT " + field2.getName() + " = `" + value2 + "` - it is wrong. They MUST be equal");
    }
  }
}
