package kz.greetgo.asm;

import lombok.NonNull;
import org.objectweb.asm.Type;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_RETURN extends AsmClassTest_Parent {
  Class<?> generatedClass;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .build()) {

      cl.generateField("field_object", Object.class, AccessType.PUBLIC, false);
      cl.generateField("field_int", int.class, AccessType.PUBLIC, false);
      cl.generateField("field_long", long.class, AccessType.PUBLIC, false);
      cl.generateField("field_float", float.class, AccessType.PUBLIC, false);
      cl.generateField("field_double", double.class, AccessType.PUBLIC, false);

      cl.generateDefaultConstructor();

      {
        String          methodDesc = "()" + Type.getDescriptor(Object.class);
        final AsmMethod asm = cl.method("return_object", methodDesc).build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_object", Object.class);
          asm.ARETURN();
        }
        asm.finishMethodCode();
      }
      {
        String          methodDesc = "()" + Type.getDescriptor(int.class);
        final AsmMethod asm = cl.method("return_int", methodDesc).build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_int", int.class);
          asm.IRETURN();
        }
        asm.finishMethodCode();
      }
      {
        String          methodDesc = "()" + Type.getDescriptor(long.class);
        final AsmMethod asm = cl.method("return_long", methodDesc).build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_long", long.class);
          asm.LRETURN();
        }
        asm.finishMethodCode();
      }
      {
        String          methodDesc = "()" + Type.getDescriptor(float.class);
        final AsmMethod asm = cl.method("return_float", methodDesc).build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_float", float.class);
          asm.FRETURN();
        }
        asm.finishMethodCode();
      }
      {
        String          methodDesc = "()" + Type.getDescriptor(double.class);
        final AsmMethod asm = cl.method("return_double", methodDesc).build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "field_double", double.class);
          asm.DRETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }
  }

  @Test
  public void return_object() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();

    final String object = "Привет котята!!!";

    generatedClass.getField("field_object").set(instance, object);
    final Object result = generatedClass.getMethod("return_object").invoke(instance);
    assertThat(result).isEqualTo(object);

  }

  @Test
  public void return_int() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_int").set(instance, 54325);
    final int result = (Integer) generatedClass.getMethod("return_int").invoke(instance);
    assertThat(result).isEqualTo(54325);

  }

  @Test
  public void return_long() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_long").set(instance, 54325);
    final long result = (Long) generatedClass.getMethod("return_long").invoke(instance);
    assertThat(result).isEqualTo(54325);

  }

  @Test
  public void return_float() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_float").set(instance, 54325f);
    final float result = (Float) generatedClass.getMethod("return_float").invoke(instance);
    assertThat(result).isEqualTo(54325f);

  }

  @Test
  public void return_double() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field_double").set(instance, 5432.5d);
    final double result = (Double) generatedClass.getMethod("return_double").invoke(instance);
    assertThat(result).isEqualTo(5432.5d);

  }

}
