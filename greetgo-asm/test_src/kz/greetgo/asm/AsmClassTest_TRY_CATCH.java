package kz.greetgo.asm;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.Assertions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.PrintStream;
import java.lang.reflect.Method;

import static kz.greetgo.asm.help.AsmHelper.voidDesc;
import static kz.greetgo.asm.AccessType.PUBLIC;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_TRY_CATCH extends AsmClassTest_Parent {

  public static abstract class ParentClass {

    public RuntimeException throwingException;

    @SuppressWarnings("unused")
    public void throwException() {
      throw throwingException;
    }

    public int  error_int;
    public long error_long;

    public abstract void callThrowExceptionInTryCatch();

    public boolean finallyCalled;
  }

  Class<?>    generatedClass;
  ParentClass instance;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) throws Exception {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(ParentClass.class)
                               .decimalByteCodeNumbers(true)
                               .build()) {

      cl.generateField("field_object", Object.class, PUBLIC, false);

      cl.generateDefaultConstructor();

      create__callThrowExceptionInTryCatch(cl);

      cl.finishClass();

      generatedClass = cl.load();

    }

    instance = (ParentClass) generatedClass.getConstructor().newInstance();
  }

  private void create__callThrowExceptionInTryCatch(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("callThrowExceptionInTryCatch", "()V").access(PUBLIC).static_(false).build();
    asm.startMethodCode();
    {
      // 1 - Exception error

      asm.GETSTATIC(System.class, "out", PrintStream.class)
         .LDC("2JA6c4DrIv :: Started method callThrowExceptionInTryCatch")
         .INVOKEVIRTUAL(PrintStream.class, "println", voidDesc(String.class), false);

      asm.space(1).label("TRY")
         .ALOAD(0)
         .INVOKEVIRTUAL(cl.classPath, "throwException", "()V", false);

      asm.space(1).label("FINALLY")
         .ALOAD(0)
         .ICONST_1()
         .PUTFIELD(cl.classPath, "finallyCalled", boolean.class)
         .RETURN();

      asm.space(1).label("CATCH_ErrorInt")
         .ASTORE(1);

      asm.ALOAD(0)
         .ALOAD(1)
         .GETFIELD(ErrorWithInt.class, "error", int.class)
         .PUTFIELD(cl.classPath, "error_int", int.class)
         .GOTO("FINALLY");

      asm.space(1).label("CATCH_ErrorLong")
         .ASTORE(1);

      asm.ALOAD(0)
         .ALOAD(1)
         .GETFIELD(ErrorWithLong.class, "error", long.class)
         .PUTFIELD(cl.classPath, "error_long", long.class)
         .GOTO("FINALLY");

      asm.space(1).label("CATCH_null")
         .ASTORE(1);

      asm.ALOAD(0)
         .ICONST_1()
         .PUTFIELD(cl.classPath, "finallyCalled", boolean.class)
         .ALOAD(1)
         .ATHROW();

    }
    asm.space(1)
       .TRY_CATCH("TRY", "FINALLY", "CATCH_ErrorInt", ErrorWithInt.class)
       .TRY_CATCH("TRY", "FINALLY", "CATCH_ErrorLong", ErrorWithLong.class)
       .TRY_CATCH("TRY", "FINALLY", "CATCH_null")
       .finishMethodCode();
  }

  @RequiredArgsConstructor
  public static class ErrorWithInt extends RuntimeException {
    public final int error;
  }

  @RequiredArgsConstructor
  public static class ErrorWithLong extends RuntimeException {
    public final long error;
  }

  public static class ErrorAnother extends RuntimeException {}

  @Test
  public void try_catch_int() {

    instance.throwingException = new ErrorWithInt(98_023);
    instance.finallyCalled     = false;

    instance.error_int  = 0;
    instance.error_long = 0;

    instance.callThrowExceptionInTryCatch();

    assertThat(instance.error_int).isEqualTo(98_023);
    assertThat(instance.error_long).isEqualTo(0);
    assertThat(instance.finallyCalled).isTrue();

  }

  @Test
  public void try_catch_long() {

    final long tmp = 98_023_456_234_456_234L;

    instance.throwingException = new ErrorWithLong(tmp);
    instance.finallyCalled     = false;

    instance.error_int  = 0;
    instance.error_long = 0;

    instance.callThrowExceptionInTryCatch();

    assertThat(instance.error_int).isEqualTo(0);
    assertThat(instance.error_long).isEqualTo(tmp);
    assertThat(instance.finallyCalled).isTrue();
  }

  @Test
  public void try_catch_another() {
    instance.throwingException = new ErrorAnother();
    instance.finallyCalled     = false;

    ErrorAnother comeError = null;

    try {
      instance.callThrowExceptionInTryCatch();
      Assertions.fail("AsFn1jkBz8 :: MUST throw ErrorAnother");
    } catch (ErrorAnother e) {
      comeError = e;
    }

    assertThat(comeError).isNotNull()
                         .isSameAs(instance.throwingException);

    assertThat(instance.finallyCalled).isTrue();
  }

}
