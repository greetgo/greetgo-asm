package kz.greetgo.asm;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;

import static kz.greetgo.asm.help.AsmHelper.voidDesc;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_INC extends AsmClassTest_Parent {
  @SuppressWarnings("unused")
  public static abstract class JavaFields {

    public byte       num_1;
    public short      num_2;
    public int        num_3;
    public long       num_4;
    public float      num_5;
    public double     num_6;
    public Byte       num_7;
    public Short      num_8;
    public Integer    num_9;
    public Long       num_10;
    public Float      num_11;
    public Double     num_12;
    public BigInteger num_13;
    public BigDecimal num_14;

  }

  Class<?>   generatedClass;
  JavaFields fields;

  @BeforeMethod
  @SneakyThrows
  public void generateClass(@NonNull Method testMethod) {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(JavaFields.class)
                               .build()) {

      cl.generateDefaultConstructor();

      for (int c = 1; c <= 14; c++) {

        final Field field = JavaFields.class.getField("num_" + c);

        for (int i = -5; i <= 5; i++) {


          final AsmMethod asm = cl.method(("inc_" + c + "_" + i), voidDesc()).build();
          asm.startMethodCode();
          asm.newVarIndex(Object.class);// place for this.0
          final int varIndex = asm.newVarIndex(field.getType());
          {

            asm.ALOAD(0)
               .GETFIELD(field)
               .STORE(varIndex, field.getType());

            asm.NOP()
               .INC(varIndex, i, field.getType())
               .NOP();

            asm.ALOAD(0)
               .LOAD(varIndex, field.getType())
               .PUTFIELD(field);

            asm.RETURN();

          }
          asm.finishMethodCode();
        }
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    fields = (JavaFields) generatedClass.getConstructor().newInstance();
  }

  @DataProvider
  private Object[][] compareDataProvider() {
    return new Object[][]{
      //@formatter:off
      {  1, (byte)10, -5,  (byte) 5  },
      {  1, (byte)10, -4,  (byte) 6  },
      {  1, (byte)10, -3,  (byte) 7  },
      {  1, (byte)10, -2,  (byte) 8  },
      {  1, (byte)10, -1,  (byte) 9  },
      {  1, (byte)10,  0,  (byte)10  },
      {  1, (byte)10,  1,  (byte)11  },
      {  1, (byte)10,  2,  (byte)12  },
      {  1, (byte)10,  3,  (byte)13  },
      {  1, (byte)10,  4,  (byte)14  },
      {  1, (byte)10,  5,  (byte)15  },

      {  2, (short)10, -5,  (short) 5  },
      {  2, (short)10, -4,  (short) 6  },
      {  2, (short)10, -3,  (short) 7  },
      {  2, (short)10, -2,  (short) 8  },
      {  2, (short)10, -1,  (short) 9  },
      {  2, (short)10,  0,  (short)10  },
      {  2, (short)10,  1,  (short)11  },
      {  2, (short)10,  2,  (short)12  },
      {  2, (short)10,  3,  (short)13  },
      {  2, (short)10,  4,  (short)14  },
      {  2, (short)10,  5,  (short)15  },

      {  3, 10, -5,   5  },
      {  3, 10, -4,   6  },
      {  3, 10, -3,   7  },
      {  3, 10, -2,   8  },
      {  3, 10, -1,   9  },
      {  3, 10,  0,  10  },
      {  3, 10,  1,  11  },
      {  3, 10,  2,  12  },
      {  3, 10,  3,  13  },
      {  3, 10,  4,  14  },
      {  3, 10,  5,  15  },

      {  4, 10L, -5,   5L  },
      {  4, 10L, -4,   6L  },
      {  4, 10L, -3,   7L  },
      {  4, 10L, -2,   8L  },
      {  4, 10L, -1,   9L  },
      {  4, 10L,  0,  10L  },
      {  4, 10L,  1,  11L  },
      {  4, 10L,  2,  12L  },
      {  4, 10L,  3,  13L  },
      {  4, 10L,  4,  14L  },
      {  4, 10L,  5,  15L  },

      {  5, 10f, -5,   5f  },
      {  5, 10f, -4,   6f  },
      {  5, 10f, -3,   7f  },
      {  5, 10f, -2,   8f  },
      {  5, 10f, -1,   9f  },
      {  5, 10f,  0,  10f  },
      {  5, 10f,  1,  11f  },
      {  5, 10f,  2,  12f  },
      {  5, 10f,  3,  13f  },
      {  5, 10f,  4,  14f  },
      {  5, 10f,  5,  15f  },

      {  6, 10d, -5,   5d  },
      {  6, 10d, -4,   6d  },
      {  6, 10d, -3,   7d  },
      {  6, 10d, -2,   8d  },
      {  6, 10d, -1,   9d  },
      {  6, 10d,  0,  10d  },
      {  6, 10d,  1,  11d  },
      {  6, 10d,  2,  12d  },
      {  6, 10d,  3,  13d  },
      {  6, 10d,  4,  14d  },
      {  6, 10d,  5,  15d  },

      {  7, (byte)10, -5,  (byte) 5  },
      {  7, (byte)10, -4,  (byte) 6  },
      {  7, (byte)10, -3,  (byte) 7  },
      {  7, (byte)10, -2,  (byte) 8  },
      {  7, (byte)10, -1,  (byte) 9  },
      {  7, (byte)10,  0,  (byte)10  },
      {  7, (byte)10,  1,  (byte)11  },
      {  7, (byte)10,  2,  (byte)12  },
      {  7, (byte)10,  3,  (byte)13  },
      {  7, (byte)10,  4,  (byte)14  },
      {  7, (byte)10,  5,  (byte)15  },

      {  8, (short)10, -5,  (short) 5  },
      {  8, (short)10, -4,  (short) 6  },
      {  8, (short)10, -3,  (short) 7  },
      {  8, (short)10, -2,  (short) 8  },
      {  8, (short)10, -1,  (short) 9  },
      {  8, (short)10,  0,  (short)10  },
      {  8, (short)10,  1,  (short)11  },
      {  8, (short)10,  2,  (short)12  },
      {  8, (short)10,  3,  (short)13  },
      {  8, (short)10,  4,  (short)14  },
      {  8, (short)10,  5,  (short)15  },

      {  9, 10, -5,   5  },
      {  9, 10, -4,   6  },
      {  9, 10, -3,   7  },
      {  9, 10, -2,   8  },
      {  9, 10, -1,   9  },
      {  9, 10,  0,  10  },
      {  9, 10,  1,  11  },
      {  9, 10,  2,  12  },
      {  9, 10,  3,  13  },
      {  9, 10,  4,  14  },
      {  9, 10,  5,  15  },

      {  10, 10L, -5,   5L  },
      {  10, 10L, -4,   6L  },
      {  10, 10L, -3,   7L  },
      {  10, 10L, -2,   8L  },
      {  10, 10L, -1,   9L  },
      {  10, 10L,  0,  10L  },
      {  10, 10L,  1,  11L  },
      {  10, 10L,  2,  12L  },
      {  10, 10L,  3,  13L  },
      {  10, 10L,  4,  14L  },
      {  10, 10L,  5,  15L  },

      {  11, 10f, -5,   5f  },
      {  11, 10f, -4,   6f  },
      {  11, 10f, -3,   7f  },
      {  11, 10f, -2,   8f  },
      {  11, 10f, -1,   9f  },
      {  11, 10f,  0,  10f  },
      {  11, 10f,  1,  11f  },
      {  11, 10f,  2,  12f  },
      {  11, 10f,  3,  13f  },
      {  11, 10f,  4,  14f  },
      {  11, 10f,  5,  15f  },

      {  12, 10d, -5,   5d  },
      {  12, 10d, -4,   6d  },
      {  12, 10d, -3,   7d  },
      {  12, 10d, -2,   8d  },
      {  12, 10d, -1,   9d  },
      {  12, 10d,  0,  10d  },
      {  12, 10d,  1,  11d  },
      {  12, 10d,  2,  12d  },
      {  12, 10d,  3,  13d  },
      {  12, 10d,  4,  14d  },
      {  12, 10d,  5,  15d  },

      {  13, new BigInteger("20"), -5,  new BigInteger("15")  },
      {  13, new BigInteger("20"), -4,  new BigInteger("16")  },
      {  13, new BigInteger("20"), -3,  new BigInteger("17")  },
      {  13, new BigInteger("20"), -2,  new BigInteger("18")  },
      {  13, new BigInteger("20"), -1,  new BigInteger("19")  },
      {  13, new BigInteger("20"),  0,  new BigInteger("20")  },
      {  13, new BigInteger("20"),  1,  new BigInteger("21")  },
      {  13, new BigInteger("20"),  2,  new BigInteger("22")  },
      {  13, new BigInteger("20"),  3,  new BigInteger("23")  },
      {  13, new BigInteger("20"),  4,  new BigInteger("24")  },
      {  13, new BigInteger("20"),  5,  new BigInteger("25")  },

      {  14, new BigDecimal("20"), -5,  new BigDecimal("15")  },
      {  14, new BigDecimal("20"), -4,  new BigDecimal("16")  },
      {  14, new BigDecimal("20"), -3,  new BigDecimal("17")  },
      {  14, new BigDecimal("20"), -2,  new BigDecimal("18")  },
      {  14, new BigDecimal("20"), -1,  new BigDecimal("19")  },
      {  14, new BigDecimal("20"),  0,  new BigDecimal("20")  },
      {  14, new BigDecimal("20"),  1,  new BigDecimal("21")  },
      {  14, new BigDecimal("20"),  2,  new BigDecimal("22")  },
      {  14, new BigDecimal("20"),  3,  new BigDecimal("23")  },
      {  14, new BigDecimal("20"),  4,  new BigDecimal("24")  },
      {  14, new BigDecimal("20"),  5,  new BigDecimal("25")  },

      //@formatter:on
    };
  }

  @Test(dataProvider = "compareDataProvider")
  public void inc(int fld, Object init, int delta, Object result) throws Exception {

    final Field field = fields.getClass().getField("num_" + fld);

    field.set(fields, init);

    fields.getClass().getMethod("inc_" + fld + "_" + delta).invoke(fields);

    Object actual = field.get(fields);

    //
    //
    assertThat(actual).isEqualTo(result);
    //
    //

  }

}
