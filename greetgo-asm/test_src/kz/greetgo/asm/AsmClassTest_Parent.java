package kz.greetgo.asm;

import org.testng.annotations.BeforeMethod;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class AsmClassTest_Parent {

  protected Path root;

  @BeforeMethod
  public void prepareTestDir() {
    root = Paths.get("build").resolve(getClass().getSimpleName());
  }

  protected String nowSorted() {
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
    return sdf.format(new Date());
  }

}
