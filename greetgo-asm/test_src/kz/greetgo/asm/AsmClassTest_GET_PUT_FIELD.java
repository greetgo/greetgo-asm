package kz.greetgo.asm;

import kz.greetgo.util.RND;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.FieldNameConstants;
import org.objectweb.asm.Type;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static kz.greetgo.asm.help.AsmHelper.returnDesc;
import static kz.greetgo.asm.help.AsmHelper.voidDesc;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_GET_PUT_FIELD extends AsmClassTest_Parent {

  @FieldNameConstants
  public static class SomeFields {

    public String field;

    public static String staticField;

  }

  @FieldNameConstants
  public static abstract class ParentClass {

    public SomeFields someFields;

    public abstract String GET_FIELD_01();

    public abstract String GET_FIELD_02();

    public abstract String GET_FIELD_03();

    public abstract String GET_FIELD_04();

    public abstract String GET_FIELD_05();

    public abstract String GET_FIELD_06();


    public abstract void PUT_FIELD_01(String value);

    public abstract void PUT_FIELD_02(String value);

    public abstract void PUT_FIELD_03(String value);

    public abstract void PUT_FIELD_04(String value);

    public abstract void PUT_FIELD_05(String value);

    public abstract void PUT_FIELD_06(String value);


    public abstract String GET_STATIC_01();

    public abstract String GET_STATIC_02();

    public abstract String GET_STATIC_03();

    public abstract String GET_STATIC_04();

    public abstract String GET_STATIC_05();

    public abstract String GET_STATIC_06();

    public abstract String GET_STATIC_07();


    public abstract void PUT_STATIC_01(String value);

    public abstract void PUT_STATIC_02(String value);

    public abstract void PUT_STATIC_03(String value);

    public abstract void PUT_STATIC_04(String value);

    public abstract void PUT_STATIC_05(String value);

    public abstract void PUT_STATIC_06(String value);

    public abstract void PUT_STATIC_07(String value);

  }

  Class<?>    generatedClass;
  ParentClass instance;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) throws Exception {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(ParentClass.class)
                               .decimalByteCodeNumbers(true)
                               .build()) {

      cl.generateDefaultConstructor();

      create__GET_FIELD_01(cl);
      create__GET_FIELD_02(cl);
      create__GET_FIELD_03(cl);
      create__GET_FIELD_04(cl);
      create__GET_FIELD_05(cl);
      create__GET_FIELD_06(cl);

      create__PUT_FIELD_01(cl);
      create__PUT_FIELD_02(cl);
      create__PUT_FIELD_03(cl);
      create__PUT_FIELD_04(cl);
      create__PUT_FIELD_05(cl);
      create__PUT_FIELD_06(cl);

      create__GET_STATIC_01(cl);
      create__GET_STATIC_02(cl);
      create__GET_STATIC_03(cl);
      create__GET_STATIC_04(cl);
      create__GET_STATIC_05(cl);
      create__GET_STATIC_06(cl);
      create__GET_STATIC_07(cl);

      create__PUT_STATIC_01(cl);
      create__PUT_STATIC_02(cl);
      create__PUT_STATIC_03(cl);
      create__PUT_STATIC_04(cl);
      create__PUT_STATIC_05(cl);
      create__PUT_STATIC_06(cl);
      create__PUT_STATIC_07(cl);

      cl.finishClass();

      generatedClass = cl.load();

    }

    instance = (ParentClass) generatedClass.getConstructor().newInstance();
  }

  private void create__GET_FIELD_01(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_FIELD_01", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .space(1);

      //
      //
      asm.GETFIELD(SomeFields.class, SomeFields.Fields.field).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_FIELD_02(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_FIELD_02", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .space(1);

      //
      //
      asm.GETFIELD(SomeFields.class, SomeFields.Fields.field, String.class).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  @SneakyThrows
  private void create__GET_FIELD_03(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_FIELD_03", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(ParentClass.class.getField(ParentClass.Fields.someFields))
         .space(1);

      //
      //
      asm.GETFIELD(SomeFields.class, SomeFields.Fields.field, Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_FIELD_04(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_FIELD_04", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .space(1);

      //
      //
      asm.GETFIELD(Type.getInternalName(SomeFields.class), SomeFields.Fields.field, Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_FIELD_05(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_FIELD_05", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .space(1);

      //
      //
      asm.GETFIELD(Type.getInternalName(SomeFields.class), SomeFields.Fields.field, String.class).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_FIELD_06(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_FIELD_06", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .space(1);

      //
      //
      asm.GETFIELD(SomeFields.class, SomeFields.Fields.field, (Class<?>) null).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }


  @Test
  public void GET_FIELD_01() {
    instance.someFields       = new SomeFields();
    instance.someFields.field = RND.str(10);
    final String actual = instance.GET_FIELD_01();
    assertThat(actual).isEqualTo(instance.someFields.field);
  }

  @Test
  public void GET_FIELD_02() {
    instance.someFields       = new SomeFields();
    instance.someFields.field = RND.str(10);
    final String actual = instance.GET_FIELD_02();
    assertThat(actual).isEqualTo(instance.someFields.field);
  }

  @Test
  public void GET_FIELD_03() {
    instance.someFields       = new SomeFields();
    instance.someFields.field = RND.str(10);
    final String actual = instance.GET_FIELD_03();
    assertThat(actual).isEqualTo(instance.someFields.field);
  }

  @Test
  public void GET_FIELD_04() {
    instance.someFields       = new SomeFields();
    instance.someFields.field = RND.str(10);
    final String actual = instance.GET_FIELD_04();
    assertThat(actual).isEqualTo(instance.someFields.field);
  }

  @Test
  public void GET_FIELD_05() {
    instance.someFields       = new SomeFields();
    instance.someFields.field = RND.str(10);
    final String actual = instance.GET_FIELD_05();
    assertThat(actual).isEqualTo(instance.someFields.field);
  }

  @Test
  public void GET_FIELD_06() {
    instance.someFields       = new SomeFields();
    instance.someFields.field = RND.str(10);
    final String actual = instance.GET_FIELD_06();
    assertThat(actual).isEqualTo(instance.someFields.field);
  }

  @Test
  public void PUT_FIELD_01() {
    instance.someFields = new SomeFields();
    String expected = RND.str(10);
    instance.PUT_FIELD_01(expected);
    assertThat(instance.someFields.field).isEqualTo(expected);
  }

  @Test
  public void PUT_FIELD_02() {
    instance.someFields = new SomeFields();
    String expected = RND.str(10);
    instance.PUT_FIELD_02(expected);
    assertThat(instance.someFields.field).isEqualTo(expected);
  }

  @Test
  public void PUT_FIELD_03() {
    instance.someFields = new SomeFields();
    String expected = RND.str(10);
    instance.PUT_FIELD_03(expected);
    assertThat(instance.someFields.field).isEqualTo(expected);
  }

  @Test
  public void PUT_FIELD_04() {
    instance.someFields = new SomeFields();
    String expected = RND.str(10);
    instance.PUT_FIELD_04(expected);
    assertThat(instance.someFields.field).isEqualTo(expected);
  }

  @Test
  public void PUT_FIELD_05() {
    instance.someFields = new SomeFields();
    String expected = RND.str(10);
    instance.PUT_FIELD_05(expected);
    assertThat(instance.someFields.field).isEqualTo(expected);
  }

  @Test
  public void PUT_FIELD_06() {
    instance.someFields = new SomeFields();
    String expected = RND.str(10);
    instance.PUT_FIELD_06(expected);
    assertThat(instance.someFields.field).isEqualTo(expected);
  }

  private void create__PUT_FIELD_01(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_FIELD_01", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .ALOAD(1)
         .space(1);

      //
      //
      asm.PUTFIELD(SomeFields.class, SomeFields.Fields.field).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_FIELD_02(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_FIELD_02", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .ALOAD(1)
         .space(1);

      //
      //
      asm.PUTFIELD(SomeFields.class, SomeFields.Fields.field, String.class).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_FIELD_03(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_FIELD_03", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .ALOAD(1)
         .space(1);

      //
      //
      asm.PUTFIELD(SomeFields.class, SomeFields.Fields.field, Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_FIELD_04(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_FIELD_04", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .ALOAD(1)
         .space(1);

      //
      //
      asm.PUTFIELD(Type.getInternalName(SomeFields.class), SomeFields.Fields.field, Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @SneakyThrows
  private void create__PUT_FIELD_05(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_FIELD_05", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .ALOAD(1)
         .space(1);

      //
      //
      asm.PUTFIELD(SomeFields.class.getField(SomeFields.Fields.field)).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_FIELD_06(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_FIELD_06", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0)
         .ALOAD(0)
         .GETFIELD(ParentClass.class, ParentClass.Fields.someFields)
         .ALOAD(1)
         .space(1);

      //
      //
      asm.PUTFIELD(SomeFields.class, SomeFields.Fields.field, (Class<?>) null).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_STATIC_01(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_STATIC_01", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);

      //
      //
      asm.GETSTATIC(SomeFields.class, "staticField").space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_STATIC_02(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_STATIC_02", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);

      //
      //
      asm.GETSTATIC(SomeFields.class, "staticField", String.class).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_STATIC_03(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_STATIC_03", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);

      //
      //
      asm.GETSTATIC(SomeFields.class, "staticField", Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_STATIC_04(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_STATIC_04", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);

      //
      //
      asm.GETSTATIC(Type.getInternalName(SomeFields.class), "staticField", Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_STATIC_05(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_STATIC_05", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);

      //
      //
      asm.GETSTATIC(Type.getInternalName(SomeFields.class), "staticField", String.class).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  private void create__GET_STATIC_06(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_STATIC_06", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);

      //
      //
      asm.GETSTATIC(SomeFields.class, "staticField", (Class<?>) null).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  @SneakyThrows
  private void create__GET_STATIC_07(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("GET_STATIC_07", returnDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(0);

      //
      //
      asm.GETSTATIC(SomeFields.class.getField("staticField")).space(1);
      //
      //

      asm.ARETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void GET_STATIC_01() {
    SomeFields.staticField = RND.str(10);
    String actual = instance.GET_STATIC_01();
    assertThat(actual).isEqualTo(SomeFields.staticField);
  }

  @Test
  public void GET_STATIC_02() {
    SomeFields.staticField = RND.str(10);
    String actual = instance.GET_STATIC_02();
    assertThat(actual).isEqualTo(SomeFields.staticField);
  }

  @Test
  public void GET_STATIC_03() {
    SomeFields.staticField = RND.str(10);
    String actual = instance.GET_STATIC_03();
    assertThat(actual).isEqualTo(SomeFields.staticField);
  }

  @Test
  public void GET_STATIC_04() {
    SomeFields.staticField = RND.str(10);
    String actual = instance.GET_STATIC_04();
    assertThat(actual).isEqualTo(SomeFields.staticField);
  }

  @Test
  public void GET_STATIC_05() {
    SomeFields.staticField = RND.str(10);
    String actual = instance.GET_STATIC_05();
    assertThat(actual).isEqualTo(SomeFields.staticField);
  }

  @Test
  public void GET_STATIC_06() {
    SomeFields.staticField = RND.str(10);
    String actual = instance.GET_STATIC_06();
    assertThat(actual).isEqualTo(SomeFields.staticField);
  }

  @Test
  public void GET_STATIC_07() {
    SomeFields.staticField = RND.str(10);
    String actual = instance.GET_STATIC_07();
    assertThat(actual).isEqualTo(SomeFields.staticField);
  }

  private void create__PUT_STATIC_01(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_STATIC_01", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1);

      //
      //
      asm.PUTSTATIC(SomeFields.class, "staticField").space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_STATIC_02(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_STATIC_02", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1);

      //
      //
      asm.PUTSTATIC(SomeFields.class, "staticField", String.class).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_STATIC_03(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_STATIC_03", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1);

      //
      //
      asm.PUTSTATIC(SomeFields.class, "staticField", Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_STATIC_04(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_STATIC_04", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1);

      //
      //
      asm.PUTSTATIC(Type.getInternalName(SomeFields.class), "staticField", Type.getDescriptor(String.class)).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_STATIC_05(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_STATIC_05", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1);

      //
      //
      asm.PUTSTATIC(Type.getInternalName(SomeFields.class), "staticField", String.class).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  private void create__PUT_STATIC_06(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_STATIC_06", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1);

      //
      //
      asm.PUTSTATIC(SomeFields.class, "staticField", (Class<?>) null).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @SneakyThrows
  private void create__PUT_STATIC_07(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("PUT_STATIC_07", voidDesc(String.class)).build();
    asm.startMethodCode();
    {
      asm.ALOAD(1);

      //
      //
      asm.PUTSTATIC(SomeFields.class.getField("staticField")).space(1);
      //
      //

      asm.RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void PUT_STATIC_01() {
    SomeFields.staticField = null;
    String expected = RND.str(10);
    instance.PUT_STATIC_01(expected);
    assertThat(SomeFields.staticField).isEqualTo(expected);
  }

  @Test
  public void PUT_STATIC_02() {
    SomeFields.staticField = null;
    String expected = RND.str(10);
    instance.PUT_STATIC_02(expected);
    assertThat(SomeFields.staticField).isEqualTo(expected);
  }

  @Test
  public void PUT_STATIC_03() {
    SomeFields.staticField = null;
    String expected = RND.str(10);
    instance.PUT_STATIC_03(expected);
    assertThat(SomeFields.staticField).isEqualTo(expected);
  }

  @Test
  public void PUT_STATIC_04() {
    SomeFields.staticField = null;
    String expected = RND.str(10);
    instance.PUT_STATIC_04(expected);
    assertThat(SomeFields.staticField).isEqualTo(expected);
  }

  @Test
  public void PUT_STATIC_05() {
    SomeFields.staticField = null;
    String expected = RND.str(10);
    instance.PUT_STATIC_05(expected);
    assertThat(SomeFields.staticField).isEqualTo(expected);
  }

  @Test
  public void PUT_STATIC_06() {
    SomeFields.staticField = null;
    String expected = RND.str(10);
    instance.PUT_STATIC_06(expected);
    assertThat(SomeFields.staticField).isEqualTo(expected);
  }

  @Test
  public void PUT_STATIC_07() {
    SomeFields.staticField = null;
    String expected = RND.str(10);
    instance.PUT_STATIC_07(expected);
    assertThat(SomeFields.staticField).isEqualTo(expected);
  }
}
