package kz.greetgo.asm;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_CMP extends AsmClassTest_Parent {

  @Test
  public void comparing_LCMP() throws Exception {

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("comparing_LCMP"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("fieldLong_1", long.class, AccessType.PUBLIC, false);
      cl.generateField("fieldLong_2", long.class, AccessType.PUBLIC, false);
      cl.generateField("result", int.class, AccessType.PUBLIC, false);


      {
        final AsmMethod asm = cl.method("compareIt", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "fieldLong_1", long.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "fieldLong_2", long.class);
          asm.LCMP()
             .PUTFIELD(cl.classPath, "result", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("fieldLong_1").set(instance, 10L);
      generatedClass.getField("fieldLong_2").set(instance, 10L);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isEqualTo(0);
    }
    {
      generatedClass.getField("fieldLong_1").set(instance, 10L);
      generatedClass.getField("fieldLong_2").set(instance, 11L);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldLong_1").set(instance, 17L);
      generatedClass.getField("fieldLong_2").set(instance, 11L);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
  }

  @Test
  public void comparing_FCMPL() throws Exception {

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("comparing_FCMPL"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("fieldFloat_1", float.class, AccessType.PUBLIC, false);
      cl.generateField("fieldFloat_2", float.class, AccessType.PUBLIC, false);
      cl.generateField("result", int.class, AccessType.PUBLIC, false);


      {
        final AsmMethod asm = cl.method("compareIt", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "fieldFloat_1", float.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "fieldFloat_2", float.class);
          asm.FCMPL()
             .PUTFIELD(cl.classPath, "result", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("fieldFloat_1").set(instance, 10f);
      generatedClass.getField("fieldFloat_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isEqualTo(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, 10f);
      generatedClass.getField("fieldFloat_2").set(instance, 11f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, 11f);
      generatedClass.getField("fieldFloat_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, 10f);
      generatedClass.getField("fieldFloat_2").set(instance, Float.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, Float.NaN);
      generatedClass.getField("fieldFloat_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, Float.NaN);
      generatedClass.getField("fieldFloat_2").set(instance, Float.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
  }

  @Test
  public void comparing_FCMPG() throws Exception {

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("comparing_FCMPG"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("fieldFloat_1", float.class, AccessType.PUBLIC, false);
      cl.generateField("fieldFloat_2", float.class, AccessType.PUBLIC, false);
      cl.generateField("result", int.class, AccessType.PUBLIC, false);


      {
        final AsmMethod asm = cl.method("compareIt", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "fieldFloat_1", float.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "fieldFloat_2", float.class);
          asm.FCMPG()
             .PUTFIELD(cl.classPath, "result", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("fieldFloat_1").set(instance, 10f);
      generatedClass.getField("fieldFloat_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isEqualTo(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, 10f);
      generatedClass.getField("fieldFloat_2").set(instance, 11f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, 11f);
      generatedClass.getField("fieldFloat_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, 10f);
      generatedClass.getField("fieldFloat_2").set(instance, Float.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, Float.NaN);
      generatedClass.getField("fieldFloat_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldFloat_1").set(instance, Float.NaN);
      generatedClass.getField("fieldFloat_2").set(instance, Float.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
  }

  @Test
  public void comparing_DCMPL() throws Exception {

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("comparing_DCMPL"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("fieldDouble_1", double.class, AccessType.PUBLIC, false);
      cl.generateField("fieldDouble_2", double.class, AccessType.PUBLIC, false);
      cl.generateField("result", int.class, AccessType.PUBLIC, false);


      {
        final AsmMethod asm = cl.method("compareIt", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "fieldDouble_1", double.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "fieldDouble_2", double.class);
          asm.DCMPL()
             .PUTFIELD(cl.classPath, "result", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("fieldDouble_1").set(instance, 10d);
      generatedClass.getField("fieldDouble_2").set(instance, 10d);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isEqualTo(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, 10d);
      generatedClass.getField("fieldDouble_2").set(instance, 11d);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, 11d);
      generatedClass.getField("fieldDouble_2").set(instance, 10d);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, 10d);
      generatedClass.getField("fieldDouble_2").set(instance, Double.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, Double.NaN);
      generatedClass.getField("fieldDouble_2").set(instance, 10d);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, Double.NaN);
      generatedClass.getField("fieldDouble_2").set(instance, Double.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
  }

  @Test
  public void comparing_DCMPG() throws Exception {

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("comparing_DCMPG"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("fieldDouble_1", double.class, AccessType.PUBLIC, false);
      cl.generateField("fieldDouble_2", double.class, AccessType.PUBLIC, false);
      cl.generateField("result", int.class, AccessType.PUBLIC, false);


      {
        final AsmMethod asm = cl.method("compareIt", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.DUP();
          asm.GETFIELD(cl.classPath, "fieldDouble_1", double.class);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "fieldDouble_2", double.class);
          asm.DCMPG()
             .PUTFIELD(cl.classPath, "result", int.class);

          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("fieldDouble_1").set(instance, 10f);
      generatedClass.getField("fieldDouble_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isEqualTo(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, 10f);
      generatedClass.getField("fieldDouble_2").set(instance, 11f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isLessThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, 11f);
      generatedClass.getField("fieldDouble_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, 10f);
      generatedClass.getField("fieldDouble_2").set(instance, Double.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, Double.NaN);
      generatedClass.getField("fieldDouble_2").set(instance, 10f);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
    {
      generatedClass.getField("fieldDouble_1").set(instance, Double.NaN);
      generatedClass.getField("fieldDouble_2").set(instance, Double.NaN);
      generatedClass.getMethod("compareIt").invoke(instance);
      final int result = (Integer) generatedClass.getField("result").get(instance);
      assertThat(result).isGreaterThan(0);
    }
  }

}
