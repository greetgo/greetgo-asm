package kz.greetgo.asm;

import kz.greetgo.util.RND;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldNameConstants;
import org.objectweb.asm.Type;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigDecimal;

import static kz.greetgo.asm.help.AsmHelper.returnDesc;
import static kz.greetgo.asm.help.AsmHelper.voidDesc;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_INVOKE extends AsmClassTest_Parent {

  Class<?>    generatedClass;
  ParentClass instance;

  @SuppressWarnings("unused")
  public static @NonNull String staticFunction(String input) {
    return "result [" + input + "]";
  }

  public interface HasStatic {
    @SuppressWarnings("unused")
    static @NonNull String interfaceStaticFunction(String input) {
      return "interface [" + input + "]";
    }
  }

  public interface TestInterface {
    @SuppressWarnings("unused")
    default String defaultMethod(String input) {
      return "DefaultMethod [" + input + "]";
    }

    @SuppressWarnings("unused")
    String abstractMethod(String input);
  }

  public static class TestClass {
    @SuppressWarnings("unused")
    public String virtualMethod(String input) {
      return "Base virtual method [" + input + "]";
    }
  }

  @RequiredArgsConstructor
  public static class CoolConstructor {
    public final String     str;
    public final BigDecimal number;
    public final boolean    bool;
  }

  @FieldNameConstants
  public abstract static class ParentClass {

    public String input;
    public String output;

    public TestInterface testInterface;

    public TestClass testClass;

    public String          str;
    public BigDecimal      number;
    public boolean         bool;
    public CoolConstructor coolConstructor;

    public abstract void call_INVOKESTATIC();

    public abstract void call_INVOKESTATICi();

    public abstract void call_INVOKEINTERFACE_defaultMethod();

    public abstract void call_INVOKEINTERFACE_abstractMethod();

    public abstract void call_INVOKEVIRTUAL();

    public abstract void call_INVOKEVIRTUAL2();

    public abstract void call_INVOKE__STATIC();

    public abstract void call_INVOKE__STATICi();

    public abstract void call_INVOKE__INTERFACE_defaultMethod();

    public abstract void call_INVOKE__INTERFACE_abstractMethod();

    public abstract void call_INVOKE__VIRTUAL();

    public abstract void call_INVOKE__CONSTRUCTOR();

  }

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) throws Exception {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(ParentClass.class)
                               .decimalByteCodeNumbers(true)
                               .build()) {

      cl.generateDefaultConstructor();

      create_call_INVOKESTATIC(cl);
      create_call_INVOKESTATICi(cl);
      create_call_INVOKEINTERFACE_defaultMethod(cl);
      create_call_INVOKEINTERFACE_abstractMethod(cl);
      create_call_INVOKEVIRTUAL(cl);
      create_call_INVOKEVIRTUAL2(cl);

      create_call_INVOKE__STATIC(cl);
      create_call_INVOKE__STATICi(cl);
      create_call_INVOKE__INTERFACE_defaultMethod(cl);
      create_call_INVOKE__INTERFACE_abstractMethod(cl);
      create_call_INVOKE__VIRTUAL(cl);

      create_call_INVOKE__CONSTRUCTOR(cl);


      cl.finishClass();

      generatedClass = cl.load();

    }

    instance = (ParentClass) generatedClass.getConstructor().newInstance();
  }

  private void create_call_INVOKESTATIC(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("call_INVOKESTATIC", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKESTATIC(AsmClassTest_INVOKE.class, "staticFunction", returnDesc(String.class, String.class))
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKESTATIC() {

    instance.input = RND.str(10);

    instance.call_INVOKESTATIC();

    assertThat(instance.output).isEqualTo("result [" + instance.input + "]");

  }

  private void create_call_INVOKESTATICi(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("call_INVOKESTATICi", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "input", String.class)
         .INVOKESTATICi(HasStatic.class, "interfaceStaticFunction", returnDesc(String.class, String.class))
         .PUTFIELD(cl.classPath, "output", String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKESTATICi() {

    instance.input = RND.str(10);

    instance.call_INVOKESTATICi();

    assertThat(instance.output).isEqualTo("interface [" + instance.input + "]");

  }

  private void create_call_INVOKEINTERFACE_defaultMethod(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("call_INVOKEINTERFACE_defaultMethod", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.testInterface, TestInterface.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKEINTERFACE(TestInterface.class, "defaultMethod", returnDesc(String.class, String.class))
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKEINTERFACE_defaultMethod_1() {

    instance.input = RND.str(10);

    //noinspection Convert2Lambda
    instance.testInterface = new TestInterface() {
      @Override
      public String abstractMethod(String input) {
        throw new RuntimeException("h1ce2xEE8u :: do not call it");
      }
    };

    instance.call_INVOKEINTERFACE_defaultMethod();

    assertThat(instance.output).isEqualTo("DefaultMethod [" + instance.input + "]");

  }

  @Test
  public void call_INVOKEINTERFACE_defaultMethod_2() {

    instance.input = RND.str(10);

    instance.testInterface = new TestInterface() {
      @Override
      public String defaultMethod(String input) {
        return "Reloaded default method [" + input + "]";
      }

      @Override
      public String abstractMethod(String input) {
        throw new RuntimeException("yCLHUp1rZT :: do not call it");
      }
    };

    instance.call_INVOKEINTERFACE_defaultMethod();

    assertThat(instance.output).isEqualTo("Reloaded default method [" + instance.input + "]");

  }


  private void create_call_INVOKEINTERFACE_abstractMethod(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("call_INVOKEINTERFACE_abstractMethod", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.testInterface, TestInterface.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKEINTERFACE(TestInterface.class, "abstractMethod", returnDesc(String.class, String.class))
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKEINTERFACE_abstractMethod() {

    instance.input = RND.str(10);

    //noinspection Convert2Lambda
    instance.testInterface = new TestInterface() {
      @Override
      public String abstractMethod(String input) {
        return "abstractMethod [" + input + "]";
      }
    };

    instance.call_INVOKEINTERFACE_abstractMethod();

    assertThat(instance.output).isEqualTo("abstractMethod [" + instance.input + "]");

  }


  private void create_call_INVOKEVIRTUAL(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("call_INVOKEVIRTUAL", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.testClass, TestClass.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKEVIRTUAL(TestClass.class, "virtualMethod", returnDesc(String.class, String.class))
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  private void create_call_INVOKEVIRTUAL2(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("call_INVOKEVIRTUAL2", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.testClass, TestClass.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKEVIRTUAL(Type.getInternalName(TestClass.class), "virtualMethod", returnDesc(String.class, String.class))
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKEVIRTUAL_1() {

    instance.input = RND.str(10);

    instance.testClass = new TestClass() {
      @Override
      public String virtualMethod(String input) {
        return "(Overridden) " + super.virtualMethod(input);
      }
    };

    instance.call_INVOKEVIRTUAL();

    assertThat(instance.output).isEqualTo("(Overridden) Base virtual method [" + instance.input + "]");

  }

  @Test
  public void call_INVOKEVIRTUAL_2() {

    instance.input = RND.str(10);

    instance.testClass = new TestClass();

    instance.call_INVOKEVIRTUAL2();

    assertThat(instance.output).isEqualTo("Base virtual method [" + instance.input + "]");

  }

  @Test
  public void call_INVOKEVIRTUAL_3() {

    instance.input = RND.str(10);

    instance.testClass = new TestClass();

    instance.call_INVOKEVIRTUAL();

    assertThat(instance.output).isEqualTo("Base virtual method [" + instance.input + "]");

  }

  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////


  private void create_call_INVOKE__STATIC(@NonNull AsmClass cl) throws Exception {

    final Method method = AsmClassTest_INVOKE.class.getMethod("staticFunction", String.class);

    final AsmMethod asm = cl.method("call_INVOKE__STATIC", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKE(method)
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKE__STATIC() {

    instance.input = RND.str(10);

    instance.call_INVOKE__STATIC();

    assertThat(instance.output).isEqualTo("result [" + instance.input + "]");

  }

  private void create_call_INVOKE__STATICi(@NonNull AsmClass cl) throws Exception {

    final Method method = HasStatic.class.getMethod("interfaceStaticFunction", String.class);

    final AsmMethod asm = cl.method("call_INVOKE__STATICi", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "input", String.class)
         .INVOKE(method)
         .PUTFIELD(cl.classPath, "output", String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKE__STATICi() {

    instance.input = RND.str(10);

    instance.call_INVOKE__STATICi();

    assertThat(instance.output).isEqualTo("interface [" + instance.input + "]");

  }

  private void create_call_INVOKE__INTERFACE_defaultMethod(@NonNull AsmClass cl) throws Exception {

    final Method method = TestInterface.class.getMethod("defaultMethod", String.class);

    final AsmMethod asm = cl.method("call_INVOKE__INTERFACE_defaultMethod", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.testInterface, TestInterface.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKE(method)
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKE__INTERFACE_defaultMethod_1() {

    instance.input = RND.str(10);

    //noinspection Convert2Lambda
    instance.testInterface = new TestInterface() {
      @Override
      public String abstractMethod(String input) {
        throw new RuntimeException("h1ce2xEE8u :: do not call it");
      }
    };

    instance.call_INVOKE__INTERFACE_defaultMethod();

    assertThat(instance.output).isEqualTo("DefaultMethod [" + instance.input + "]");

  }

  @Test
  public void call_INVOKE__INTERFACE_defaultMethod_2() {

    instance.input = RND.str(10);

    instance.testInterface = new TestInterface() {
      @Override
      public String defaultMethod(String input) {
        return "Reloaded default method [" + input + "]";
      }

      @Override
      public String abstractMethod(String input) {
        throw new RuntimeException("yCLHUp1rZT :: do not call it");
      }
    };

    instance.call_INVOKE__INTERFACE_defaultMethod();

    assertThat(instance.output).isEqualTo("Reloaded default method [" + instance.input + "]");

  }


  private void create_call_INVOKE__INTERFACE_abstractMethod(@NonNull AsmClass cl) throws Exception {

    final Method method = TestInterface.class.getMethod("abstractMethod", String.class);

    final AsmMethod asm = cl.method("call_INVOKE__INTERFACE_abstractMethod", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.testInterface, TestInterface.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKE(method)
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKE__INTERFACE_abstractMethod() {

    instance.input = RND.str(10);

    //noinspection Convert2Lambda
    instance.testInterface = new TestInterface() {
      @Override
      public String abstractMethod(String input) {
        return "abstractMethod [" + input + "]";
      }
    };

    instance.call_INVOKE__INTERFACE_abstractMethod();

    assertThat(instance.output).isEqualTo("abstractMethod [" + instance.input + "]");

  }


  private void create_call_INVOKE__VIRTUAL(@NonNull AsmClass cl) throws Exception {

    final Method method = TestClass.class.getMethod("virtualMethod", String.class);

    final AsmMethod asm = cl.method("call_INVOKE__VIRTUAL", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.testClass, TestClass.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.input, String.class)
         .INVOKE(method)
         .PUTFIELD(cl.classPath, ParentClass.Fields.output, String.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }


  @Test
  public void call_INVOKE__VIRTUAL_1() {

    instance.input = RND.str(10);

    instance.testClass = new TestClass() {
      @Override
      public String virtualMethod(String input) {
        return "(Overridden) " + super.virtualMethod(input);
      }
    };

    instance.call_INVOKE__VIRTUAL();

    assertThat(instance.output).isEqualTo("(Overridden) Base virtual method [" + instance.input + "]");

  }

  @Test
  public void call_INVOKE__VIRTUAL_3() {

    instance.input = RND.str(10);

    instance.testClass = new TestClass();

    instance.call_INVOKE__VIRTUAL();

    assertThat(instance.output).isEqualTo("Base virtual method [" + instance.input + "]");

  }

  @SneakyThrows
  private void create_call_INVOKE__CONSTRUCTOR(@NonNull AsmClass cl) {

    final Constructor<CoolConstructor> constructor = CoolConstructor.class.getConstructor(String.class, BigDecimal.class, boolean.class);

    final AsmMethod asm = cl.method("call_INVOKE__CONSTRUCTOR", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1)
         .ALOAD(0)
         .NEW(CoolConstructor.class)
         .DUP()
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.str, String.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.number, BigDecimal.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, ParentClass.Fields.bool, boolean.class)
         .INVOKE(constructor)
         .PUTFIELD(cl.classPath, ParentClass.Fields.coolConstructor, CoolConstructor.class)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void call_INVOKE__CONSTRUCTOR() {

    instance.input = RND.str(10);

    instance.str    = RND.str(10);
    instance.number = RND.bd(100_000_000L, 10);
    instance.bool   = RND.bool();

    instance.coolConstructor = null;

    instance.call_INVOKE__CONSTRUCTOR();

    assertThat(instance.coolConstructor).isNotNull();
    assertThat(instance.coolConstructor.str).isEqualTo(instance.str);
    assertThat(instance.coolConstructor.number).isEqualTo(instance.number);
    assertThat(instance.coolConstructor.bool).isEqualTo(instance.bool);

  }

}
