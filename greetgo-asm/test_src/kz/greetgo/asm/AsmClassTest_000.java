package kz.greetgo.asm;

import org.assertj.core.data.Offset;
import org.objectweb.asm.Type;
import org.testng.annotations.Test;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_000 extends AsmClassTest_Parent {

  @Test
  public void simple() throws Exception {
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + sdf.format(new Date()) + ".Test")
                               .src(root.resolve("src_simple"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.generateField("field1", long.class, AccessType.PUBLIC, false);
      cl.generateField("field2", long.class, AccessType.PUBLIC, false);

      {
        final AsmMethod asm = cl.method("getFieldsSum", "()J").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0)
             .GETFIELD(cl.classPath, "field1", "J")
          ;

          asm.ALOAD(0)
             .GETFIELD(cl.classPath, "field2", "J")
          ;

          asm.LADD()
             .LRETURN()
          ;
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("field1").set(instance, 123);
    generatedClass.getField("field2").set(instance, 700);

    final Long sum = (Long) generatedClass.getMethod("getFieldsSum").invoke(instance);

    System.out.println("0BgyMoBuJV :: Fields sum = " + sum);

    assertThat(sum).isEqualTo(700 + 123);

  }

  @Test
  public void findSquareRoots_double() throws Exception {
    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("findSquareRoots_double"))
                               .build()) {

      cl.generateDefaultConstructor();

      {
        final AsmMethod asm = cl.method("findSquareRoots", "(DDD)[D").static_(true).build();// a=0 b=2 c=4 D=6 ret=8 X=9
        asm.startMethodCode();
        {
          asm.rem("Вычисляем дискриминант:  a=0 b=2 c=4 D=6")
             .DLOAD(2)     //  STACK: b
             .DUP2()       //  STACK: b           b
             .DMUL()       //  STACK: b^2
             .BIPUSH(4)    //  STACK: b^2         4i
             .I2D()        //  STACK: b^2         4d
             .DLOAD(0)     //  STACK: b^2         4d       a
             .DLOAD(4)     //  STACK: b^2         4d       a        c
             .DMUL()       //  STACK: b^2         4d       a*c
             .DMUL()       //  STACK: b^2         4*a*c
             .DSUB()       //  STACK: b^2-a*c*4
             .DSTORE(6)
             .space(1)
          ;
          asm.DLOAD(6)
             .DCONST_0()
             .DCMPL()
             .IFGE("D_IS_OK")
             .space(1)
          ;
          asm.ACONST_NULL()
             .ARETURN()
             .space(1)
          ;
          asm.label("D_IS_OK")
             .DLOAD(6)
             .INVOKESTATIC(Math.class, "sqrt", "(D)D", false) // STACK: sqrt(b^2-a*c*4)
             .DSTORE(6)
             .space(1)
          ;
          asm.space(1)
             .rem("Создаём массив double из двух элементов")
             .BIPUSH(2)
             .NEWARRAY_double()
             .ASTORE(8)
             .space(1)
          ;
          asm.space(1)
             .rem("Вычисляем первый корень: (-b+sqrt(D))/(2*a), и кладём его в массив на 0 место: a=0 b=2 c=4 D=6 ret=8")
             .DLOAD(2)      //   STACK: b
             .DNEG()        //   STACK: -b
             .DLOAD(6)      //   STACK: -b                   sqrt(D)
             .DADD()        //   STACK: -b+sqrt(D)
             .BIPUSH(2)     //   STACK: -b+sqrt(D)           2i
             .I2D()         //   STACK: -b+sqrt(D)           2d
             .DLOAD(0)      //   STACK: -b+sqrt(D)           2d          a
             .DMUL()        //   STACK: -b+sqrt(D)           2*a
             .DDIV()        //   STACK: (-b+sqrt(D))/(2*a)
             .DSTORE(9)     //   X := STACK.pop
             .space(1)
          ;

          asm.ALOAD(8)      //   STACK: ret
             .ICONST_0()    //   STACK: ret          0
             .DLOAD(9)      //   STACK: ret          0         X
             .DASTORE()     //   ret[0] := X
             .space(1)
          ;

          asm.rem("Вычисляем второй корень: (-b-sqrt(D))/(2*a): b=1")
             .DLOAD(2)      //   STACK: b
             .DNEG()        //   STACK: -b
             .DLOAD(6)      //   STACK: -b                   sqrt(D)
             .DSUB()        //   STACK: -b-sqrt(D)
             .BIPUSH(2)     //   STACK: -b-sqrt(D)           2i
             .I2D()         //   STACK: -b-sqrt(D)           2d
             .DLOAD(0)      //   STACK: -b-sqrt(D)           2d          a
             .DMUL()        //   STACK: -b-sqrt(D)           2*a
             .DDIV()        //   STACK: (-b-sqrt(D))/(2*a)
             .DSTORE(9)     //   X := STACK.pop
             .space(1)
          ;
          asm.ALOAD(8)      //   STACK: ret
             .ICONST_1()    //   STACK: ret          1
             .DLOAD(9)      //   STACK: ret          1         X
             .DASTORE()     //   ret[0] := X
             .space(1)
          ;
          asm.space(1)
             .rem("Результат готов - возвращаем его")
             .label("RETURN")
             .ALOAD(8)
             .ARETURN()
             .space(1)
          ;
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();
    }

    final Method findSquareRoots = generatedClass.getMethod("findSquareRoots", double.class, double.class, double.class);

    {
      final double[] roots = (double[]) findSquareRoots.invoke(null, 1, -1, -6);

      assertThat(roots).hasSize(2);
      assertThat(roots[0]).isEqualTo(+3, Offset.offset(1e-10));
      assertThat(roots[1]).isEqualTo(-2, Offset.offset(1e-10));
    }
    {
      final double[] roots = (double[]) findSquareRoots.invoke(null, 1, -1, +6);

      assertThat(roots).isNull();
    }

  }

  public abstract static class ForTest {
    protected ForTest() {}

    public double var;

    public int Q;

    public double X;
    public int    Y;
    public double RESULT;

    public double X1;
    public int    Y1;
    public double RESULT1;

    public double X2;
    public int    Y2;
    public double RESULT2;

    public abstract double operations(int A, long B, String C);
  }

  @Test
  public void usingLocalVariableGenerator() throws Exception {

    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("usingLocalVariableGenerator"))
                               .ext(ForTest.class)
                               .build()) {

      cl.generateDefaultConstructor();

      {
        //region Types
        // Параметры метода
        Type intType    = Type.getType(int.class);// переменная A
        Type longType   = Type.getType(long.class);// переменная B
        Type stringType = Type.getType(String.class);// переменная C

        // Возвращаемый тип
        Type doubleType = Type.getType(double.class);

        // Получение дескриптора метода
        String methodDescriptor = Type.getMethodDescriptor(doubleType, intType, longType, stringType);
        //endregion

        int localThis = 0;
        int localA    = 1;//int
        int localB    = 2;//local
        int localC    = 4;//String

        final AsmMethod asm = cl.method("operations", methodDescriptor).build();// a=0 b=2 c=4 D=6 ret=8 X=9
        asm.startMethodCode();
        {
          //region Step 1
          int localSumAB = asm.newVarIndex(long.class);
          asm.rem("Вычисляем localSumAB = localA + localB")
             .ILOAD(localA, "A")            //  STACK: A
             .I2L()                         //  STACK: A(long)
             .LLOAD(localB, "B")            //  STACK: A(long)       B(long)
             .LADD()                        //  STACK: (A+B)(long)
             .LSTORE(localSumAB, "SumAB")   //  STACK:
             .space(1);
          //endregion

          //region Печатаем SumAB и C и this.var
          asm.rem("Печатаем SumAB и C и this.var")
             .GETSTATIC(System.class.getField("out"))
             .NEW(StringBuilder.class)
             .DUP()
             .INVOKE(StringBuilder.class.getConstructor())
             .LDC("SumAB = ")
             .INVOKE(StringBuilder.class.getMethod("append", String.class))
             .LLOAD(localSumAB, "SumAB")
             .INVOKE(StringBuilder.class.getMethod("append", long.class))
             .LDC(", C = [[")
             .INVOKE(StringBuilder.class.getMethod("append", String.class))
             .ALOAD(localC, "C")
             .INVOKE(StringBuilder.class.getMethod("append", String.class))
             .LDC("]], this.var = ")
             .INVOKE(StringBuilder.class.getMethod("append", String.class))
             .ALOAD(localThis, "this")
             .GETFIELD(ForTest.class.getField("var"))
             .INVOKE(StringBuilder.class.getMethod("append", double.class))
             .INVOKE(StringBuilder.class.getMethod("toString"))
             .INVOKE(PrintStream.class.getMethod("println", String.class))
             .space(1)
          ;
          //endregion

          int localQ = asm.newVarIndex(int.class);
          {
            asm.ALOAD(localThis, "this")
               .GETFIELD(ForTest.class.getField("Q"))
               .ISTORE(localQ, "Q")
               .space(1);
          }
          {
            int localX = asm.newVarIndex(double.class);
            asm.ALOAD(localThis, "this")
               .GETFIELD(ForTest.class.getField("X"))
               .DSTORE(localX, "X")
               .space(1);

            int localY = asm.newVarIndex(int.class);
            asm.ALOAD(localThis, "this")
               .GETFIELD(ForTest.class.getField("Y"))
               .ISTORE(localY, "Y")
               .space(1);

            int localResult = asm.newVarIndex(double.class);
            asm.DLOAD(localX, "X")
               .ILOAD(localY, "Y")
               .I2D()
               .DMUL()
               .DSTORE(localResult, "Result")
               .space(1);

            asm.ALOAD(localThis, "this")
               .DLOAD(localResult, "Result")
               .PUTFIELD(ForTest.class.getField("RESULT"))
               .space(1);
          }
          {
            int localX = asm.newVarIndex(double.class);
            asm.ALOAD(localThis, "this")
               .GETFIELD(ForTest.class.getField("X1"))
               .DSTORE(localX, "X")
               .space(1);

            int localY = asm.newVarIndex(int.class);
            asm.ALOAD(localThis, "this")
               .GETFIELD(ForTest.class.getField("Y1"))
               .ISTORE(localY, "Y")
               .space(1);

            int localResult = asm.newVarIndex(double.class);
            asm.DLOAD(localX, "X")
               .ILOAD(localY, "Y")
               .I2D()
               .DMUL()
               .DSTORE(localResult, "Result")
               .space(1);

            asm.ALOAD(localThis, "this")
               .DLOAD(localResult, "Result")
               .PUTFIELD(ForTest.class.getField("RESULT1"))
               .space(1);
          }
          {
            int localX = asm.newVarIndex(double.class);
            asm.ALOAD(localThis, "this")
               .GETFIELD(ForTest.class.getField("X2"))
               .DSTORE(localX, "X")
               .space(1);

            int localY = asm.newVarIndex(int.class);
            asm.ALOAD(localThis, "this")
               .GETFIELD(ForTest.class.getField("Y2"))
               .ISTORE(localY, "Y")
               .space(1);

            int localResult = asm.newVarIndex(double.class);
            asm.DLOAD(localX, "X")
               .ILOAD(localY, "Y")
               .I2D()
               .DMUL()
               .ILOAD(localQ, "Q")
               .I2D()
               .DMUL()
               .DSTORE(localResult, "Result")
               .space(1);

            asm.ALOAD(localThis, "this")
               .DLOAD(localResult, "Result")
               .PUTFIELD(ForTest.class.getField("RESULT2"))
               .space(1);
          }

          asm.rem("Преобразуем localSumAB в double и возвращаем")
             .LLOAD(localSumAB, "SumAB")
             .L2D()
             .DRETURN()
             .space(1);
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();
    }

    final ForTest forTest = (ForTest) generatedClass.getConstructor().newInstance();
    forTest.var = 34.5;

    final double result = forTest.operations(3, 4, "Hi");
    assertThat(result).isEqualTo(3 + 4);
  }
}
