package kz.greetgo.asm;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_NEG extends AsmClassTest_Parent {

  @Test
  public void neg() throws Exception {
    final Class<?> generatedClass;

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve("neg"))
                               .build()) {

      cl.generateDefaultConstructor();

      cl.spaceLine();

      cl.generateField("src_int", int.class, AccessType.PUBLIC, false);
      cl.generateField("src_long", long.class, AccessType.PUBLIC, false);
      cl.generateField("src_float", float.class, AccessType.PUBLIC, false);
      cl.generateField("src_double", double.class, AccessType.PUBLIC, false);

      cl.generateField("dest_int", int.class, AccessType.PUBLIC, false);
      cl.generateField("dest_long", long.class, AccessType.PUBLIC, false);
      cl.generateField("dest_float", float.class, AccessType.PUBLIC, false);
      cl.generateField("dest_double", double.class, AccessType.PUBLIC, false);

      {
        final AsmMethod asm = cl.method("neg", "()V").build();
        asm.startMethodCode();
        {
          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "src_int", int.class);
          asm.INEG()
             .PUTFIELD(cl.classPath, "dest_int", int.class);

          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "src_long", long.class);
          asm.LNEG()
             .PUTFIELD(cl.classPath, "dest_long", long.class);

          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "src_float", float.class);
          asm.FNEG()
             .PUTFIELD(cl.classPath, "dest_float", float.class);

          asm.ALOAD(0);
          asm.ALOAD(0);
          asm.GETFIELD(cl.classPath, "src_double", double.class);
          asm.DNEG()
             .PUTFIELD(cl.classPath, "dest_double", double.class);

          asm.space(1)
             .RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    int    src_int    = 34215;
    long   src_long   = 437862585435L;
    float  src_float  = 4325.432f;
    double src_double = 54325435.654d;

    final Object instance = generatedClass.getConstructor().newInstance();

    generatedClass.getField("src_int").set(instance, src_int);
    generatedClass.getField("src_long").set(instance, src_long);
    generatedClass.getField("src_float").set(instance, src_float);
    generatedClass.getField("src_double").set(instance, src_double);

    generatedClass.getMethod("neg").invoke(instance);

    int    dest_int    = (Integer) generatedClass.getField("dest_int").get(instance);
    long   dest_long   = (Long) generatedClass.getField("dest_long").get(instance);
    float  dest_float  = (Float) generatedClass.getField("dest_float").get(instance);
    double dest_double = (Double) generatedClass.getField("dest_double").get(instance);

    assertThat(dest_int).isEqualTo(-src_int);
    assertThat(dest_long).isEqualTo(-src_long);
    assertThat(dest_float).isEqualTo(-src_float);
    assertThat(dest_double).isEqualTo(-src_double);
  }

}
