package kz.greetgo.asm;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;

import static kz.greetgo.asm.help.AsmHelper.returnDesc;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_COMPARE extends AsmClassTest_Parent {
  @SuppressWarnings("unused")
  public static abstract class JavaFields {

    public byte       left_1;
    public short      left_2;
    public int        left_3;
    public long       left_4;
    public float      left_5;
    public double     left_6;
    public Byte       left_7;
    public Short      left_8;
    public Integer    left_9;
    public Long       left_10;
    public Float      left_11;
    public Double     left_12;
    public BigInteger left_13;
    public BigDecimal left_14;
    public String     left_15;

    public byte       right_1;
    public short      right_2;
    public int        right_3;
    public long       right_4;
    public float      right_5;
    public double     right_6;
    public Byte       right_7;
    public Short      right_8;
    public Integer    right_9;
    public Long       right_10;
    public Float      right_11;
    public Double     right_12;
    public BigInteger right_13;
    public BigDecimal right_14;
    public String     right_15;

    public abstract int compare_1();

    public abstract int compare_2();

    public abstract int compare_3();

    public abstract int compare_4();

    public abstract int compare_5();

    public abstract int compare_6();

    public abstract int compare_7();

    public abstract int compare_8();

    public abstract int compare_9();

    public abstract int compare_10();

    public abstract int compare_11();

    public abstract int compare_12();

    public abstract int compare_13();

    public abstract int compare_14();

    public abstract int compare_15();

  }

  Class<?>   generatedClass;
  JavaFields fields;

  @BeforeMethod
  @SneakyThrows
  public void generateClass(@NonNull Method testMethod) {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(JavaFields.class)
                               .build()) {

      cl.generateDefaultConstructor();


      for (int c = 1; c <= 15; c++) {

        final AsmMethod asm = cl.method(("compare_" + c), returnDesc(int.class)).build();
        asm.startMethodCode();
        asm.newVarIndex(Object.class);// place for this.0
        {
          final Field left  = JavaFields.class.getField("left_" + c);
          final Field right = JavaFields.class.getField("right_" + c);
          if (!left.getType().equals(right.getType())) {
            throw new RuntimeException("zBW2Ie1yNl :: Different left and right types: " + left.getType() + " and " + right.getType());
          }

          if (c % 2 == 1) {
            asm.ALOAD(0)
               .GETFIELD(left)
               .ALOAD(0)
               .GETFIELD(right)
               .CMPL(left.getType())
               .IRETURN();
          } else {
            asm.ALOAD(0)
               .GETFIELD(left)
               .ALOAD(0)
               .GETFIELD(right)
               .CMPG(left.getType())
               .IRETURN();
          }
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    fields = (JavaFields) generatedClass.getConstructor().newInstance();
  }

  private static int correct(int x) {
    return Integer.compare(x, 0);
  }

  @DataProvider
  private Object[][] compareDataProvider() {
    return new Object[][]{
      //@formatter:off
      {  -10,   34  },
      {  -10,  -10  },
      {   11,   11  },
      {  111,   11  },
      {  -11, -111  },
      //@formatter:on
    };
  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_1(int left, int right) {

    fields.left_1  = (byte) left;
    fields.right_1 = (byte) right;

    //
    //
    assertThat(correct(fields.compare_1())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_2(int left, int right) {

    fields.left_2  = (short) left;
    fields.right_2 = (short) right;

    //
    //
    assertThat(correct(fields.compare_2())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_3(int left, int right) {

    fields.left_3  = left;
    fields.right_3 = right;

    //
    //
    assertThat(correct(fields.compare_3())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_4(int left, int right) {

    fields.left_4  = left;
    fields.right_4 = right;

    //
    //
    assertThat(correct(fields.compare_4())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_5(int left, int right) {

    fields.left_5  = left;
    fields.right_5 = right;

    //
    //
    assertThat(correct(fields.compare_5())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_6(int left, int right) {

    fields.left_6  = left;
    fields.right_6 = right;

    //
    //
    assertThat(correct(fields.compare_6())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_7(int left, int right) {

    fields.left_7  = (byte) left;
    fields.right_7 = (byte) right;

    //
    //
    assertThat(correct(fields.compare_7())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_8(int left, int right) {

    fields.left_8  = (short) left;
    fields.right_8 = (short) right;

    //
    //
    assertThat(correct(fields.compare_8())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_9(int left, int right) {

    fields.left_9  = left;
    fields.right_9 = right;

    //
    //
    assertThat(correct(fields.compare_9())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_10(int left, int right) {

    fields.left_10  = (long) left;
    fields.right_10 = (long) right;

    //
    //
    assertThat(correct(fields.compare_10())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_11(int left, int right) {

    fields.left_11  = (float) left;
    fields.right_11 = (float) right;

    //
    //
    assertThat(correct(fields.compare_11())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_12(int left, int right) {

    fields.left_12  = (double) left;
    fields.right_12 = (double) right;

    //
    //
    assertThat(correct(fields.compare_12())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_13(int left, int right) {

    fields.left_13  = BigInteger.valueOf(left);
    fields.right_13 = BigInteger.valueOf(right);

    //
    //
    assertThat(correct(fields.compare_13())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_14(int left, int right) {

    fields.left_14  = BigDecimal.valueOf(left);
    fields.right_14 = BigDecimal.valueOf(right);

    //
    //
    assertThat(correct(fields.compare_14())).isEqualTo(Integer.compare(left, right));
    //
    //

  }

  @Test(dataProvider = "compareDataProvider")
  public void compare_15(int left, int right) {

    fields.left_15  = "" + left;
    fields.right_15 = "" + right;

    //
    //
    assertThat(fields.compare_15()).isEqualTo(("" + left).compareTo("" + right));
    //
    //

  }
}
