package kz.greetgo.asm;

import lombok.NonNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_POP_DUP extends AsmClassTest_Parent {
  Class<?> generatedClass;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) {
    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .build()) {

      cl.generateField("in1", int.class, AccessType.PUBLIC, false)
        .generateField("in2", int.class, AccessType.PUBLIC, false)
        .generateField("in3", int.class, AccessType.PUBLIC, false)
        .generateField("in4", int.class, AccessType.PUBLIC, false);

      cl.generateField("out1", int.class, AccessType.PUBLIC, false)
        .generateField("out2", int.class, AccessType.PUBLIC, false)
        .generateField("out3", int.class, AccessType.PUBLIC, false)
        .generateField("out4", int.class, AccessType.PUBLIC, false)
        .generateField("out5", int.class, AccessType.PUBLIC, false)
        .generateField("out6", int.class, AccessType.PUBLIC, false);

      cl.generateDefaultConstructor();

      {
        final AsmMethod asm = cl.method("do__POP", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 2);
          asm.space(2)
             .POP(Object.class)
             .space(2);
          saveFields(cl, asm, 1);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("do__POP2", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 3);
          asm.space(2)
             .POP2()
             .space(2);
          saveFields(cl, asm, 1);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("do__DUP", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 1);
          asm.space(2)
             .DUP()
             .space(2);
          saveFields(cl, asm, 2);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("do__DUP_X1", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 2);
          asm.space(2)
             .DUP_X1()
             .space(2);
          saveFields(cl, asm, 3);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("do__DUP_X2", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 3);
          asm.space(2)
             .DUP_X2()
             .space(2);
          saveFields(cl, asm, 4);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("do__DUP2", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 2);
          asm.space(2)
             .DUP2()
             .space(2);
          saveFields(cl, asm, 4);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("do__DUP2_X1", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 3);
          asm.space(2)
             .DUP2_X1()
             .space(2);
          saveFields(cl, asm, 5);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }
      {
        final AsmMethod asm = cl.method("do__DUP2_X2", "()V").build();
        asm.startMethodCode();
        {
          loadFields(cl, asm, 4);
          asm.space(2)
             .DUP2_X2()
             .space(2);
          saveFields(cl, asm, 6);
          asm.RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();
    }
  }

  @SuppressWarnings("SameParameterValue")
  private static void loadFields(@NonNull AsmClass cl, @NonNull AsmMethod asm, int count) {
    for (int i = 1; i <= count; i++) {
      asm.ALOAD(0)
         .GETFIELD(cl.classPath, "in" + i, "I");
    }
  }

  @SuppressWarnings("SameParameterValue")
  private static void saveFields(@NonNull AsmClass cl, @NonNull AsmMethod asm, int count) {
    for (int i = count; i >= 1; i--) {
      asm.ALOAD(0)
         .SWAP()
         .PUTFIELD(cl.classPath, "out" + i, "I");
    }
  }

  @Test
  public void stackOperations_POP() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();

    {
      generatedClass.getField("in1").set(instance, 1);
      generatedClass.getField("in2").set(instance, 2);

      generatedClass.getMethod("do__POP").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);

      System.out.println("UQuxVLkDoI :: out1 = " + out1);

      assertThat(out1).isEqualTo(1);
    }
  }

  @Test
  public void stackOperations_POP2() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("in1").set(instance, 1);
      generatedClass.getField("in2").set(instance, 2);
      generatedClass.getField("in3").set(instance, 3);

      generatedClass.getMethod("do__POP2").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);

      System.out.println("3cMAMmsOw4 :: out1 = " + out1);

      assertThat(out1).isEqualTo(1);
    }
  }

  @Test
  public void stackOperations_DUP() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("in1").set(instance, 17);

      generatedClass.getMethod("do__DUP").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);
      int out2 = (Integer) generatedClass.getField("out2").get(instance);

      System.out.println("rth5zdRNEe :: out1 = " + out1);
      System.out.println("rth5zdRNEe :: out2 = " + out2);

      assertThat(out1).isEqualTo(17);
      assertThat(out2).isEqualTo(17);
    }
  }

  @Test
  public void stackOperations_DUP_X1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("in1").set(instance, 1);
      generatedClass.getField("in2").set(instance, 2);

      generatedClass.getMethod("do__DUP_X1").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);
      int out2 = (Integer) generatedClass.getField("out2").get(instance);
      int out3 = (Integer) generatedClass.getField("out3").get(instance);

      System.out.println("VLA2U3yMyg :: out1 = " + out1);
      System.out.println("VLA2U3yMyg :: out2 = " + out2);
      System.out.println("VLA2U3yMyg :: out3 = " + out3);

      assertThat(out1).isEqualTo(2);
      assertThat(out2).isEqualTo(1);
      assertThat(out3).isEqualTo(2);
    }
  }

  @Test
  public void stackOperations_DUP_X2() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("in1").set(instance, 1);
      generatedClass.getField("in2").set(instance, 2);
      generatedClass.getField("in3").set(instance, 3);

      generatedClass.getMethod("do__DUP_X2").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);
      int out2 = (Integer) generatedClass.getField("out2").get(instance);
      int out3 = (Integer) generatedClass.getField("out3").get(instance);
      int out4 = (Integer) generatedClass.getField("out4").get(instance);

      System.out.println("Fng1qunEGT :: out1 = " + out1);
      System.out.println("Fng1qunEGT :: out2 = " + out2);
      System.out.println("Fng1qunEGT :: out3 = " + out3);
      System.out.println("Fng1qunEGT :: out4 = " + out4);

      assertThat(out1).isEqualTo(3);
      assertThat(out2).isEqualTo(1);
      assertThat(out3).isEqualTo(2);
      assertThat(out4).isEqualTo(3);
    }
  }

  @Test
  public void stackOperations_DUP2() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("in1").set(instance, 1);
      generatedClass.getField("in2").set(instance, 2);

      generatedClass.getMethod("do__DUP2").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);
      int out2 = (Integer) generatedClass.getField("out2").get(instance);
      int out3 = (Integer) generatedClass.getField("out3").get(instance);
      int out4 = (Integer) generatedClass.getField("out4").get(instance);

      System.out.println("5FPY4L4zfm :: out1 = " + out1);
      System.out.println("5FPY4L4zfm :: out2 = " + out2);
      System.out.println("5FPY4L4zfm :: out3 = " + out3);
      System.out.println("5FPY4L4zfm :: out4 = " + out4);

      assertThat(out1).isEqualTo(1);
      assertThat(out2).isEqualTo(2);
      assertThat(out3).isEqualTo(1);
      assertThat(out4).isEqualTo(2);
    }
  }

  @Test
  public void stackOperations_DUP2_X1() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("in1").set(instance, 1);
      generatedClass.getField("in2").set(instance, 2);
      generatedClass.getField("in3").set(instance, 3);

      generatedClass.getMethod("do__DUP2_X1").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);
      int out2 = (Integer) generatedClass.getField("out2").get(instance);
      int out3 = (Integer) generatedClass.getField("out3").get(instance);
      int out4 = (Integer) generatedClass.getField("out4").get(instance);
      int out5 = (Integer) generatedClass.getField("out5").get(instance);

      System.out.println("TvKjq4HQQ2 :: out1 = " + out1);
      System.out.println("TvKjq4HQQ2 :: out2 = " + out2);
      System.out.println("TvKjq4HQQ2 :: out3 = " + out3);
      System.out.println("TvKjq4HQQ2 :: out4 = " + out4);
      System.out.println("TvKjq4HQQ2 :: out5 = " + out5);

      assertThat(out1).isEqualTo(2);
      assertThat(out2).isEqualTo(3);
      assertThat(out3).isEqualTo(1);
      assertThat(out4).isEqualTo(2);
      assertThat(out5).isEqualTo(3);
    }
  }

  @Test
  public void stackOperations_DUP2_X2() throws Exception {
    final Object instance = generatedClass.getConstructor().newInstance();
    {
      generatedClass.getField("in1").set(instance, 1);
      generatedClass.getField("in2").set(instance, 2);
      generatedClass.getField("in3").set(instance, 3);
      generatedClass.getField("in4").set(instance, 4);

      generatedClass.getMethod("do__DUP2_X2").invoke(instance);

      int out1 = (Integer) generatedClass.getField("out1").get(instance);
      int out2 = (Integer) generatedClass.getField("out2").get(instance);
      int out3 = (Integer) generatedClass.getField("out3").get(instance);
      int out4 = (Integer) generatedClass.getField("out4").get(instance);
      int out5 = (Integer) generatedClass.getField("out5").get(instance);
      int out6 = (Integer) generatedClass.getField("out6").get(instance);

      System.out.println("fz1uxAP1HO :: out1 = " + out1);
      System.out.println("fz1uxAP1HO :: out2 = " + out2);
      System.out.println("fz1uxAP1HO :: out3 = " + out3);
      System.out.println("fz1uxAP1HO :: out4 = " + out4);
      System.out.println("fz1uxAP1HO :: out5 = " + out5);
      System.out.println("fz1uxAP1HO :: out6 = " + out6);

      assertThat(out1).isEqualTo(3);
      assertThat(out2).isEqualTo(4);
      assertThat(out3).isEqualTo(1);
      assertThat(out4).isEqualTo(2);
      assertThat(out5).isEqualTo(3);
      assertThat(out6).isEqualTo(4);
    }

  }

}
