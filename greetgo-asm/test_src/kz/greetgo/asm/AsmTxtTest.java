package kz.greetgo.asm;

import org.testng.annotations.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmTxtTest {

  @Test
  public void importDescriptor_01() {
    //noinspection resource
    AsmTxt txt = new AsmTxt(null, "test.Test", true);

    //
    //
    final String result = txt.importDescriptor("sinus Lkz/greetgo/wow/HelloClass; cloud Lkz/greetgo/wow/Ground; test");
    //
    //

    txt.importMap().forEach((k, v) -> System.out.println("bMjWm0I1RF :: import " + k + " from " + v));

    assertThat(result).isEqualTo("sinus /HelloClass; cloud /Ground; test");
    assertThat(txt.importMap()).isEqualTo(Map.of(
      "/Ground;", "kz/greetgo/wow/Ground",
      "/HelloClass;", "kz/greetgo/wow/HelloClass"
    ));
  }

  @Test
  public void importDescriptor_02() {
    //noinspection resource
    AsmTxt txt = new AsmTxt(null, "test.Test", true);

    //
    //
    final String result = txt.importDescriptor("sinus Lkz/greetgo/wow/HelloClass; cloud Lkz/greetgo/wow/Ground;");
    //
    //

    txt.importMap().forEach((k, v) -> System.out.println("bMjWm0I1RF :: import " + k + " from " + v));

    assertThat(result).isEqualTo("sinus /HelloClass; cloud /Ground;");
    assertThat(txt.importMap()).isEqualTo(Map.of(
      "/Ground;", "kz/greetgo/wow/Ground",
      "/HelloClass;", "kz/greetgo/wow/HelloClass"
    ));
  }

  @Test
  public void importDescriptor_03() {
    //noinspection resource
    AsmTxt txt = new AsmTxt(null, "test.Test", true);

    //
    //
    final String result = txt.importDescriptor("Lkz/greetgo/wow/HelloClass; cloud Lkz/greetgo/wow/Ground; test");
    //
    //

    txt.importMap().forEach((k, v) -> System.out.println("bMjWm0I1RF :: import " + k + " from " + v));

    assertThat(result).isEqualTo("/HelloClass; cloud /Ground; test");
    assertThat(txt.importMap()).isEqualTo(Map.of(
      "/Ground;", "kz/greetgo/wow/Ground",
      "/HelloClass;", "kz/greetgo/wow/HelloClass"
    ));
  }

  @Test
  public void importDescriptor_04() {
    //noinspection resource
    AsmTxt txt = new AsmTxt(null, "test.Test", true);

    //
    //
    final String result = txt.importDescriptor("Lkz/greetgo/wow/HelloClass;");
    //
    //

    txt.importMap().forEach((k, v) -> System.out.println("bMjWm0I1RF :: import " + k + " from " + v));

    assertThat(result).isEqualTo("/HelloClass;");
    assertThat(txt.importMap()).isEqualTo(Map.of("/HelloClass;", "kz/greetgo/wow/HelloClass"));
  }

  @Test
  public void importDescriptor_05() {
    //noinspection resource
    AsmTxt txt = new AsmTxt(null, "test.Test", true);

    //
    //
    final String result = txt.importDescriptor("/inus Lkz/greetgo/wow$HelloClass; cloud Lkz/greetgo/wow/Ground; test");
    //
    //

    txt.importMap().forEach((k, v) -> System.out.println("bMjWm0I1RF :: import " + k + " from " + v));

    assertThat(result).isEqualTo("/inus /HelloClass; cloud /Ground; test");
    assertThat(txt.importMap()).isEqualTo(Map.of(
      "/Ground;", "kz/greetgo/wow/Ground",
      "/HelloClass;", "kz/greetgo/wow$HelloClass"
    ));
  }

}