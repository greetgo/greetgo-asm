package kz.greetgo.asm;

import lombok.NonNull;
import lombok.experimental.FieldNameConstants;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static kz.greetgo.asm.help.AsmHelper.voidDesc;
import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_LineNumber extends AsmClassTest_Parent {

  @FieldNameConstants
  public static abstract class ParentClass {
    public int a1, b1, c1;
    public int a2, b2, c2;

    public abstract void c_eq_a_plus_b();

    public abstract void c_eq_a_minus_b();

    public abstract void copy_1_to_2();

  }

  Class<?>    generatedClass;
  ParentClass instance;

  @BeforeMethod
  public void generateClass(@NonNull Method testMethod) throws Exception {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(ParentClass.class)
                               .decimalByteCodeNumbers(true)
                               .build()) {

      cl.generateDefaultConstructor();

      create_c_eq_a_plus_b(cl);
      create_c_eq_a_minus_b(cl);
      create__copy_1_to_2(cl);

      cl.finishClass();

      generatedClass = cl.load();

    }

    instance = (ParentClass) generatedClass.getConstructor().newInstance();
  }

  private void create_c_eq_a_minus_b(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("c_eq_a_minus_b", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.label("L1", 54326560)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "a1", int.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "b1", int.class)
         .ISUB()
         .PUTFIELD(cl.classPath, "c1", int.class)
         .space(1);

      asm.label("L2", 32145543)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "a2", int.class)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "b2", int.class)
         .ISUB()
         .PUTFIELD(cl.classPath, "c2", int.class)
         .space(1);

      asm.RETURN();
    }
    asm.finishMethodCode();
  }


  private void create_c_eq_a_plus_b(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("c_eq_a_plus_b", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.label("L1");
      asm.ALOAD(0);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "a1", int.class);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "b1", int.class);
      asm.IADD();
      asm.PUTFIELD(cl.classPath, "c1", int.class);
      asm.space(1);

      asm.label("L2");
      asm.ALOAD(0);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "a2", int.class);
      asm.ALOAD(0);
      asm.GETFIELD(cl.classPath, "b2", int.class);
      asm.IADD();
      asm.PUTFIELD(cl.classPath, "c2", int.class);
      asm.space(1);

      asm.RETURN();
    }
    asm.lineNumber("L1", 1234)
       .lineNumber("L2", 1245)
       .finishMethodCode();
  }

  @Test
  public void c_eq_a_plus_b() {

    instance.a1 = 234;
    instance.b1 = 5435;

    instance.a2 = -700;
    instance.b2 = 700;

    instance.c_eq_a_plus_b();

    assertThat(instance.c1).isEqualTo(instance.a1 + instance.b1);
    assertThat(instance.c2).isEqualTo(instance.a2 + instance.b2);
  }

  @Test
  public void c_eq_a_minus_b() {

    instance.a1 = 23400;
    instance.b1 = 5435;

    instance.a2 = -700;
    instance.b2 = -700;

    instance.c_eq_a_minus_b();

    assertThat(instance.c1).isEqualTo(instance.a1 - instance.b1);
    assertThat(instance.c2).isEqualTo(instance.a2 - instance.b2);
  }


  private void create__copy_1_to_2(@NonNull AsmClass cl) {
    final AsmMethod asm = cl.method("copy_1_to_2", voidDesc()).build();
    asm.startMethodCode();
    {
      asm.space(1).label(asm.newLabelName(), 26317)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "a1", int.class)
         .PUTFIELD(cl.classPath, "a2", int.class);

      asm.space(1).label(asm.newLabelName(), 15181)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "b1", int.class)
         .PUTFIELD(cl.classPath, "b2", int.class);

      asm.space(1).label(asm.newLabelName(), 20068)
         .ALOAD(0)
         .ALOAD(0)
         .GETFIELD(cl.classPath, "c1", int.class)
         .PUTFIELD(cl.classPath, "c2", int.class);

      asm.space(1).label(asm.newLabelName(), 12728)
         .RETURN();
    }
    asm.finishMethodCode();
  }

  @Test
  public void copy_1_to_2() {
    instance.a1 = 93893;
    instance.b1 = 111;
    instance.c1 = 546;

    instance.a2 = 0;
    instance.b2 = 0;
    instance.c2 = 0;

    instance.copy_1_to_2();

    assertThat(instance.a2).isEqualTo(instance.a1);
    assertThat(instance.b2).isEqualTo(instance.b1);
    assertThat(instance.c2).isEqualTo(instance.c1);

  }
}
