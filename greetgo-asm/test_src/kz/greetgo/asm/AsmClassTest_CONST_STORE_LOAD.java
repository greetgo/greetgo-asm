package kz.greetgo.asm;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;

import static org.assertj.core.api.Assertions.assertThat;

public class AsmClassTest_CONST_STORE_LOAD extends AsmClassTest_Parent {

  public static abstract class JavaFields {

    public byte       field_1  = 45;
    public short      field_2  = 67;
    public int        field_3  = 654;
    public long       field_4  = 3456;
    public float      field_5  = 656;
    public double     field_6  = 543;
    public Byte       field_7  = 34;
    public Short      field_8  = 654;
    public Integer    field_9  = 54325;
    public Long       field_10 = 654425L;
    public Float      field_11 = 34.56f;
    public Double     field_12 = 6543.567;
    public BigInteger field_13 = new BigInteger("542364564365");
    public BigDecimal field_14 = new BigDecimal("43256436425413542543.254325432");
    public String     field_15 = "kj1b342khb53h2b54k32b54";

    public abstract void set_m5();

    public abstract void set_m4();

    public abstract void set_m3();

    public abstract void set_m2();

    public abstract void set_m1();

    public abstract void set_0();

    public abstract void set_1();

    public abstract void set_2();

    public abstract void set_3();

    public abstract void set_4();

    public abstract void set_5();

  }

  JavaFields fields;
  Class<?>   generatedClass;

  @BeforeMethod
  @SneakyThrows
  public void generateClass(@NonNull Method testMethod) {

    try (AsmClass cl = AsmClass.builder("test.test_" + nowSorted() + ".Test")
                               .src(root.resolve(testMethod.getName()))
                               .ext(JavaFields.class)
                               .build()) {

      cl.generateDefaultConstructor();


      for (int c = -5; c <= 5; c++) {

        final AsmMethod asm = cl.method(("set_" + c).replace('-', 'm'), "()V").build();
        asm.startMethodCode();
        asm.newVarIndex(Object.class);// place for this.0
        {
          for (int i = 1; i <= 15; i++) {
            final Field field = JavaFields.class.getField("field_" + i);

            int indexVar = asm.newVarIndex(field.getType());

            asm.label("INIT_FIELD_" + indexVar);

            asm.CONST(c, field.getType())
               .STORE(indexVar, field.getType());

            asm.ALOAD(0)
               .LOAD(indexVar, field.getType())
               .PUTFIELD(field);
          }
          asm.label("RETURN")
             .RETURN();
        }
        asm.finishMethodCode();
      }

      cl.finishClass();

      generatedClass = cl.load();

    }

    fields = (JavaFields) generatedClass.getConstructor().newInstance();
  }

  @Test
  public void set_0() {

    fields.set_0();

    assertThat(fields.field_1).isZero();
    assertThat(fields.field_2).isZero();
    assertThat(fields.field_3).isZero();
    assertThat(fields.field_4).isZero();
    assertThat(fields.field_5).isZero();
    assertThat(fields.field_6).isZero();
    assertThat(fields.field_7).isZero();
    assertThat(fields.field_8).isZero();
    assertThat(fields.field_9).isZero();
    assertThat(fields.field_10).isZero();
    assertThat(fields.field_11).isZero();
    assertThat(fields.field_12).isZero();
    assertThat(fields.field_13).isZero();
    assertThat(fields.field_14).isZero();
    assertThat(fields.field_15).isEqualTo("0");

  }

  @Test
  public void set_1() {

    fields.set_1();

    assertThat(fields.field_1).isEqualTo((byte) 1);
    assertThat(fields.field_2).isEqualTo((short) 1);
    assertThat(fields.field_3).isEqualTo(1);
    assertThat(fields.field_4).isEqualTo(1);
    assertThat(fields.field_5).isEqualTo(1);
    assertThat(fields.field_6).isEqualTo(1);
    assertThat(fields.field_7).isEqualTo((byte) 1);
    assertThat(fields.field_8).isEqualTo((short) 1);
    assertThat(fields.field_9).isEqualTo(1);
    assertThat(fields.field_10).isEqualTo(1);
    assertThat(fields.field_11).isEqualTo(1);
    assertThat(fields.field_12).isEqualTo(1);
    assertThat(fields.field_13).isEqualTo(1);
    assertThat(fields.field_14).isEqualTo(BigDecimal.ONE);
    assertThat(fields.field_15).isEqualTo("1");

  }

  @Test
  public void set_2() {

    fields.set_2();

    assertThat(fields.field_1).isEqualTo((byte) 2);
    assertThat(fields.field_2).isEqualTo((short) 2);
    assertThat(fields.field_3).isEqualTo(2);
    assertThat(fields.field_4).isEqualTo(2);
    assertThat(fields.field_5).isEqualTo(2);
    assertThat(fields.field_6).isEqualTo(2);
    assertThat(fields.field_7).isEqualTo((byte) 2);
    assertThat(fields.field_8).isEqualTo((short) 2);
    assertThat(fields.field_9).isEqualTo(2);
    assertThat(fields.field_10).isEqualTo(2);
    assertThat(fields.field_11).isEqualTo(2);
    assertThat(fields.field_12).isEqualTo(2);
    assertThat(fields.field_13).isEqualTo(2);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("2"));
    assertThat(fields.field_15).isEqualTo("2");

  }

  @Test
  public void set_3() {

    fields.set_3();

    assertThat(fields.field_1).isEqualTo((byte) 3);
    assertThat(fields.field_2).isEqualTo((short) 3);
    assertThat(fields.field_3).isEqualTo(3);
    assertThat(fields.field_4).isEqualTo(3);
    assertThat(fields.field_5).isEqualTo(3);
    assertThat(fields.field_6).isEqualTo(3);
    assertThat(fields.field_7).isEqualTo((byte) 3);
    assertThat(fields.field_8).isEqualTo((short) 3);
    assertThat(fields.field_9).isEqualTo(3);
    assertThat(fields.field_10).isEqualTo(3);
    assertThat(fields.field_11).isEqualTo(3);
    assertThat(fields.field_12).isEqualTo(3);
    assertThat(fields.field_13).isEqualTo(3);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("3"));
    assertThat(fields.field_15).isEqualTo("3");

  }

  @Test
  public void set_4() {

    fields.set_4();

    assertThat(fields.field_1).isEqualTo((byte) 4);
    assertThat(fields.field_2).isEqualTo((short) 4);
    assertThat(fields.field_3).isEqualTo(4);
    assertThat(fields.field_4).isEqualTo(4);
    assertThat(fields.field_5).isEqualTo(4);
    assertThat(fields.field_6).isEqualTo(4);
    assertThat(fields.field_7).isEqualTo((byte) 4);
    assertThat(fields.field_8).isEqualTo((short) 4);
    assertThat(fields.field_9).isEqualTo(4);
    assertThat(fields.field_10).isEqualTo(4);
    assertThat(fields.field_11).isEqualTo(4);
    assertThat(fields.field_12).isEqualTo(4);
    assertThat(fields.field_13).isEqualTo(4);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("4"));
    assertThat(fields.field_15).isEqualTo("4");

  }

  @Test
  public void set_5() {

    fields.set_5();

    assertThat(fields.field_1).isEqualTo((byte) 5);
    assertThat(fields.field_2).isEqualTo((short) 5);
    assertThat(fields.field_3).isEqualTo(5);
    assertThat(fields.field_4).isEqualTo(5);
    assertThat(fields.field_5).isEqualTo(5);
    assertThat(fields.field_6).isEqualTo(5);
    assertThat(fields.field_7).isEqualTo((byte) 5);
    assertThat(fields.field_8).isEqualTo((short) 5);
    assertThat(fields.field_9).isEqualTo(5);
    assertThat(fields.field_10).isEqualTo(5);
    assertThat(fields.field_11).isEqualTo(5);
    assertThat(fields.field_12).isEqualTo(5);
    assertThat(fields.field_13).isEqualTo(5);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("5"));
    assertThat(fields.field_15).isEqualTo("5");

  }


  @Test
  public void set_m1() {

    fields.set_m1();

    assertThat(fields.field_1).isEqualTo((byte) -1);
    assertThat(fields.field_2).isEqualTo((short) -1);
    assertThat(fields.field_3).isEqualTo(-1);
    assertThat(fields.field_4).isEqualTo(-1);
    assertThat(fields.field_5).isEqualTo(-1);
    assertThat(fields.field_6).isEqualTo(-1);
    assertThat(fields.field_7).isEqualTo((byte) -1);
    assertThat(fields.field_8).isEqualTo((short) -1);
    assertThat(fields.field_9).isEqualTo(-1);
    assertThat(fields.field_10).isEqualTo(-1);
    assertThat(fields.field_11).isEqualTo(-1);
    assertThat(fields.field_12).isEqualTo(-1);
    assertThat(fields.field_13).isEqualTo(-1);
    assertThat(fields.field_14).isEqualTo(BigDecimal.ONE.negate());
    assertThat(fields.field_15).isEqualTo("-1");

  }

  @Test
  public void set_m2() {

    fields.set_m2();

    assertThat(fields.field_1).isEqualTo((byte) -2);
    assertThat(fields.field_2).isEqualTo((short) -2);
    assertThat(fields.field_3).isEqualTo(-2);
    assertThat(fields.field_4).isEqualTo(-2);
    assertThat(fields.field_5).isEqualTo(-2);
    assertThat(fields.field_6).isEqualTo(-2);
    assertThat(fields.field_7).isEqualTo((byte) -2);
    assertThat(fields.field_8).isEqualTo((short) -2);
    assertThat(fields.field_9).isEqualTo(-2);
    assertThat(fields.field_10).isEqualTo(-2);
    assertThat(fields.field_11).isEqualTo(-2);
    assertThat(fields.field_12).isEqualTo(-2);
    assertThat(fields.field_13).isEqualTo(-2);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("2").negate());
    assertThat(fields.field_15).isEqualTo("-2");

  }

  @Test
  public void set_m3() {

    fields.set_m3();

    assertThat(fields.field_1).isEqualTo((byte) -3);
    assertThat(fields.field_2).isEqualTo((short) -3);
    assertThat(fields.field_3).isEqualTo(-3);
    assertThat(fields.field_4).isEqualTo(-3);
    assertThat(fields.field_5).isEqualTo(-3);
    assertThat(fields.field_6).isEqualTo(-3);
    assertThat(fields.field_7).isEqualTo((byte) -3);
    assertThat(fields.field_8).isEqualTo((short) -3);
    assertThat(fields.field_9).isEqualTo(-3);
    assertThat(fields.field_10).isEqualTo(-3);
    assertThat(fields.field_11).isEqualTo(-3);
    assertThat(fields.field_12).isEqualTo(-3);
    assertThat(fields.field_13).isEqualTo(-3);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("3").negate());
    assertThat(fields.field_15).isEqualTo("-3");

  }

  @Test
  public void set_m4() {

    fields.set_m4();

    assertThat(fields.field_1).isEqualTo((byte) -4);
    assertThat(fields.field_2).isEqualTo((short) -4);
    assertThat(fields.field_3).isEqualTo(-4);
    assertThat(fields.field_4).isEqualTo(-4);
    assertThat(fields.field_5).isEqualTo(-4);
    assertThat(fields.field_6).isEqualTo(-4);
    assertThat(fields.field_7).isEqualTo((byte) -4);
    assertThat(fields.field_8).isEqualTo((short) -4);
    assertThat(fields.field_9).isEqualTo(-4);
    assertThat(fields.field_10).isEqualTo(-4);
    assertThat(fields.field_11).isEqualTo(-4);
    assertThat(fields.field_12).isEqualTo(-4);
    assertThat(fields.field_13).isEqualTo(-4);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("4").negate());
    assertThat(fields.field_15).isEqualTo("-4");

  }

  @Test
  public void set_m5() {

    fields.set_m5();

    assertThat(fields.field_1).isEqualTo((byte) -5);
    assertThat(fields.field_2).isEqualTo((short) -5);
    assertThat(fields.field_3).isEqualTo(-5);
    assertThat(fields.field_4).isEqualTo(-5);
    assertThat(fields.field_5).isEqualTo(-5);
    assertThat(fields.field_6).isEqualTo(-5);
    assertThat(fields.field_7).isEqualTo((byte) -5);
    assertThat(fields.field_8).isEqualTo((short) -5);
    assertThat(fields.field_9).isEqualTo(-5);
    assertThat(fields.field_10).isEqualTo(-5);
    assertThat(fields.field_11).isEqualTo(-5);
    assertThat(fields.field_12).isEqualTo(-5);
    assertThat(fields.field_13).isEqualTo(-5);
    assertThat(fields.field_14).isEqualTo(new BigDecimal("5").negate());
    assertThat(fields.field_15).isEqualTo("-5");

  }

}
