package kz.greetgo.asm;

import kz.greetgo.asm.help.AsmHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNullElse;

public class AsmClass implements AutoCloseable {
  public final @NonNull  String      classPath;
  final @NonNull         ClassWriter cw;
  private final @NonNull AsmTxt      txt;
  private final @NonNull String      extendsClassPath;
  private final          Path        src;

  private boolean classFinished = false;

  public static class Builder {
    private final @NonNull String classPath;

    private Path   src;
    private String extendsClassPath;

    private final List<String> implInterfacePathList = new ArrayList<>();

    private boolean decimalByteCodeNumbers = true;

    private Builder(@NonNull String classNameOrPath) {
      this.classPath = classNameOrPath.replace('.', '/');
    }

    public Builder src(Path src) {
      this.src = src;
      return this;
    }

    public Builder ext(Class<?> extendsClass) {
      return ext(extendsClass == null ? null : Type.getInternalName(extendsClass));
    }

    public Builder decimalByteCodeNumbers(boolean decimalByteCodeNumbers) {
      this.decimalByteCodeNumbers = decimalByteCodeNumbers;
      return this;
    }

    public Builder ext(String classNameOrPath) {
      if (classNameOrPath == null) {
        extendsClassPath = null;
      } else {
        extendsClassPath = classNameOrPath.replace('.', '/');
      }
      return this;
    }

    public Builder impl(Class<?> interfaceClass) {
      return impl(Type.getInternalName(interfaceClass));
    }

    public Builder impl(String interfaceNameOrPath) {
      if (interfaceNameOrPath != null) {
        implInterfacePathList.add(interfaceNameOrPath.replace('.', '/'));
      }
      return this;
    }

    public AsmClass build() {
      return new AsmClass(classPath, extendsClassPath, src, implInterfacePathList, decimalByteCodeNumbers);
    }
  }

  public static @NonNull Builder builder(@NonNull String classNameOrPath) {
    return new Builder(classNameOrPath);
  }

  @SneakyThrows
  private AsmClass(@NonNull String classPath,
                   String extendsClassPath,
                   Path src,
                   @NonNull List<String> implInterfacePathList,
                   boolean decimalByteCodeNumbers) {

    this.classPath = classPath;
    this.src       = src;

    this.cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

    this.extendsClassPath = extendsClassPath != null ? extendsClassPath : Type.getInternalName(Object.class);

    cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC + Opcodes.ACC_SUPER,
             classPath,
             null,
             this.extendsClassPath,
             implInterfacePathList.toArray(new String[0]));

    txt = new AsmTxt(src, classPath, decimalByteCodeNumbers);
    txt.topOfClass(this.extendsClassPath, implInterfacePathList);
  }

  @RequiredArgsConstructor
  public class AsmMethodBuilder {
    private final @NonNull String     methodName;
    private final @NonNull String     methodDesc;
    private                AccessType accessType = AccessType.PUBLIC;
    private                boolean    isStatic   = false;

    public AsmMethodBuilder access(AccessType accessType) {
      this.accessType = requireNonNullElse(accessType, this.accessType);
      return this;
    }

    public AsmMethodBuilder static_(boolean isStatic) {
      this.isStatic = isStatic;
      return this;
    }

    public AsmMethod build() {
      return new AsmMethod(AsmClass.this, txt, new AsmMethodDefinition(methodName, methodDesc, accessType, isStatic));
    }
  }

  public AsmMethodBuilder method(@NonNull String methodName, @NonNull String methodDesc) {
    return new AsmMethodBuilder(methodName, methodDesc);
  }

  public void finishClass() {
    if (classFinished) {
      throw new RuntimeException("KiJWPaeLiD :: Class already finished");
    }

    cw.visitEnd();
    txt.finishClass();
    classFinished = true;
  }

  public byte[] toByteArray() {
    checkFinishClass();
    return cw.toByteArray();
  }

  private void checkFinishClass() {
    if (!classFinished) {
      throw new RuntimeException("I6PgKIzAR3 :: Class not yet finished.\n\t\tPlease call `finishClass()` first, and then call this method");
    }
  }

  @Override
  @SneakyThrows
  public void close() {
    txt.close();

    if (src != null) {
      final Path mainClassFile = src.resolve(classPath + ".class");
      mainClassFile.toFile().getParentFile().mkdirs();
      Files.write(mainClassFile, toByteArray());
    }

  }

  public void generateDefaultConstructor() {
    final AsmMethod asm = method("<init>", "()V").build();
    asm.startMethodCode();
    asm.ALOAD(0);
    asm.INVOKESPECIAL(extendsClassPath, "<init>", "()V", false);
    asm.RETURN();
    asm.finishMethodCode();
  }

  public AsmClass generateField(@NonNull String fieldName, @NonNull Class<?> fieldType, @NonNull AccessType accessType, boolean isStatic) {

    int opCodes = AsmHelper.takeOpcodes(accessType, isStatic);

    cw.visitField(opCodes, fieldName, Type.getDescriptor(fieldType), null, null);
    txt.field(fieldName, fieldType, accessType, isStatic);

    return this;
  }

  @SneakyThrows
  public Class<?> load() {
    checkFinishClass();
    final String className  = classPath.replace('/', '.');
    final byte[] classBytes = toByteArray();

    final ClassLoader classLoader = new ClassLoader() {
      @Override
      protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (className.equals(name)) {
          return defineClass(name, classBytes, 0, classBytes.length);
        }
        throw new ClassNotFoundException(name);
      }
    };

    return classLoader.loadClass(className);
  }

  public void spaceLine() {
    txt.spaceLine();
  }
}
