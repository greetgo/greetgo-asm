package kz.greetgo.asm.printers.parts;

import kz.greetgo.asm.AccessType;
import kz.greetgo.asm.help.AsmHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.PrintStream;

@RequiredArgsConstructor
public class PrintMethod  extends PrintAbstract  {

  private final @NonNull AccessType accessType;
  private final          boolean    isStatic;
  private final @NonNull String     methodName;
  private final @NonNull String     methodDesc;

  private static final int TAB = 34;

  @Override
  public void print(@NonNull PrintStream out) {
    StringBuilder sb = new StringBuilder();
    sb.append("method ").append(methodName).append(' ').append(methodDesc);

    AsmHelper.tab(sb, TAB, true);

    sb.append('(').append(accessType.name().toLowerCase()).append(isStatic ? " static" : "").append(')');
    out.println(sb);
  }
}
