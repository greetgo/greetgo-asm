package kz.greetgo.asm.printers.parts;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public abstract class PrintAbstract implements Print {

  @Override
  public String toString() {
    ByteArrayOutputStream bs          = new ByteArrayOutputStream();
    final PrintStream     printStream = new PrintStream(bs, false, StandardCharsets.UTF_8);
    print(printStream);
    printStream.flush();
    return bs.toString(StandardCharsets.UTF_8);
  }
}
