package kz.greetgo.asm.printers.parts;

import kz.greetgo.asm.help.AsmByteHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.objectweb.asm.MethodVisitor;

import java.io.PrintStream;
import java.util.Arrays;

import static java.util.stream.Collectors.joining;

@RequiredArgsConstructor
public class PrintMne implements Print {
  private final String        mne;
  private final String[]      args;
  private final MethodVisitor mv;
  private final int           offset;
  private final int           count;
  private final boolean       decimalByteCodeNumbers;

  @Override
  public String toString() {
    return mne + Arrays.stream(args).map(x -> " " + x).collect(joining());
  }

  @Override
  public void print(@NonNull PrintStream out) {
    if (mv != null) {

      final int leftTab   = 6;
      final int argTab    = 16;
      final int bytesSize = 28;
      final int mneSize   = 50;

      if (args.length == 0) {
        out.println(formatMne(mne, leftTab, bytesSize, 0));
      } else {
        out.println(formatMne(mne, leftTab, bytesSize, mneSize) + formatMneArgs(args, argTab));
      }

    } else {

      final int leftTab = 12;
      final int argTab  = 16;
      final int mneSize = 30;

      if (args.length == 0) {
        out.println(formatMne(mne, leftTab, 0, 0));
      } else {
        out.println(formatMne(mne, leftTab, 0, mneSize) + formatMneArgs(args, argTab));
      }
    }

  }


  @SuppressWarnings("SameParameterValue")
  private @NonNull String formatMneArgs(String[] args, int tabOnArg) {

    StringBuilder sb = new StringBuilder();

    for (final String arg : args) {
      if (arg == null) {
        continue;
      }

      final int mod = sb.length() % tabOnArg;
      if (mod > 0) {
        sb.append(" ".repeat(tabOnArg - mod));
      }

      if (sb.length() > 0 && sb.charAt(sb.length() - 1) != ' ') {
        sb.append(' ');
      }

      sb.append(arg);
    }

    return sb.toString();
  }


  @SuppressWarnings("SameParameterValue")
  private @NonNull String formatMne(String mne, int leftTab, int bytesSize, int allLength) {
    StringBuilder sb = new StringBuilder();
    sb.append(" ".repeat(leftTab));

    if (bytesSize > 0) {
      int startedLength = sb.length();
      sb.append(decimalByteCodeNumbers ? AsmByteHelper.intToDec(offset, 5) : AsmByteHelper.intToHex(offset, 4));
      sb.append(": ");
      sb.append(AsmByteHelper.bytesToHex(AsmByteHelper.extract(mv, offset, count)));
      int bytesLen = sb.length() - startedLength;
      if (bytesLen < bytesSize) {
        sb.append(" ".repeat(bytesSize - bytesLen));
      }
    }

    sb.append(mne);
    if (sb.length() < allLength) {
      sb.append(" ".repeat(allLength - sb.length()));
    }
    if (allLength > 0 && sb.charAt(sb.length() - 1) != ' ') {
      sb.append(' ');
    }
    return sb.toString();
  }

}
