package kz.greetgo.asm.printers.parts;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.PrintStream;

@RequiredArgsConstructor
public class PrintTryCatch  extends PrintAbstract  {
  private final @NonNull String labelTry;
  private final @NonNull String labelFinally;
  private final @NonNull String labelCatch;
  private final          String errorClassPath;

  @Override
  public void print(@NonNull PrintStream out) {
    out.println("TRY (from `" + labelTry + "` to `" + labelFinally + "`) CATCH (`" + labelCatch + "`) over " + errorClassPath);
  }
}
