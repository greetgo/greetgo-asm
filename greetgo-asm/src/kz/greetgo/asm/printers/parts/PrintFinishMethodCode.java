package kz.greetgo.asm.printers.parts;

import lombok.NonNull;

import java.io.PrintStream;

public class PrintFinishMethodCode  extends PrintAbstract {
  @Override
  public void print(@NonNull PrintStream out) {
    out.println("finish code");
  }
}
