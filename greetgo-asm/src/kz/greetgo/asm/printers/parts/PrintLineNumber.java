package kz.greetgo.asm.printers.parts;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.PrintStream;

@RequiredArgsConstructor
public class PrintLineNumber  extends PrintAbstract  {
  private final short    lineNumber;
  private final String labelName;

  @Override
  public void print(@NonNull PrintStream out) {
    out.println("line " + labelName + " : " + lineNumber);
  }
}
