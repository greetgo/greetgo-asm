package kz.greetgo.asm.printers.parts;

import kz.greetgo.asm.help.AsmHelper;
import lombok.NonNull;

import java.io.PrintStream;

public class PrintLabel  extends PrintAbstract  {
  private final @NonNull String  labelName;
  private final          short   lineNumber;
  private final          boolean showLineNumber;

  public PrintLabel(@NonNull String labelName, short lineNumber) {
    this.labelName      = labelName;
    this.lineNumber     = lineNumber;
    this.showLineNumber = true;
  }

  public PrintLabel(@NonNull String labelName) {
    this.labelName      = labelName;
    this.lineNumber     = -1;
    this.showLineNumber = false;
  }

  @Override
  public void print(@NonNull PrintStream out) {
    if (showLineNumber) {
      StringBuilder sb = new StringBuilder();
      sb.append("   ").append(labelName).append(":");
      AsmHelper.tab(sb, 34 - 8, true);
      sb.append(lineNumber);
      out.println(sb);
    } else {
      out.println("   " + labelName + ":");
    }
  }
}
