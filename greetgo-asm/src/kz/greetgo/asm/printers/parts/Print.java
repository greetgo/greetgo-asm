package kz.greetgo.asm.printers.parts;

import lombok.NonNull;

import java.io.PrintStream;

public interface Print {
  void print(@NonNull PrintStream out);
}
