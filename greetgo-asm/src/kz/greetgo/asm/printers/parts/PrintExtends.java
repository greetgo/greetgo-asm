package kz.greetgo.asm.printers.parts;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.PrintStream;
import java.util.List;

@RequiredArgsConstructor
public class PrintExtends extends PrintAbstract {
  private final String       extendsClassPath;
  private final List<String> implInterfacePathList;

  @Override
  public void print(@NonNull PrintStream out) {
    out.println("extends " + extendsClassPath + (
      implInterfacePathList.isEmpty() ? "" : (" implements " + String.join(" ", implInterfacePathList))
    ));
    out.println();
  }
}
