package kz.greetgo.asm.printers.parts;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.PrintStream;

@RequiredArgsConstructor
public class PrintRem  extends PrintAbstract  {
  private final @NonNull String commentLine;

  @Override
  public void print(@NonNull PrintStream out) {
    out.println(" ".repeat(34) + "; " + commentLine);
  }
}
