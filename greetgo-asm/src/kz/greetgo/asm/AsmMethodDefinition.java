package kz.greetgo.asm;

import kz.greetgo.asm.help.AsmHelper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

@RequiredArgsConstructor
public class AsmMethodDefinition {
  public final @NonNull String     methodName;
  public final @NonNull String     methodDesc;
  public final @NonNull AccessType accessType;
  public final          boolean    isStatic;

  public void showMethod(@NonNull AsmTxt txt) {
    txt.method(methodName, methodDesc, accessType, isStatic);
  }

  public MethodVisitor visitMethodOver(@NonNull ClassWriter cw) {
    final int opCodes = AsmHelper.takeOpcodes(accessType, isStatic);
    return cw.visitMethod(opCodes, methodName, methodDesc, null, null);
  }
}
