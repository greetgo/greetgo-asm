package kz.greetgo.asm;

import kz.greetgo.asm.printers.parts.Print;
import kz.greetgo.asm.printers.parts.PrintExtends;
import kz.greetgo.asm.printers.parts.PrintField;
import kz.greetgo.asm.printers.parts.PrintFinishClass;
import kz.greetgo.asm.printers.parts.PrintFinishMethodCode;
import kz.greetgo.asm.printers.parts.PrintLabel;
import kz.greetgo.asm.printers.parts.PrintLineNumber;
import kz.greetgo.asm.printers.parts.PrintMethod;
import kz.greetgo.asm.printers.parts.PrintMne;
import kz.greetgo.asm.printers.parts.PrintRem;
import kz.greetgo.asm.printers.parts.PrintSpaceLine;
import kz.greetgo.asm.printers.parts.PrintStartMethodCode;
import kz.greetgo.asm.printers.parts.PrintTryCatch;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.objectweb.asm.MethodVisitor;

import java.io.File;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AsmTxt implements AutoCloseable {
  private final Path    src;
  private final String  classPath;
  private final boolean decimalByteCodeNumbers;

  private MethodVisitor mv;
  private int           offset;
  private int           count;

  private final List<Print>         printListBeforeImports = new ArrayList<>();
  private final Map<String, String> importMap              = new HashMap<>();
  private final List<Print>         printList              = new ArrayList<>();


  @SneakyThrows
  public AsmTxt(Path src, @NonNull String classPath, boolean decimalByteCodeNumbers) {
    this.src                    = src;
    this.classPath              = classPath;
    this.decimalByteCodeNumbers = decimalByteCodeNumbers;
  }

  private static @NonNull String spacing(String str, int length) {
    //@formatter:off
    if (str == null          ) return " ".repeat(length);
    if (str.length() < length) return str + " ".repeat(length - str.length());
                               return str;
    //@formatter:on
  }

  @Override
  @SneakyThrows
  public void close() {
    if (src != null) {
      {
        File outFile = src.resolve(classPath + ".asm.txt").toFile();
        outFile.getParentFile().mkdirs();
        try (PrintStream out = new PrintStream(outFile, StandardCharsets.UTF_8)) {
          printListBeforeImports.forEach(p -> p.print(out));
          final int maxLen = importMap.keySet().stream().mapToInt(String::length).max().orElse(0);
          importMap.entrySet().stream().sorted(Map.Entry.comparingByKey())
                   .forEachOrdered(e -> out.println("import as " + spacing(e.getKey(), maxLen) + " : " + e.getValue()));
          printList.forEach(p -> p.print(out));
        }
      }
    }
  }

  public Map<String, String> importMap() {
    return Map.copyOf(importMap);
  }

  public void topOfClass(@NonNull String extendsClassPath, @NonNull List<String> implInterfacePathList) {
    printListBeforeImports.add(new PrintExtends(extendsClassPath, implInterfacePathList));
  }

  public void mne(@NonNull String mne, String... args) {
    printList.add(new PrintMne(mne, args, mv, offset, count, decimalByteCodeNumbers));
  }

  public String importType(String internalName) {
    if (internalName == null) {
      return null;
    }

    final int idx1 = internalName.lastIndexOf('/');
    final int idx2 = internalName.lastIndexOf('$');
    if (idx1 < 0 && idx2 < 0) {
      return internalName;
    }
    final int idx = Math.max(idx1, idx2);

    final String prefix = internalName.substring(idx + 1);

    for (int i = 1; ; i++) {
      String shortName = "/" + prefix + (i == 1 ? "" : "" + i) + ";";

      final String savedInternalName = importMap.get(shortName);
      if (internalName.equals(savedInternalName)) {
        return shortName;
      }
      if (savedInternalName == null) {
        importMap.put(shortName, internalName);
        return shortName;
      }
    }
  }

  private static final Pattern PRN_NAME = Pattern.compile("L(\\w+([/$]\\w+)+);");

  public String importDescriptor(String descriptor) {
    if (descriptor == null) {
      return null;
    }

    StringBuilder sb      = new StringBuilder();
    final Matcher matcher = PRN_NAME.matcher(descriptor);
    int           prevEnd = 0;

    while (matcher.find()) {
      sb.append(descriptor, prevEnd, matcher.start());
      final String internalClassName = matcher.group(1);
      final String shortName         = importType(internalClassName);
      sb.append(shortName);
      prevEnd = matcher.end();
    }

    sb.append(descriptor.substring(prevEnd));
    return sb.toString();
  }

  public void txtInvoke(String invokeName, String ownerClassPath, String methodName, String methodDesc, boolean isOwnerInterface) {
    mne(invokeName, methodName + (isOwnerInterface ? ":(interface " : ":(class ") + importType(ownerClassPath) + ")", importDescriptor(methodDesc));
  }

  public void fieldAccess(String mne, String ownerClassPath, String fieldName, String fieldTypeDescriptor) {
    mne(mne, fieldName + ":" + importDescriptor(fieldTypeDescriptor), "of(" + importType(ownerClassPath) + ")");
  }

  public void label(@NonNull String labelName) {
    if (labelName.startsWith("__")) {
      throw new RuntimeException("P5xXk1jv1V :: Label cannot be started with double underscore");
    }
    printList.add(new PrintLabel(labelName));
  }

  public void label(@NonNull String labelName, short lineNumber) {
    printList.add(new PrintLabel(labelName, lineNumber));
  }

  public void lineNumber(short lineNumber, String labelName) {
    printList.add(new PrintLineNumber(lineNumber, labelName));
  }

  public void field(@NonNull String fieldName, @NonNull Class<?> fieldType, AccessType accessType, boolean isStatic) {
    printList.add(new PrintField(fieldName, fieldType, accessType, isStatic));
  }

  public void putExtractedBytes(MethodVisitor mv, int offset, int count) {
    this.mv     = mv;
    this.offset = offset;
    this.count  = count;
  }

  public void rem(String commentLine) {
    printList.add(new PrintRem(commentLine));
  }

  public void startMethodCode() {
    printList.add(new PrintStartMethodCode());
  }

  public void finishMethodCode() {
    printList.add(new PrintFinishMethodCode());
  }

  public void spaceLine() {
    printList.add(new PrintSpaceLine());
  }

  public void method(String methodName, String methodDesc, AccessType accessType, boolean isStatic) {
    printList.add(new PrintSpaceLine());
    printList.add(new PrintMethod(accessType, isStatic, methodName, importDescriptor(methodDesc)));
  }

  public void finishClass() {
    printList.add(new PrintSpaceLine());
    printList.add(new PrintFinishClass());
  }

  public void tryCatch(String labelTry, String labelFinally, String labelCatch, String errorClassPath) {
    printList.add(new PrintTryCatch(labelTry, labelFinally, labelCatch, importType(errorClassPath)));
  }

}
