package kz.greetgo.asm;

public enum AccessType {
  PUBLIC,
  PROTECTED,
  PRIVATE,
  PACKAGE,
}
