package kz.greetgo.asm;

import kz.greetgo.asm.help.AsmByteHelper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.LocalVariablesSorter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static kz.greetgo.asm.help.AsmHelper.extractFieldClass;

@SuppressWarnings("SpellCheckingInspection")
public class AsmMethod {
  private final MethodVisitor        mv;
  private final AsmTxt               txt;
  private final LocalVariablesSorter localVariablesSorter;

  private boolean byteExtractingOff = true;
  private int     byteExtractingStartIndex;

  private final Map<String, Label> labels = new HashMap<>();

  private final Map<String, Integer> newLabelPrefixes = new HashMap<>();

  public AsmMethod(@NonNull AsmClass asmClass, @NonNull AsmTxt txt, @NonNull AsmMethodDefinition definition) {
    this.mv              = definition.visitMethodOver(asmClass.cw);
    this.txt             = txt;
    localVariablesSorter = new LocalVariablesSorter(Opcodes.ACC_PUBLIC, definition.methodDesc, mv);

    definition.showMethod(txt);
  }

  private @NonNull Label getLabel(@NonNull String labelName) {
    {
      final Label label = labels.get(labelName);
      if (label != null) {
        return label;
      }
    }
    {
      final Label label = new Label();
      labels.put(labelName, label);
      return label;
    }
  }

  public int newVarIndex(@NonNull Class<?> varClass) {
    return localVariablesSorter.newLocal(Type.getType(varClass));
  }

  public AsmMethod startMethodCode() {
    byteExtractingOff = !AsmByteHelper.canByteExtracting(mv);
    mv.visitCode();
    txt.startMethodCode();
    return this;
  }

  private void visitStart() {
    if (byteExtractingOff) {
      return;
    }
    byteExtractingStartIndex = AsmByteHelper.length(mv);
  }

  private void visitFinish() {
    if (byteExtractingOff) {
      txt.putExtractedBytes(null, 0, 0);
      return;
    }
    int count = AsmByteHelper.length(mv) - byteExtractingStartIndex;
    txt.putExtractedBytes(mv, byteExtractingStartIndex, count);
  }

  public AsmMethod ALOAD(int varIndex) {
    return ALOAD(varIndex, null);
  }

  public AsmMethod ALOAD(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.ALOAD, varIndex);
    visitFinish();

    String commentArg = comment == null ? null : "; " + comment;

    switch (varIndex) {
      case 0 -> txt.mne("ALOAD_0", commentArg);
      case 1 -> txt.mne("ALOAD_1", commentArg);
      case 2 -> txt.mne("ALOAD_2", commentArg);
      case 3 -> txt.mne("ALOAD_3", commentArg);
      default -> txt.mne("ALOAD", "" + varIndex, commentArg);
    }

    return this;
  }


  public AsmMethod ISTORE(int varIndex) {
    return ISTORE(varIndex, null);
  }

  public AsmMethod ISTORE(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.ISTORE, varIndex);
    visitFinish();

    String commentArg = comment == null ? null : "; " + comment;

    switch (varIndex) {
      case 0 -> txt.mne("ISTORE_0", commentArg);
      case 1 -> txt.mne("ISTORE_1", commentArg);
      case 2 -> txt.mne("ISTORE_2", commentArg);
      case 3 -> txt.mne("ISTORE_3", commentArg);
      default -> txt.mne("ISTORE", "" + varIndex, commentArg);
    }

    return this;
  }

  public AsmMethod INVOKE(@NonNull Constructor<?> constructor) {
    final String   descriptor        = Type.getConstructorDescriptor(constructor);
    final Class<?> ownerClass        = constructor.getDeclaringClass();
    final String   ownerInternalName = Type.getInternalName(ownerClass);
    final String   methodName        = "<init>";

    visitStart();
    mv.visitMethodInsn(Opcodes.INVOKESPECIAL, ownerInternalName, methodName, descriptor, false);
    visitFinish();
    txt.txtInvoke("INVOKESPECIAL", ownerInternalName, methodName, descriptor, false);
    return this;
  }

  public AsmMethod INVOKE(@NonNull Method method) {
    final Class<?> ownerClass  = method.getDeclaringClass();
    final String   declName    = Type.getInternalName(ownerClass);
    final String   methodName  = method.getName();
    final String   descriptor  = Type.getMethodDescriptor(method);
    final boolean  isStatic    = Modifier.isStatic(method.getModifiers());
    final boolean  isInterface = ownerClass.isInterface();

    //@formatter:off
    final int    opCode = isStatic ? Opcodes.INVOKESTATIC  : isInterface ? Opcodes.INVOKEINTERFACE  : Opcodes.INVOKEVIRTUAL;
    final String mne    = isStatic ?        "INVOKESTATIC" : isInterface ?        "INVOKEINTERFACE" :        "INVOKEVIRTUAL";
    //@formatter:on

    visitStart();
    mv.visitMethodInsn(opCode, declName, methodName, descriptor, isInterface);
    visitFinish();
    txt.txtInvoke(mne, declName, methodName, descriptor, isInterface);
    return this;
  }

  public AsmMethod INVOKEINTERFACE(Class<?> ownerClass, String methodName, String methodDesc, boolean isOwnerInterface) {
    return INVOKEINTERFACE(Type.getInternalName(ownerClass), methodName, methodDesc, isOwnerInterface);
  }

  public AsmMethod INVOKEINTERFACE(Class<?> ownerClass, String methodName, String methodDesc) {
    return INVOKEINTERFACE(Type.getInternalName(ownerClass), methodName, methodDesc, true);
  }

  public AsmMethod INVOKEINTERFACE(@NonNull String ownerClassPath, @NonNull String methodName, @NonNull String methodDesc, boolean isOwnerInterface) {
    visitStart();
    mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, ownerClassPath, methodName, methodDesc, isOwnerInterface);
    visitFinish();
    txt.txtInvoke("INVOKEINTERFACE", ownerClassPath, methodName, methodDesc, isOwnerInterface);
    return this;
  }

  public AsmMethod INVOKEVIRTUAL(@NonNull Class<?> ownerClass, @NonNull String methodName, @NonNull String methodDesc, boolean isOwnerInterface) {
    return INVOKEVIRTUAL(Type.getInternalName(ownerClass), methodName, methodDesc, isOwnerInterface);
  }

  public AsmMethod INVOKEVIRTUAL(@NonNull Class<?> ownerClass, @NonNull String methodName, @NonNull String methodDesc) {
    return INVOKEVIRTUAL(Type.getInternalName(ownerClass), methodName, methodDesc, false);
  }

  public AsmMethod INVOKEVIRTUAL(@NonNull String ownerClassPath, @NonNull String methodName, @NonNull String methodDesc) {
    return INVOKEVIRTUAL(ownerClassPath, methodName, methodDesc, false);
  }

  public AsmMethod INVOKEVIRTUAL(@NonNull String ownerClassPath, @NonNull String methodName, @NonNull String methodDesc, boolean isOwnerInterface) {
    visitStart();
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, ownerClassPath, methodName, methodDesc, isOwnerInterface);
    visitFinish();
    txt.txtInvoke("INVOKEVIRTUAL", ownerClassPath, methodName, methodDesc, isOwnerInterface);
    return this;
  }

  public AsmMethod INVOKESPECIAL(@NonNull Class<?> ownerClass, @NonNull String methodName, @NonNull String methodDesc, boolean isOwnerInterface) {
    return INVOKESPECIAL(Type.getInternalName(ownerClass), methodName, methodDesc, isOwnerInterface);
  }

  public AsmMethod INVOKESPECIAL(@NonNull Class<?> ownerClass, @NonNull String methodName, @NonNull String methodDesc) {
    return INVOKESPECIAL(Type.getInternalName(ownerClass), methodName, methodDesc, false);
  }

  public AsmMethod INVOKESPECIAL(@NonNull String ownerClassPath, @NonNull String methodName, @NonNull String methodDesc, boolean isOwnerInterface) {
    visitStart();
    mv.visitMethodInsn(Opcodes.INVOKESPECIAL, ownerClassPath, methodName, methodDesc, isOwnerInterface);
    visitFinish();
    txt.txtInvoke("INVOKESPECIAL", ownerClassPath, methodName, methodDesc, isOwnerInterface);
    return this;
  }

  public AsmMethod INVOKESTATIC(@NonNull Class<?> ownerClass, @NonNull String methodName, @NonNull String methodDesc) {
    return INVOKESTATIC(Type.getInternalName(ownerClass), methodName, methodDesc, false);
  }

  public AsmMethod INVOKESTATICi(@NonNull Class<?> ownerClass, @NonNull String methodName, @NonNull String methodDesc) {
    return INVOKESTATIC(Type.getInternalName(ownerClass), methodName, methodDesc, true);
  }

  public AsmMethod INVOKESTATIC(@NonNull Class<?> ownerClass, @NonNull String methodName, @NonNull String methodDesc, boolean isOwnerInterface) {
    return INVOKESTATIC(Type.getInternalName(ownerClass), methodName, methodDesc, isOwnerInterface);
  }

  public AsmMethod INVOKESTATIC(@NonNull String ownerClassPath, @NonNull String methodName, @NonNull String methodDesc, boolean isOwnerInterface) {
    visitStart();
    mv.visitMethodInsn(Opcodes.INVOKESTATIC, ownerClassPath, methodName, methodDesc, isOwnerInterface);
    visitFinish();
    txt.txtInvoke("INVOKESTATIC", ownerClassPath, methodName, methodDesc, isOwnerInterface);
    return this;
  }

  public AsmMethod RETURN() {
    visitStart();
    mv.visitInsn(Opcodes.RETURN);
    visitFinish();
    txt.mne("RETURN");
    return this;
  }

  public AsmMethod ARETURN() {
    visitStart();
    mv.visitInsn(Opcodes.ARETURN);
    visitFinish();
    txt.mne("ARETURN");
    return this;
  }

  public void finishMethodCode() {
    mv.visitMaxs(0, 0);
    mv.visitEnd();
    txt.finishMethodCode();
  }

  public AsmMethod GOTO(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.GOTO, getLabel(labelName));
    visitFinish();
    txt.mne("GOTO", labelName);
    return this;
  }

  public AsmMethod IFGE(@NonNull String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFGE, getLabel(labelName));
    visitFinish();
    txt.mne("IFGE", labelName);
    return this;
  }

  public AsmMethod IFLE(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFLE, getLabel(labelName));
    visitFinish();
    txt.mne("IFLE", labelName);
    return this;
  }


  public AsmMethod IFLT(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFLT, getLabel(labelName));
    visitFinish();
    txt.mne("IFLT", labelName);
    return this;
  }

  public AsmMethod NEW(Class<?> aClass) {
    return NEW(Type.getInternalName(aClass));
  }

  public AsmMethod NEW(String classInternalName) {
    visitStart();
    mv.visitTypeInsn(Opcodes.NEW, classInternalName);
    visitFinish();
    txt.mne("NEW", txt.importType(classInternalName));
    return this;
  }

  public AsmMethod DUP() {
    visitStart();
    mv.visitInsn(Opcodes.DUP);
    visitFinish();
    txt.mne("DUP");
    return this;
  }

  public AsmMethod ATHROW() {
    visitStart();
    mv.visitInsn(Opcodes.ATHROW);
    visitFinish();
    txt.mne("ATHROW");
    return this;
  }


  public AsmMethod CHECKCAST(@NonNull Class<?> aClass) {
    if (aClass.isPrimitive()) {
      return NOP();
    }
    return CHECKCAST(Type.getInternalName(aClass));
  }

  public AsmMethod CHECKCAST(String internalClassName) {
    visitStart();
    mv.visitTypeInsn(Opcodes.CHECKCAST, internalClassName);
    visitFinish();
    txt.mne("CHECKCAST", txt.importType(internalClassName));
    return this;
  }

  public AsmMethod LSTORE(int varIndex) {
    return LSTORE(varIndex, null);
  }

  public AsmMethod LSTORE(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.LSTORE, varIndex);
    visitFinish();

    String commentArg = comment == null ? null : "; " + comment;

    switch (varIndex) {
      case 0 -> txt.mne("LSTORE_0", commentArg);
      case 1 -> txt.mne("LSTORE_1", commentArg);
      case 2 -> txt.mne("LSTORE_2", commentArg);
      case 3 -> txt.mne("LSTORE_3", commentArg);
      default -> txt.mne("LSTORE", "" + varIndex, commentArg);
    }
    return this;
  }

  public AsmMethod ASTORE(int varIndex) {
    return ASTORE(varIndex, null);
  }

  public AsmMethod ASTORE(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.ASTORE, varIndex);
    visitFinish();

    String commentArg = comment == null ? null : "; " + comment;

    switch (varIndex) {
      case 0 -> txt.mne("ASTORE_0", commentArg);
      case 1 -> txt.mne("ASTORE_1", commentArg);
      case 2 -> txt.mne("ASTORE_2", commentArg);
      case 3 -> txt.mne("ASTORE_3", commentArg);
      default -> txt.mne("ASTORE", "" + varIndex, commentArg);
    }
    return this;
  }

  public AsmMethod LLOAD(int varIndex) {
    return LLOAD(varIndex, null);
  }

  public AsmMethod LLOAD(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.LLOAD, varIndex);
    visitFinish();
    txt.mne("LLOAD", "" + varIndex, comment == null ? null : "; " + comment);
    return this;
  }

  public AsmMethod LOAD(int varIndex, @NonNull Class<?> varClass) {
    return LOAD(varIndex, varClass, null);
  }

  public AsmMethod LOAD(int varIndex, @NonNull Class<?> varClass, String comment) {
    //@formatter:off
    if (varClass == double.class) return DLOAD(varIndex, comment);
    if (varClass == float .class) return FLOAD(varIndex, comment);
    if (varClass == long  .class) return LLOAD(varIndex, comment);
    if (varClass.isPrimitive()  ) return ILOAD(varIndex, comment);
    //@formatter:on
    return ALOAD(varIndex, comment);
  }

  public AsmMethod STORE(int varIndex, @NonNull Class<?> varClass) {
    return STORE(varIndex, varClass, null);
  }

  public AsmMethod STORE(int varIndex, @NonNull Class<?> varClass, String comment) {
    //@formatter:off
    if (varClass == double.class) return DSTORE(varIndex, comment);
    if (varClass == float .class) return FSTORE(varIndex, comment);
    if (varClass == long  .class) return LSTORE(varIndex, comment);
    if (varClass.isPrimitive()  ) return ISTORE(varIndex, comment);
    //@formatter:on
    return ASTORE(varIndex, comment);
  }

  public AsmMethod PUTFIELD(@NonNull String ownerClassPath, @NonNull String fieldName, @NonNull Class<?> fieldType) {
    return PUTFIELD(ownerClassPath, fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod PUTFIELD(@NonNull Class<?> ownerClass, @NonNull String fieldName, Class<?> fieldType) {
    return fieldType == null
      ? PUTFIELD(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)))
      : PUTFIELD(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod PUTFIELD(@NonNull Class<?> ownerClass, @NonNull String fieldName) {
    return PUTFIELD(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)));
  }

  public AsmMethod PUTFIELD(@NonNull Field field) {
    return PUTFIELD(Type.getInternalName(field.getDeclaringClass()), field.getName(), Type.getDescriptor(field.getType()));
  }

  public AsmMethod PUTFIELD(@NonNull Class<?> ownerClass, @NonNull String fieldName, @NonNull String fieldTypeDescriptor) {
    return PUTFIELD(Type.getInternalName(ownerClass), fieldName, fieldTypeDescriptor);
  }

  public AsmMethod PUTFIELD(String ownerClassPath, String fieldName, String fieldTypeDescriptor) {
    visitStart();
    mv.visitFieldInsn(Opcodes.PUTFIELD, ownerClassPath, fieldName, fieldTypeDescriptor);
    visitFinish();
    txt.fieldAccess("PUTFIELD", ownerClassPath, fieldName, fieldTypeDescriptor);
    return this;
  }

  public AsmMethod PUTSTATIC(@NonNull Field field) {
    if (!Modifier.isStatic(field.getModifiers())) {
      throw new RuntimeException("waYzTrs6jJ :: Field must be static: " + field);
    }
    return PUTSTATIC(field.getDeclaringClass(), field.getName(), Type.getDescriptor(field.getType()));
  }

  public AsmMethod PUTSTATIC(@NonNull String ownerClassPath, @NonNull String fieldName, @NonNull Class<?> fieldType) {
    return PUTSTATIC(ownerClassPath, fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod PUTSTATIC(@NonNull Class<?> ownerClass, @NonNull String fieldName, Class<?> fieldType) {
    return fieldType == null
      ? PUTSTATIC(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)))
      : PUTSTATIC(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod PUTSTATIC(@NonNull Class<?> ownerClass, @NonNull String fieldName) {
    return PUTSTATIC(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)));
  }

  public AsmMethod PUTSTATIC(@NonNull Class<?> ownerClass, @NonNull String fieldName, @NonNull String fieldTypeDescriptor) {
    return PUTSTATIC(Type.getInternalName(ownerClass), fieldName, fieldTypeDescriptor);
  }

  public AsmMethod PUTSTATIC(String ownerClassPath, String fieldName, String fieldTypeDescriptor) {
    visitStart();
    mv.visitFieldInsn(Opcodes.PUTSTATIC, ownerClassPath, fieldName, fieldTypeDescriptor);
    visitFinish();
    txt.fieldAccess("PUTSTATIC", ownerClassPath, fieldName, fieldTypeDescriptor);
    return this;
  }

  public AsmMethod GETFIELD(@NonNull String ownerClassPath, @NonNull String fieldName, @NonNull Class<?> fieldType) {
    return GETFIELD(ownerClassPath, fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod GETFIELD(@NonNull Class<?> ownerClass, @NonNull String fieldName, Class<?> fieldType) {
    return fieldType == null
      ? GETFIELD(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)))
      : GETFIELD(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod GETFIELD(@NonNull Class<?> ownerClass, @NonNull String fieldName, String fieldTypeDescriptor) {
    return GETFIELD(Type.getInternalName(ownerClass), fieldName, fieldTypeDescriptor);
  }

  public AsmMethod GETFIELD(@NonNull Class<?> ownerClass, @NonNull String fieldName) {
    return GETFIELD(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)));
  }

  public AsmMethod GETFIELD(@NonNull Field field) {
    return GETFIELD(field.getDeclaringClass(), field.getName(), field.getType());
  }

  public AsmMethod GETFIELD(String ownerClassPath, String fieldName, String fieldTypeDescriptor) {
    visitStart();
    mv.visitFieldInsn(Opcodes.GETFIELD, ownerClassPath, fieldName, fieldTypeDescriptor);
    visitFinish();
    txt.fieldAccess("GETFIELD", ownerClassPath, fieldName, fieldTypeDescriptor);
    return this;
  }

  public AsmMethod GETSTATIC(@NonNull Field field) {
    if (!Modifier.isStatic(field.getModifiers())) {
      throw new RuntimeException("vgcAhmy1rd :: Field must be static: " + field);
    }
    return GETSTATIC(field.getDeclaringClass(), field.getName(), Type.getDescriptor(field.getType()));
  }

  public AsmMethod GETSTATIC(@NonNull String ownerClassPath, @NonNull String fieldName, @NonNull Class<?> fieldType) {
    return GETSTATIC(ownerClassPath, fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod GETSTATIC(@NonNull Class<?> ownerClass, @NonNull String fieldName, Class<?> fieldType) {
    return fieldType == null
      ? GETSTATIC(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)))
      : GETSTATIC(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(fieldType));
  }

  public AsmMethod GETSTATIC(@NonNull Class<?> ownerClass, @NonNull String fieldName) {
    return GETSTATIC(Type.getInternalName(ownerClass), fieldName, Type.getDescriptor(extractFieldClass(ownerClass, fieldName)));
  }

  public AsmMethod GETSTATIC(@NonNull Class<?> ownerClass, @NonNull String fieldName, @NonNull String fieldTypeDescsriptor) {
    return GETSTATIC(Type.getInternalName(ownerClass), fieldName, fieldTypeDescsriptor);
  }

  public AsmMethod GETSTATIC(@NonNull String ownerClassPath, @NonNull String fieldName, @NonNull String fieldTypeDescriptor) {
    visitStart();
    mv.visitFieldInsn(Opcodes.GETSTATIC, ownerClassPath, fieldName, fieldTypeDescriptor);
    visitFinish();
    txt.fieldAccess("GETSTATIC", ownerClassPath, fieldName, fieldTypeDescriptor);
    return this;
  }

  @SneakyThrows
  public AsmMethod CONST(int value, Class<?> valueType) {
    if (valueType == byte.class || valueType == short.class || valueType == int.class || valueType == char.class) {
      return LDC_BI_CI_PUSH(value);
    }
    if (valueType == boolean.class) {
      if (value == 0) {
        return ICONST_0();
      }
      return ICONST_1();
    }
    if (valueType == long.class) {
      //@formatter:off
      if (value == 0) return LCONST_0();
      if (value == 1) return LCONST_1();
      //@formatter:on
      return LDC((long) value);
    }
    if (valueType == float.class) {
      //@formatter:off
      if (value == 0) return FCONST_0();
      if (value == 1) return FCONST_1();
      if (value == 2) return FCONST_2();
      //@formatter:on
      return LDC((float) value);
    }
    if (valueType == double.class) {
      //@formatter:off
      if (value == 0) return DCONST_0();
      if (value == 1) return DCONST_1();
      //@formatter:on
      return LDC((double) value);
    }

    if (valueType == String.class) {
      return LDC("" + value);
    }

    if (valueType == Boolean.class) {
      if (value == 0) {
        return GETSTATIC(Boolean.class.getField("TRUE"));
      } else {
        return GETSTATIC(Boolean.class.getField("FALSE"));
      }
    }

    if (valueType == Byte.class) {
      LDC_BI_CI_PUSH(value);
      return INVOKE(Byte.class.getMethod("valueOf", byte.class));
    }

    if (valueType == Short.class) {
      LDC_BI_CI_PUSH(value);
      return INVOKE(Short.class.getMethod("valueOf", short.class));
    }

    if (valueType == Integer.class) {
      LDC(value);
      return INVOKE(Integer.class.getMethod("valueOf", int.class));
    }

    if (valueType == Long.class) {
      LDC((long) value);
      return INVOKE(Long.class.getMethod("valueOf", long.class));
    }

    if (valueType == Float.class) {
      LDC((float) value);
      return INVOKE(Float.class.getMethod("valueOf", float.class));
    }

    if (valueType == Double.class) {
      LDC((double) value);
      return INVOKE(Double.class.getMethod("valueOf", double.class));
    }

    if (valueType == BigInteger.class) {
      //@formatter:off
      if (value ==  0) return GETSTATIC(BigInteger.class.getField("ZERO"));
      if (value ==  1) return GETSTATIC(BigInteger.class.getField("ONE" ));
      if (value ==  2) return GETSTATIC(BigInteger.class.getField("TWO" ));
      if (value == 10) return GETSTATIC(BigInteger.class.getField("TEN" ));
      //@formatter:on

      LDC((long) value);
      return INVOKE(BigInteger.class.getMethod("valueOf", long.class));
    }

    if (valueType == BigDecimal.class) {
      //@formatter:off
      if (value ==  0) return GETSTATIC(BigDecimal.class.getField("ZERO"));
      if (value ==  1) return GETSTATIC(BigDecimal.class.getField("ONE" ));
      if (value == 10) return GETSTATIC(BigDecimal.class.getField("TEN" ));
      //@formatter:on

      LDC((long) value);
      return INVOKE(BigDecimal.class.getMethod("valueOf", long.class));
    }

    throw new RuntimeException("St1QBu72MP :: Cannot CONST `" + value + "` for type " + valueType);
  }


  public AsmMethod LCONST_0() {
    visitStart();
    mv.visitInsn(Opcodes.LCONST_0);
    visitFinish();
    txt.mne("LCONST_0");
    return this;
  }

  public AsmMethod LCONST_1() {
    visitStart();
    mv.visitInsn(Opcodes.LCONST_1);
    visitFinish();
    txt.mne("LCONST_1");
    return this;
  }

  public AsmMethod ACONST_NULL() {
    visitStart();
    mv.visitInsn(Opcodes.ACONST_NULL);
    visitFinish();
    txt.mne("ACONST_NULL");
    return this;
  }

  public AsmMethod LRETURN() {
    visitStart();
    mv.visitInsn(Opcodes.LRETURN);
    visitFinish();
    txt.mne("LRETURN");
    return this;
  }

  public AsmMethod ICONST_M1() {
    visitStart();
    mv.visitInsn(Opcodes.ICONST_M1);
    visitFinish();
    txt.mne("ICONST_M1");
    return this;
  }

  public AsmMethod ICONST_5() {
    visitStart();
    mv.visitInsn(Opcodes.ICONST_5);
    visitFinish();
    txt.mne("ICONST_5");
    return this;
  }

  public AsmMethod ICONST_4() {
    visitStart();
    mv.visitInsn(Opcodes.ICONST_4);
    visitFinish();
    txt.mne("ICONST_4");
    return this;
  }

  public AsmMethod ICONST_3() {
    visitStart();
    mv.visitInsn(Opcodes.ICONST_3);
    visitFinish();
    txt.mne("ICONST_3");
    return this;
  }

  public AsmMethod ICONST_2() {
    visitStart();
    mv.visitInsn(Opcodes.ICONST_2);
    visitFinish();
    txt.mne("ICONST_2");
    return this;
  }

  public AsmMethod ICONST_1() {
    visitStart();
    mv.visitInsn(Opcodes.ICONST_1);
    visitFinish();
    txt.mne("ICONST_1");
    return this;
  }

  public AsmMethod ICONST_0() {
    visitStart();
    mv.visitInsn(Opcodes.ICONST_0);
    visitFinish();
    txt.mne("ICONST_0");
    return this;
  }


  public AsmMethod FCONST_0() {
    visitStart();
    mv.visitInsn(Opcodes.FCONST_0);
    visitFinish();
    txt.mne("FCONST_0");
    return this;
  }

  public AsmMethod FCONST_1() {
    visitStart();
    mv.visitInsn(Opcodes.FCONST_1);
    visitFinish();
    txt.mne("FCONST_1");
    return this;
  }

  public AsmMethod FCONST_2() {
    visitStart();
    mv.visitInsn(Opcodes.FCONST_2);
    visitFinish();
    txt.mne("FCONST_2");
    return this;
  }

  public AsmMethod DCONST_0() {
    visitStart();
    mv.visitInsn(Opcodes.DCONST_0);
    visitFinish();
    txt.mne("DCONST_0");
    return this;
  }

  public AsmMethod DCONST_1() {
    visitStart();
    mv.visitInsn(Opcodes.DCONST_1);
    visitFinish();
    txt.mne("DCONST_1");
    return this;
  }

  public AsmMethod BIPUSH(int byteValue) {
    visitStart();
    mv.visitIntInsn(Opcodes.BIPUSH, byteValue);
    visitFinish();
    txt.mne("BIPUSH", "" + byteValue + " (0x" + AsmByteHelper.intToHex(byteValue, 2) + ")");
    return this;
  }

  public AsmMethod SIPUSH(int shortValue) {
    visitStart();
    mv.visitIntInsn(Opcodes.SIPUSH, shortValue);
    visitFinish();
    txt.mne("SIPUSH", "" + shortValue + " (0x" + AsmByteHelper.intToHex(shortValue, 4) + ")");
    return this;
  }

  public AsmMethod LDC(int intValue) {
    visitStart();
    mv.visitLdcInsn(intValue);
    visitFinish();
    txt.mne("LDC", "" + intValue + " (0x" + AsmByteHelper.intToHex(intValue, 8) + ")", "int");
    return this;
  }

  public AsmMethod LDC_BI_CI_PUSH(int intValue) {
    //@formatter:off

    if (-1 == intValue) return ICONST_M1();
    if ( 0 == intValue) return ICONST_0 ();
    if ( 1 == intValue) return ICONST_1 ();
    if ( 2 == intValue) return ICONST_2 ();
    if ( 3 == intValue) return ICONST_3 ();
    if ( 4 == intValue) return ICONST_4 ();
    if ( 5 == intValue) return ICONST_5 ();

    if ( Byte.MIN_VALUE <= intValue && intValue <=  Byte.MAX_VALUE) return BIPUSH(intValue);
    if (Short.MIN_VALUE <= intValue && intValue <= Short.MAX_VALUE) return SIPUSH(intValue);

    //@formatter:on
    return LDC(intValue);
  }

  public AsmMethod LDC(Class<?> aClass) {
    visitStart();
    if (aClass == null) {
      mv.visitInsn(Opcodes.ACONST_NULL);
    } else {
      mv.visitLdcInsn(Type.getType(aClass));
    }
    visitFinish();
    txt.mne("LDC", "" + aClass);
    return this;
  }

  public AsmMethod LDC(String strValue) {
    visitStart();
    if (strValue == null) {
      mv.visitInsn(Opcodes.ACONST_NULL);
    } else {
      mv.visitLdcInsn(strValue);
    }
    visitFinish();
    if (strValue == null) {
      txt.mne("ACONST_NULL");
    } else {
      txt.mne("LDC", displayString(strValue), "STRING");
    }

    return this;
  }

  private static final int CUT_STRING_LEN = 100;

  private @NonNull String displayString(@NonNull String str) {
    if (str.length() <= CUT_STRING_LEN) {
      return '"' + str + '"';
    }

    return str.length() + " сим. \"" + str.substring(0, CUT_STRING_LEN).replaceAll("\\n", "\\\\n") + "...\"";
  }

  public AsmMethod LDC(float floatValue) {
    visitStart();
    mv.visitLdcInsn(floatValue);
    visitFinish();
    txt.mne("LDC", "" + floatValue + "f", "float");
    return this;
  }

  public AsmMethod LDC(long longValue) {
    visitStart();
    mv.visitLdcInsn(longValue);
    visitFinish();
    txt.mne("LDC", "" + longValue + "L" + " (0x" + AsmByteHelper.longToHex(longValue, 16) + ")", "long");
    return this;
  }

  public AsmMethod LDC(double doubleValue) {
    visitStart();
    mv.visitLdcInsn(doubleValue);
    visitFinish();
    txt.mne("LDC", "" + doubleValue + "d", "double");
    return this;
  }

  public AsmMethod SWAP() {
    visitStart();
    mv.visitInsn(Opcodes.SWAP);
    visitFinish();
    txt.mne("SWAP");
    return this;
  }

  public AsmMethod POP() {
    visitStart();
    mv.visitInsn(Opcodes.POP);
    visitFinish();
    txt.mne("POP");
    return this;
  }

  public AsmMethod POP2() {
    visitStart();
    mv.visitInsn(Opcodes.POP2);
    visitFinish();
    txt.mne("POP2");
    return this;
  }

  public AsmMethod POP(@NonNull Class<?> topStackClass) {
    if (topStackClass == void.class) {
      // Nothing to pop
      return this;
    }
    if (topStackClass == long.class || topStackClass == double.class) {
      return POP2();
    }
    return POP();
  }

  public AsmMethod DUP_X1() {
    visitStart();
    mv.visitInsn(Opcodes.DUP_X1);
    visitFinish();
    txt.mne("DUP_X1");
    return this;
  }

  public AsmMethod DUP_X2() {
    visitStart();
    mv.visitInsn(Opcodes.DUP_X2);
    visitFinish();
    txt.mne("DUP_X2");
    return this;
  }

  public AsmMethod DUP2() {
    visitStart();
    mv.visitInsn(Opcodes.DUP2);
    visitFinish();
    txt.mne("DUP2");
    return this;
  }

  public AsmMethod DUP2_X1() {
    visitStart();
    mv.visitInsn(Opcodes.DUP2_X1);
    visitFinish();
    txt.mne("DUP2_X1");
    return this;
  }

  public AsmMethod DUP2_X2() {
    visitStart();
    mv.visitInsn(Opcodes.DUP2_X2);
    visitFinish();
    txt.mne("DUP2_X2");
    return this;
  }

  public AsmMethod DLOAD(int varIndex) {
    return DLOAD(varIndex, null);
  }

  public AsmMethod DLOAD(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.DLOAD, varIndex);
    visitFinish();

    String commentArg = comment == null ? null : "; " + comment;

    switch (varIndex) {
      case 0 -> txt.mne("DLOAD_0", commentArg);
      case 1 -> txt.mne("DLOAD_1", commentArg);
      case 2 -> txt.mne("DLOAD_2", commentArg);
      case 3 -> txt.mne("DLOAD_3", commentArg);
      default -> txt.mne("DLOAD", "" + varIndex, commentArg);
    }
    return this;
  }

  public AsmMethod DSTORE(int varIndex) {
    return DSTORE(varIndex, null);
  }

  public AsmMethod DSTORE(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.DSTORE, varIndex);
    visitFinish();
    txt.mne("DSTORE", "" + varIndex, comment == null ? null : "; " + comment);
    return this;
  }

  public AsmMethod rem(String commentLine) {
    txt.rem(commentLine);
    return this;
  }

  public AsmMethod space(int lineCount) {
    for (int i = 0; i < lineCount; i++) {
      txt.spaceLine();
    }
    return this;
  }

  public AsmMethod NEWARRAY_double() {
    visitStart();
    mv.visitIntInsn(Opcodes.NEWARRAY, Opcodes.T_DOUBLE);
    visitFinish();
    txt.mne("NEWARRAY", "(double)");
    return this;
  }

  public AsmMethod DASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.DASTORE);
    visitFinish();
    txt.mne("DASTORE");
    return this;
  }

  public AsmMethod DCMPL() {
    visitStart();
    mv.visitInsn(Opcodes.DCMPL);
    visitFinish();
    txt.mne("DCMPL");
    return this;
  }

  public AsmMethod DALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.DALOAD);
    visitFinish();
    txt.mne("DALOAD");
    return this;
  }

  public AsmMethod DCMPG() {
    visitStart();
    mv.visitInsn(Opcodes.DCMPG);
    visitFinish();
    txt.mne("DCMPG");
    return this;
  }

  public AsmMethod IFEQ(@NonNull String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFEQ, getLabel(labelName));
    visitFinish();
    txt.mne("IFEQ", labelName);
    return this;
  }

  public AsmMethod IFNE(@NonNull String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFNE, getLabel(labelName));
    visitFinish();
    txt.mne("IFNE", labelName);
    return this;
  }

  public AsmMethod IFGT(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFGT, getLabel(labelName));
    visitFinish();
    txt.mne("IFGT", labelName);
    return this;
  }

  public AsmMethod IFNULL(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFNULL, getLabel(labelName));
    visitFinish();
    txt.mne("IFNULL", labelName);
    return this;
  }

  public AsmMethod IFNONNULL(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IFNONNULL, getLabel(labelName));
    visitFinish();
    txt.mne("IFNONNULL", labelName);
    return this;
  }

  public AsmMethod IF_ICMPEQ(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ICMPEQ, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ICMPEQ", labelName);
    return this;
  }

  public AsmMethod IF_ICMPNE(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ICMPNE, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ICMPNE", labelName);
    return this;
  }

  public AsmMethod IF_ICMPLT(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ICMPLT, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ICMPLT", labelName);
    return this;
  }

  public AsmMethod IF_ICMPGE(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ICMPGE, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ICMPGE", labelName);
    return this;
  }

  public AsmMethod IF_ICMPGT(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ICMPGT, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ICMPGT", labelName);
    return this;
  }

  public AsmMethod IF_ICMPLE(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ICMPLE, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ICMPLE", labelName);
    return this;
  }

  public AsmMethod IF_ACMPEQ(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ACMPEQ, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ACMPEQ", labelName);
    return this;
  }

  public AsmMethod IF_ACMPNE(String labelName) {
    visitStart();
    mv.visitJumpInsn(Opcodes.IF_ACMPNE, getLabel(labelName));
    visitFinish();
    txt.mne("IF_ACMPNE", labelName);
    return this;
  }

  public AsmMethod LCMP() {
    visitStart();
    mv.visitInsn(Opcodes.LCMP);
    visitFinish();
    txt.mne("LCMP");
    return this;
  }

  public AsmMethod FCMPL() {
    visitStart();
    mv.visitInsn(Opcodes.FCMPL);
    visitFinish();
    txt.mne("FCMPL");
    return this;
  }

  public AsmMethod FCMPG() {
    visitStart();
    mv.visitInsn(Opcodes.FCMPG);
    visitFinish();
    txt.mne("FCMPG");
    return this;
  }


  public AsmMethod IADD() {
    visitStart();
    mv.visitInsn(Opcodes.IADD);
    visitFinish();
    txt.mne("IADD");
    return this;
  }

  public AsmMethod LADD() {
    visitStart();
    mv.visitInsn(Opcodes.LADD);
    visitFinish();
    txt.mne("LADD");
    return this;
  }

  public AsmMethod FADD() {
    visitStart();
    mv.visitInsn(Opcodes.FADD);
    visitFinish();
    txt.mne("FADD");
    return this;
  }

  public AsmMethod DADD() {
    visitStart();
    mv.visitInsn(Opcodes.DADD);
    visitFinish();
    txt.mne("DADD");
    return this;
  }

  public AsmMethod ISUB() {
    visitStart();
    mv.visitInsn(Opcodes.ISUB);
    visitFinish();
    txt.mne("ISUB");
    return this;
  }

  public AsmMethod LSUB() {
    visitStart();
    mv.visitInsn(Opcodes.LSUB);
    visitFinish();
    txt.mne("LSUB");
    return this;
  }

  public AsmMethod FSUB() {
    visitStart();
    mv.visitInsn(Opcodes.FSUB);
    visitFinish();
    txt.mne("FSUB");
    return this;
  }

  public AsmMethod DSUB() {
    visitStart();
    mv.visitInsn(Opcodes.DSUB);
    visitFinish();
    txt.mne("DSUB");
    return this;
  }

  public AsmMethod IMUL() {
    visitStart();
    mv.visitInsn(Opcodes.IMUL);
    visitFinish();
    txt.mne("IMUL");
    return this;
  }

  public AsmMethod LMUL() {
    visitStart();
    mv.visitInsn(Opcodes.LMUL);
    visitFinish();
    txt.mne("LMUL");
    return this;
  }

  public AsmMethod FMUL() {
    visitStart();
    mv.visitInsn(Opcodes.FMUL);
    visitFinish();
    txt.mne("FMUL");
    return this;
  }

  public AsmMethod DMUL() {
    visitStart();
    mv.visitInsn(Opcodes.DMUL);
    visitFinish();
    txt.mne("DMUL");
    return this;
  }

  public AsmMethod IDIV() {
    visitStart();
    mv.visitInsn(Opcodes.IDIV);
    visitFinish();
    txt.mne("IDIV");
    return this;
  }

  public AsmMethod LDIV() {
    visitStart();
    mv.visitInsn(Opcodes.LDIV);
    visitFinish();
    txt.mne("LDIV");
    return this;
  }

  public AsmMethod FDIV() {
    visitStart();
    mv.visitInsn(Opcodes.FDIV);
    visitFinish();
    txt.mne("FDIV");
    return this;
  }

  public AsmMethod DDIV() {
    visitStart();
    mv.visitInsn(Opcodes.DDIV);
    visitFinish();
    txt.mne("DDIV");
    return this;
  }

  public AsmMethod IREM() {
    visitStart();
    mv.visitInsn(Opcodes.IREM);
    visitFinish();
    txt.mne("IREM");
    return this;
  }

  public AsmMethod LREM() {
    visitStart();
    mv.visitInsn(Opcodes.LREM);
    visitFinish();
    txt.mne("LREM");
    return this;
  }

  public AsmMethod FREM() {
    visitStart();
    mv.visitInsn(Opcodes.FREM);
    visitFinish();
    txt.mne("FREM");
    return this;
  }

  public AsmMethod DREM() {
    visitStart();
    mv.visitInsn(Opcodes.DREM);
    visitFinish();
    txt.mne("DREM");
    return this;
  }

  public AsmMethod INEG() {
    visitStart();
    mv.visitInsn(Opcodes.INEG);
    visitFinish();
    txt.mne("INEG");
    return this;
  }

  public AsmMethod LNEG() {
    visitStart();
    mv.visitInsn(Opcodes.LNEG);
    visitFinish();
    txt.mne("LNEG");
    return this;
  }

  public AsmMethod FNEG() {
    visitStart();
    mv.visitInsn(Opcodes.FNEG);
    visitFinish();
    txt.mne("FNEG");
    return this;
  }

  public AsmMethod DNEG() {
    visitStart();
    mv.visitInsn(Opcodes.DNEG);
    visitFinish();
    txt.mne("DNEG");
    return this;
  }

  public AsmMethod ISHL() {
    visitStart();
    mv.visitInsn(Opcodes.ISHL);
    visitFinish();
    txt.mne("ISHL");
    return this;
  }

  public AsmMethod LSHL() {
    visitStart();
    mv.visitInsn(Opcodes.LSHL);
    visitFinish();
    txt.mne("LSHL");
    return this;
  }

  public AsmMethod ISHR() {
    visitStart();
    mv.visitInsn(Opcodes.ISHR);
    visitFinish();
    txt.mne("ISHR");
    return this;
  }

  public AsmMethod LSHR() {
    visitStart();
    mv.visitInsn(Opcodes.LSHR);
    visitFinish();
    txt.mne("LSHR");
    return this;
  }

  public AsmMethod IUSHR() {
    visitStart();
    mv.visitInsn(Opcodes.IUSHR);
    visitFinish();
    txt.mne("IUSHR");
    return this;
  }

  public AsmMethod LUSHR() {
    visitStart();
    mv.visitInsn(Opcodes.LUSHR);
    visitFinish();
    txt.mne("LUSHR");
    return this;
  }

  public AsmMethod IAND() {
    visitStart();
    mv.visitInsn(Opcodes.IAND);
    visitFinish();
    txt.mne("IAND");
    return this;
  }

  public AsmMethod LAND() {
    visitStart();
    mv.visitInsn(Opcodes.LAND);
    visitFinish();
    txt.mne("LAND");
    return this;
  }

  public AsmMethod IOR() {
    visitStart();
    mv.visitInsn(Opcodes.IOR);
    visitFinish();
    txt.mne("IOR");
    return this;
  }

  public AsmMethod LOR() {
    visitStart();
    mv.visitInsn(Opcodes.LOR);
    visitFinish();
    txt.mne("LOR");
    return this;
  }

  public AsmMethod IXOR() {
    visitStart();
    mv.visitInsn(Opcodes.IXOR);
    visitFinish();
    txt.mne("IXOR");
    return this;
  }

  public AsmMethod LXOR() {
    visitStart();
    mv.visitInsn(Opcodes.LXOR);
    visitFinish();
    txt.mne("LXOR");
    return this;
  }

  public AsmMethod IINC(int varIndex, int incValue) {
    visitStart();
    mv.visitIincInsn(varIndex, incValue);
    visitFinish();
    txt.mne("IINC", "" + varIndex, "" + incValue);
    return this;
  }

  public AsmMethod NOP() {
    visitStart();
    mv.visitInsn(Opcodes.NOP);
    visitFinish();
    txt.mne("NOP");
    return this;
  }


  public AsmMethod I2L() {
    visitStart();
    mv.visitInsn(Opcodes.I2L);
    visitFinish();
    txt.mne("I2L");
    return this;
  }

  public AsmMethod I2F() {
    visitStart();
    mv.visitInsn(Opcodes.I2F);
    visitFinish();
    txt.mne("I2F");
    return this;
  }

  public AsmMethod I2D() {
    visitStart();
    mv.visitInsn(Opcodes.I2D);
    visitFinish();
    txt.mne("I2D");
    return this;
  }

  public AsmMethod I2B() {
    visitStart();
    mv.visitInsn(Opcodes.I2B);
    visitFinish();
    txt.mne("I2B");
    return this;
  }

  public AsmMethod I2C() {
    visitStart();
    mv.visitInsn(Opcodes.I2C);
    visitFinish();
    txt.mne("I2C");
    return this;
  }

  public AsmMethod I2S() {
    visitStart();
    mv.visitInsn(Opcodes.I2S);
    visitFinish();
    txt.mne("I2S");
    return this;
  }

  public AsmMethod L2I() {
    visitStart();
    mv.visitInsn(Opcodes.L2I);
    visitFinish();
    txt.mne("L2I");
    return this;
  }

  public AsmMethod L2F() {
    visitStart();
    mv.visitInsn(Opcodes.L2F);
    visitFinish();
    txt.mne("L2F");
    return this;
  }

  public AsmMethod L2D() {
    visitStart();
    mv.visitInsn(Opcodes.L2D);
    visitFinish();
    txt.mne("L2D");
    return this;
  }

  public AsmMethod F2I() {
    visitStart();
    mv.visitInsn(Opcodes.F2I);
    visitFinish();
    txt.mne("F2I");
    return this;
  }

  public AsmMethod F2L() {
    visitStart();
    mv.visitInsn(Opcodes.F2L);
    visitFinish();
    txt.mne("F2L");
    return this;
  }

  public AsmMethod F2D() {
    visitStart();
    mv.visitInsn(Opcodes.F2D);
    visitFinish();
    txt.mne("F2D");
    return this;
  }

  public AsmMethod D2I() {
    visitStart();
    mv.visitInsn(Opcodes.D2I);
    visitFinish();
    txt.mne("D2I");
    return this;
  }

  public AsmMethod D2L() {
    visitStart();
    mv.visitInsn(Opcodes.D2L);
    visitFinish();
    txt.mne("D2L");
    return this;
  }

  public AsmMethod D2F() {
    visitStart();
    mv.visitInsn(Opcodes.D2F);
    visitFinish();
    txt.mne("D2F");
    return this;
  }

  public AsmMethod ILOAD(int varIndex) {
    return ILOAD(varIndex, null);
  }

  public AsmMethod ILOAD(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.ILOAD, varIndex);
    visitFinish();

    String commentArg = comment == null ? null : "; " + comment;

    switch (varIndex) {
      case 0 -> txt.mne("ILOAD_0", commentArg);
      case 1 -> txt.mne("ILOAD_1", commentArg);
      case 2 -> txt.mne("ILOAD_2", commentArg);
      case 3 -> txt.mne("ILOAD_3", commentArg);
      default -> txt.mne("ILOAD", "" + varIndex, commentArg);
    }
    return this;
  }


  public AsmMethod FLOAD(int varIndex) {
    return FLOAD(varIndex, null);
  }

  public AsmMethod FLOAD(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.FLOAD, varIndex);
    visitFinish();
    txt.mne("FLOAD", "" + varIndex, comment == null ? null : ";" + comment);
    return this;
  }

  public AsmMethod FSTORE(int varIndex) {
    return FSTORE(varIndex, null);
  }

  public AsmMethod FSTORE(int varIndex, String comment) {
    visitStart();
    mv.visitVarInsn(Opcodes.FSTORE, varIndex);
    visitFinish();
    txt.mne("FSTORE", "" + varIndex, comment == null ? null : "; " + comment);
    return this;
  }

  public AsmMethod IRETURN() {
    visitStart();
    mv.visitInsn(Opcodes.IRETURN);
    visitFinish();
    txt.mne("IRETURN");
    return this;
  }

  public AsmMethod FRETURN() {
    visitStart();
    mv.visitInsn(Opcodes.FRETURN);
    visitFinish();
    txt.mne("FRETURN");
    return this;
  }

  public AsmMethod DRETURN() {
    visitStart();
    mv.visitInsn(Opcodes.DRETURN);
    visitFinish();
    txt.mne("DRETURN");
    return this;
  }

  public AsmMethod INSTANCEOF(@NonNull Class<?> aClass) {
    return INSTANCEOF(Type.getInternalName(aClass));
  }

  public AsmMethod INSTANCEOF(@NonNull String internalClassName) {
    visitStart();
    mv.visitTypeInsn(Opcodes.INSTANCEOF, internalClassName);
    visitFinish();
    txt.mne("INSTANCEOF", txt.importType(internalClassName));
    return this;
  }

  public AsmMethod ARRAYLENGTH() {
    visitStart();
    mv.visitInsn(Opcodes.ARRAYLENGTH);
    visitFinish();
    txt.mne("ARRAYLENGTH");
    return this;
  }

  public AsmMethod IALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.IALOAD);
    visitFinish();
    txt.mne("IALOAD");
    return this;
  }

  public AsmMethod LALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.LALOAD);
    visitFinish();
    txt.mne("LALOAD");
    return this;
  }

  public AsmMethod FALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.FALOAD);
    visitFinish();
    txt.mne("FALOAD");
    return this;
  }

  public AsmMethod AALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.AALOAD);
    visitFinish();
    txt.mne("AALOAD");
    return this;
  }

  public AsmMethod BALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.BALOAD);
    visitFinish();
    txt.mne("BALOAD");
    return this;
  }

  public AsmMethod СALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.CALOAD);
    visitFinish();
    txt.mne("CALOAD");
    return this;
  }

  public AsmMethod SALOAD() {
    visitStart();
    mv.visitInsn(Opcodes.SALOAD);
    visitFinish();
    txt.mne("SALOAD");
    return this;
  }

  public AsmMethod SASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.SASTORE);
    visitFinish();
    txt.mne("SASTORE");
    return this;
  }

  public AsmMethod CASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.CASTORE);
    visitFinish();
    txt.mne("CASTORE");
    return this;
  }

  public AsmMethod BASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.BASTORE);
    visitFinish();
    txt.mne("BASTORE");
    return this;
  }

  public AsmMethod AASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.AASTORE);
    visitFinish();
    txt.mne("AASTORE");
    return this;
  }

  public AsmMethod FASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.FASTORE);
    visitFinish();
    txt.mne("FASTORE");
    return this;
  }

  public AsmMethod LASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.LASTORE);
    visitFinish();
    txt.mne("LASTORE");
    return this;
  }

  public AsmMethod IASTORE() {
    visitStart();
    mv.visitInsn(Opcodes.IASTORE);
    visitFinish();
    txt.mne("IASTORE");
    return this;
  }

  public AsmMethod MULTIANEWARRAY(int arrayDimentions, @NonNull String arrayType) {
    visitStart();
    mv.visitMultiANewArrayInsn(arrayType, arrayDimentions);
    visitFinish();
    txt.mne("MULTIANEWARRAY", "" + arrayDimentions, txt.importType(arrayType));
    return this;
  }

  public AsmMethod MULTIANEWARRAY(int arrayDimentions, @NonNull Class<?> arrayType) {
    return MULTIANEWARRAY(arrayDimentions, Type.getInternalName(arrayType));
  }

  public AsmMethod NEWARRAY(@NonNull VarType varType) {
    visitStart();
    mv.visitIntInsn(Opcodes.NEWARRAY, varType.opCode);
    visitFinish();
    txt.mne("NEWARRAY", varType.name());
    return this;
  }

  public AsmMethod ANEWARRAY(Class<?> aClass) {
    return ANEWARRAY(Type.getInternalName(aClass));
  }

  public AsmMethod ANEWARRAY(String elementType) {
    visitStart();
    mv.visitTypeInsn(Opcodes.ANEWARRAY, elementType);
    visitFinish();
    txt.mne("ANEWARRAY", txt.importType(elementType));
    return this;
  }

  public AsmMethod TRY_CATCH(String labelTry, String labelFinally, String labelCatch, Class<?> errorClass) {
    return TRY_CATCH(labelTry, labelFinally, labelCatch, errorClass == null ? null : Type.getInternalName(errorClass));
  }

  public AsmMethod TRY_CATCH(String labelTry, String labelFinally, String labelCatch) {
    return TRY_CATCH(labelTry, labelFinally, labelCatch, (String) null);
  }

  public AsmMethod TRY_CATCH(String labelTry, String labelFinally, String labelCatch, String errorClassPath) {
    visitStart();
    mv.visitTryCatchBlock(getLabel(labelTry), getLabel(labelFinally), getLabel(labelCatch), errorClassPath);
    visitFinish();
    txt.tryCatch(labelTry, labelFinally, labelCatch, errorClassPath);
    return this;
  }

  public AsmMethod label(@NonNull String labelName) {
    mv.visitLabel(getLabel(labelName));
    txt.label(labelName);
    return this;
  }

  public @NonNull String newLabelName() {
    return newLabelName(null);
  }

  public @NonNull String newLabelName(String labelPrefix) {
    labelPrefix = labelPrefix != null ? labelPrefix : "__L";

    Integer i = newLabelPrefixes.get(labelPrefix);
    if (i == null) {
      i = 1;
    } else {
      i++;
    }
    newLabelPrefixes.put(labelPrefix, i);

    return labelPrefix + i;
  }

  public AsmMethod label(@NonNull String labelName, int lineNumber) {
    mv.visitLabel(getLabel(labelName));
    mv.visitLineNumber(lineNumber, getLabel(labelName));
    txt.label(labelName, (short) lineNumber);
    return this;
  }

  public AsmMethod lineNumber(@NonNull String labelName, int lineNumber) {
    mv.visitLineNumber(lineNumber, getLabel(labelName));
    txt.lineNumber((short) lineNumber, labelName);
    return this;
  }

  @SneakyThrows
  public AsmMethod CMP(boolean less, @NonNull Class<?> valueClass) {
    if (valueClass == boolean.class || valueClass == byte.class || valueClass == short.class || valueClass == int.class) {
      return ISUB();
    }

    if (valueClass == long.class) {
      return LCMP();
    }

    if (valueClass == float.class) {
      return less ? FCMPL() : FCMPG();
    }

    if (valueClass == double.class) {
      return less ? DCMPL() : DCMPG();
    }

    if (Comparable.class.isAssignableFrom(valueClass)) {
      return INVOKE(Comparable.class.getMethod("compareTo", Object.class));
    }

    throw new RuntimeException("oc6YdNTh5T :: Cannot compare type " + valueClass);
  }

  public AsmMethod CMPL(@NonNull Class<?> valueClass) {
    return CMP(true, valueClass);
  }

  public AsmMethod CMPG(@NonNull Class<?> valueClass) {
    return CMP(false, valueClass);
  }

  public AsmMethod INC(int varIndex, int delta, @NonNull Class<?> valueClass) {
    return INC(varIndex, delta, valueClass, null);
  }

  @SneakyThrows
  public AsmMethod INC(int varIndex, int delta, @NonNull Class<?> valueClass, String comment) {

    if (valueClass == byte.class || valueClass == short.class || valueClass == int.class) {
      return IINC(varIndex, delta);
    }

    if (valueClass == long.class) {
      LLOAD(varIndex, comment);
      CONST(delta, valueClass);
      LADD();
      LSTORE(varIndex, comment);
      return this;
    }

    if (valueClass == float.class) {
      FLOAD(varIndex, comment);
      CONST(delta, valueClass);
      FADD();
      FSTORE(varIndex, comment);
      return this;
    }

    if (valueClass == double.class) {
      DLOAD(varIndex, comment);
      CONST(delta, valueClass);
      DADD();
      DSTORE(varIndex, comment);
      return this;
    }

    if (valueClass == Byte.class) {
      ALOAD(varIndex, comment);
      INVOKE(Byte.class.getMethod("byteValue"));
      CONST(delta, byte.class);
      IADD();
      INVOKE(Byte.class.getMethod("valueOf", byte.class));
      ASTORE(varIndex, comment);
      return this;
    }

    if (valueClass == Short.class) {
      ALOAD(varIndex, comment);
      INVOKE(Short.class.getMethod("shortValue"));
      CONST(delta, short.class);
      IADD();
      INVOKE(Short.class.getMethod("valueOf", short.class));
      ASTORE(varIndex, comment);
      return this;
    }

    if (valueClass == Integer.class) {
      ALOAD(varIndex, comment);
      INVOKE(Integer.class.getMethod("intValue"));
      CONST(delta, int.class);
      IADD();
      INVOKE(Integer.class.getMethod("valueOf", int.class));
      ASTORE(varIndex, comment);
      return this;
    }

    if (valueClass == Long.class) {
      ALOAD(varIndex, comment);
      INVOKE(Long.class.getMethod("longValue"));
      CONST(delta, long.class);
      LADD();
      INVOKE(Long.class.getMethod("valueOf", long.class));
      ASTORE(varIndex, comment);
      return this;
    }

    if (valueClass == Float.class) {
      ALOAD(varIndex, comment);
      INVOKE(Float.class.getMethod("floatValue"));
      CONST(delta, float.class);
      FADD();
      INVOKE(Float.class.getMethod("valueOf", float.class));
      ASTORE(varIndex, comment);
      return this;
    }

    if (valueClass == Double.class) {
      ALOAD(varIndex, comment);
      INVOKE(Double.class.getMethod("doubleValue"));
      CONST(delta, double.class);
      DADD();
      INVOKE(Double.class.getMethod("valueOf", double.class));
      ASTORE(varIndex, comment);
      return this;
    }

    if (valueClass == BigInteger.class) {
      ALOAD(varIndex, comment);
      CONST(delta, BigInteger.class);
      INVOKE(BigInteger.class.getMethod("add", BigInteger.class));
      ASTORE(varIndex, comment);
      return this;
    }

    if (valueClass == BigDecimal.class) {
      ALOAD(varIndex, comment);
      CONST(delta, BigDecimal.class);
      INVOKE(BigDecimal.class.getMethod("add", BigDecimal.class));
      ASTORE(varIndex, comment);
      return this;
    }

    throw new RuntimeException("cXA8mnrPRI :: Cannot increment type: " + valueClass);
  }

}
