package kz.greetgo.asm;

import lombok.RequiredArgsConstructor;
import org.objectweb.asm.Opcodes;

@RequiredArgsConstructor
public enum VarType {
  BOOLEAN(Opcodes.T_BOOLEAN),
  CHAR(Opcodes.T_CHAR),
  FLOAT(Opcodes.T_FLOAT),
  DOUBLE(Opcodes.T_DOUBLE),
  BYTE(Opcodes.T_BYTE),
  SHORT(Opcodes.T_SHORT),
  INT(Opcodes.T_INT),
  LONG(Opcodes.T_LONG),

  ;

  public final int opCode;
}
