package kz.greetgo.asm.help;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.objectweb.asm.ByteVector;
import org.objectweb.asm.MethodVisitor;

import java.lang.reflect.Field;
import java.util.Optional;

public class AsmByteHelper {

  @SneakyThrows
  private static Optional<ByteVector> extractByteVector(@NonNull MethodVisitor mv) {
    final Field fieldCode;

    try {
      fieldCode = mv.getClass().getDeclaredField("code");
    } catch (NoSuchFieldException e) {
      return Optional.empty();
    }

    fieldCode.setAccessible(true);

    final Object byteVector = fieldCode.get(mv);

    if (!(byteVector instanceof ByteVector bv)) {
      return Optional.empty();
    }

    final Field fieldData;
    try {
      fieldData = bv.getClass().getDeclaredField("data");
    } catch (NoSuchFieldException e) {
      return Optional.empty();
    }

    fieldData.setAccessible(true);

    final Object data = fieldData.get(bv);

    return data instanceof byte[] ? Optional.of(bv) : Optional.empty();
  }

  public static boolean canByteExtracting(@NonNull MethodVisitor mv) {
    return extractByteVector(mv).isPresent();
  }

  public static int length(@NonNull MethodVisitor mv) {
    return extractByteVector(mv).map(ByteVector::size).orElseThrow();
  }

  public static byte[] extract(@NonNull MethodVisitor mv, int offset, int count) {
    return extractByteVector(mv).map(bv -> AsmByteHelper.extractBytes(bv, offset, count)).orElseThrow();
  }

  @SneakyThrows
  private static byte[] extractBytes(@NonNull ByteVector bv, int offset, int count) {
    final Field fieldData = bv.getClass().getDeclaredField("data");
    fieldData.setAccessible(true);

    byte[] data = (byte[]) fieldData.get(bv);

    final byte[] ret = new byte[count];

    System.arraycopy(data, offset, ret, 0, count);

    return ret;
  }

  public static @NonNull String intToHex(int intValue, int resultLength) {
    final String hex    = Integer.toHexString(intValue);
    final int    hexLen = hex.length();
    if (hexLen >= resultLength) {
      return hex;
    }

    return "0".repeat(resultLength - hexLen) + hex;
  }

  public static @NonNull String longToHex(long longValue, int resultLength) {
    final String hex    = Long.toHexString(longValue);
    final int    hexLen = hex.length();
    if (hexLen >= resultLength) {
      return hex;
    }

    return "0".repeat(resultLength - hexLen) + hex;
  }

  public static @NonNull String bytesToHex(byte[] bytes) {

    StringBuilder sb = new StringBuilder();

    for (final byte b : bytes) {

      if (sb.length() > 0) {
        sb.append(' ');
      }

      final String byteHex = Integer.toHexString(Byte.toUnsignedInt(b));
      if (byteHex.length() < 2) {
        sb.append('0');
      }

      sb.append(byteHex);
    }

    return sb.toString();
  }

  public static String intToDec(int intValue, int resultLength) {
    final String str    = "" + intValue;
    final int    hexLen = str.length();
    if (hexLen >= resultLength) {
      return str;
    }

    return "0".repeat(resultLength - hexLen) + str;
  }
}
