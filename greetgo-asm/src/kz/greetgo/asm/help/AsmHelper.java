package kz.greetgo.asm.help;

import kz.greetgo.asm.AccessType;
import lombok.NonNull;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;

public class AsmHelper {
  public static void tab(@NonNull StringBuilder sb, int tab, boolean makeSpaceAnEnd) {
    final int length = sb.length();
    if (length < tab) {
      sb.append(" ".repeat(tab - length));
    }
    if (makeSpaceAnEnd && sb.charAt(sb.length() - 1) != ' ') {
      sb.append(' ');
    }
  }

  public static int takeOpcodes(@NonNull AccessType accessType, boolean isStatic) {
    int opCodes = 0;

    switch (accessType) {
      case PUBLIC -> opCodes = Opcodes.ACC_PUBLIC;
      case PRIVATE -> opCodes = Opcodes.ACC_PRIVATE;
      case PROTECTED -> opCodes = Opcodes.ACC_PROTECTED;
    }

    if (isStatic) {
      opCodes |= Opcodes.ACC_STATIC;
    }
    return opCodes;
  }

  public static String voidDesc(Class<?>... classes) {
    return Arrays.stream(classes)
                 .map(Type::getDescriptor)
                 .collect(joining("", "(", ")V"));
  }

  public static String returnDesc(@NonNull Class<?> returnClass, Class<?>... classes) {
    return Arrays.stream(classes)
                 .map(Type::getDescriptor)
                 .collect(joining("", "(", ")" + Type.getDescriptor(returnClass)));
  }

  public static @NonNull Class<?> extractFieldClass(@NonNull Class<?> ownerClass, @NonNull String fieldName) {
    try {
      return ownerClass.getField(fieldName).getType();
    } catch (NoSuchFieldException ignore) {
      // ignore it
    }

    try {
      return ownerClass.getDeclaredField(fieldName).getType();
    } catch (NoSuchFieldException e) {
      throw new RuntimeException(e);
    }

  }

}
